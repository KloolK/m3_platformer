<?xml version="1.0" encoding="UTF-8"?>
<ColorScheme>
  <objects.Block>
  	<colorWrapper>
    	<color r="12" g="20" b="31" a="255"/>
    	<color r="12" g="20" b="31" a="255"/>
    	<color r="111" g="195" b="223" a="255"/>
    	<color r="230" g="255" b="255" a="255"/>
    </colorWrapper>
  </objects.Block>
  <objects.SolidRectangle>
  	<colorWrapper>
    	<color r="12" g="20" b="31" a="255"/>
    	<color r="12" g="20" b="31" a="255"/>
    	<color r="111" g="195" b="223" a="255"/>
    	<color r="230" g="255" b="255" a="255"/>
    </colorWrapper>
  </objects.SolidRectangle>
  <objects.DeathZone>
  	<colorWrapper>
      <color r="12" g="20" b="31" a="255"/>
      <color r="12" g="20" b="31" a="255"/>
      <color r="255" g="43" b="22" a="255"/>
      <color r="255" g="43" b="22" a="255"/>
    </colorWrapper>
  </objects.DeathZone>
  <objects.SpawnZone>
    <colorWrapper>
      <color r="12" g="20" b="31" a="255"/> 
      <color r="12" g="20" b="31" a="255"/> 
      <color r="255" g="230" b="77" a="255"/> 
      <color r="255" g="230" b="77" a="255"/>
    </colorWrapper>
  </objects.SpawnZone>
  <objects.Gun>
    <colorWrapper>
      <color r="12" g="20" b="31" a="255"/>
      <color r="12" g="20" b="31" a="255"/>
      <color r="246" g="168" b="40" a="255"/>
      <color r="246" g="168" b="40" a="255"/>
    </colorWrapper>
  </objects.Gun>
  <objects.PathSpeedTrigger>
    <colorWrapper>
      <color r="12" g="20" b="31" a="255"/> 
      <color r="12" g="20" b="31" a="255"/> 
      <color r="255" g="230" b="77" a="255"/> 
      <color r="255" g="230" b="77" a="255"/>
    </colorWrapper>
  </objects.PathSpeedTrigger>
  <objects.TriggerZone>
    <colorWrapper>
      <color r="12" g="20" b="31" a="255"/>
      <color r="12" g="20" b="31" a="255"/>
      <color r="111" g="195" b="223" a="255"/>
      <color r="230" g="255" b="255" a="255"/>
    </colorWrapper>
  </objects.TriggerZone>
  <objects.SimpleEnemy>
    <colorWrapper>
      <color r="12" g="20" b="31" a="255"/>
      <color r="12" g="20" b="31" a="255"/>
      <color r="255" g="43" b="22" a="255"/>
      <color r="255" g="43" b="22" a="255"/>
    </colorWrapper>
  </objects.SimpleEnemy>
  <objects.PlayerFigure>
    <spriteList>
      <m3platformer.AnimatedSprite name="data/imgs/alan/idle.gif" x="0" y="-6" rotation="0" scale="1"/>
      <m3platformer.Sprite name="data/imgs/alan/left.gif" x="0" y="-6" rotation="0" scale="1"/>
      <m3platformer.Sprite name="data/imgs/alan/right.gif" x="0" y="-6" rotation="0" scale="1"/>
    </spriteList>
  </objects.PlayerFigure>
  <objects.Coin>
    <spriteList>
      <m3platformer.AnimatedSprite name="data/imgs/coinFlip.gif" x="0" y="0" scale="0.5" rotation="0"/>
    </spriteList>
  </objects.Coin>
  <objects.EndZone>
    <spriteList>
      <m3platformer.AnimatedSprite name="data/imgs/portals/blueAnim.gif" x="0" y="0" scale="1" rotation="0"/>
    </spriteList>
  </objects.EndZone>
</ColorScheme>