package objects;

public interface Movable {
	public void move();
}
