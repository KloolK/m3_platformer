package objects;

import java.awt.Color;
import m3platformer.Layer;
import m3platformer.M3Platformer;

public class Figure extends SolidRectangle
{
	// Constructor
	public Figure(M3Platformer parent, Layer layer, Color fill, Color border, float x, float y, int w, int h)
	{
		super(parent, layer, fill, fill, border, border, x, y, w, h);
	}
}