package objects;

import java.awt.Color;

import m3platformer.Layer;
import m3platformer.M3Platformer;

public class Block extends SolidRectangle
{
	public Block(M3Platformer parent, float x, float y, int w, int h)
	{
		super(parent, Layer.Layer1, new Color(12, 20, 31), new Color(12, 20, 31), new Color(111,195,223), new Color(230, 255, 255), x, y, w, h);
	}
	
	protected Block(M3Platformer parent)
	{
		super(parent);
	}
}
