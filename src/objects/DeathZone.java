package objects;

import java.awt.Color;

import m3platformer.Layer;
import m3platformer.M3Platformer;

public class DeathZone extends RectangleZone implements Movable
{
	public DeathZone(M3Platformer parent, float x, float y, float w, float h)
	{
		super(parent, Layer.Layer2, new Color(12, 20, 31), new Color(12, 20, 31), new Color(255, 43, 22), new Color(255, 43, 22), x, y, w, h);
	}

	// empty default constructor (used for deserialization)
	protected DeathZone(M3Platformer parent)
	{
		super(parent);
	}

	public void move()
	{
		defaultMove();
	}
}
