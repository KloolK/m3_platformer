package objects;

import java.awt.Color;
import java.util.List;

import m3platformer.AnimatedSprite;
import m3platformer.ColorScheme;
import m3platformer.Layer;
import m3platformer.M3Platformer;
import m3platformer.Sprite;

public class Coin extends SimpleItem {

	Sprite coinGif;

	protected Coin(M3Platformer parent) {
		super(parent);
		
		loadAnimatedSprite(new AnimatedSprite(parent, "data/imgs/coinFlip.gif", 0.5f));
	}
	
	public Coin(M3Platformer parent, float x, float y) 
	{
		super(parent, Layer.Layer1, new Color(0xffd144), new Color(0xffd144), new Color(0xfff664), new Color(0xfff664), x, y, 10, 10);
		loadAnimatedSprite(new AnimatedSprite(parent, "data/imgs/coinFlip.gif", 0.5f));
	}
	
	@Override
	public void applyColorScheme(ColorScheme c) {
		List<Sprite> spList = c.getSpriteList(Coin.class.getName());
		if(spList == null || spList.size()==0) return;
		
		Sprite sp = spList.get(0);
		if(sp instanceof AnimatedSprite) loadAnimatedSprite((AnimatedSprite) sp);
		else coinGif = sp;
	}
	
	private void loadAnimatedSprite(AnimatedSprite a) {
		coinGif = new AnimatedSprite(a);
		((AnimatedSprite)coinGif).jump((int)this.parent.random(0, 20));
	}
	
	@Override
	public void fetch(PlayerFigure player) {
		player.addCoin();
	}
	
	@Override
	public void drawShape(int x, int y)
	{
		coinGif.draw(x, y);
	}
}