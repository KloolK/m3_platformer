package objects;

import m3platformer.ColorScheme;

public interface ColorSchemable {
	public void applyColorScheme(ColorScheme c);
}
