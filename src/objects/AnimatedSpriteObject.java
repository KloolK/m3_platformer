package objects;

import processing.data.XML;
import m3platformer.AnimatedSprite;
import m3platformer.Layer;
import m3platformer.M3Platformer;

public abstract class AnimatedSpriteObject extends SpriteObject {

	protected AnimatedSpriteObject(M3Platformer parent) {
		super(parent);
	}
	
	public AnimatedSpriteObject(M3Platformer parent, Layer l, AnimatedSprite anim, float x, float y)
	{
		super(parent, l, anim, x, y);
	}

	@Override
	public void deserialize(XML xml) {
		super.deserialize(xml);
		if(xml.getChild(AnimatedSprite.class.getName())!=null) {
			this.setSprite(AnimatedSprite.deserialize(parent, xml.getChild(AnimatedSprite.class.getName())));
		}
	}
}
