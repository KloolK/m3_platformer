package objects;

import java.awt.Color;
import java.awt.geom.Rectangle2D;

import m3platformer.Layer;
import m3platformer.M3Platformer;
import m3platformer.Zone;

public abstract class RectangleZone extends RectangleObject implements Zone
{	
	public RectangleZone(M3Platformer parent, Layer layer, Color fill, Color fillC, Color border, Color borderC, float x, float y, float w, float h)
	{
		super(parent, layer, fill, fillC, border, borderC, x, y, w, h);
	}
	
	// empty default constructor (used for deserialization)
	protected RectangleZone(M3Platformer parent) {
		super(parent);
	}

	@Override
	public Rectangle2D getZoneFrame()
	{
		return getFrame();
	}
}
