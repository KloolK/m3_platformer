package objects;

import java.awt.Color;

import m3platformer.Layer;
import m3platformer.M3Platformer;

public abstract class SimpleItem extends RectangleZone implements Fetchable {

	protected SimpleItem(M3Platformer parent) {
		super(parent);
	}
	
	public SimpleItem(M3Platformer parent, Layer layer, Color fill, Color fillC, Color border, Color borderC, float x, float y, float w, float h) {
		super(parent,layer, fill, fillC, border, borderC, x,y,w,h);
	}
}
