package objects;

import java.awt.geom.Rectangle2D;
import m3platformer.M3Platformer;

public class SimpleEnemy extends DeathZone implements Collidable {
	
	public SimpleEnemy(M3Platformer parent, float x, float y, float w, float h)
	{
		super(parent, x, y, w, h);
		this.gravityAffected = true;
		this.moveTowardsPlayer = true;
	}
	
	protected SimpleEnemy(M3Platformer parent) {
		super(parent);
		this.gravityAffected = true;
		this.moveTowardsPlayer = true;
	}
		
	public Rectangle2D getCollisionFrame() {
		return getFrame();
	}
}
