package objects;

import java.awt.Color;
import java.util.List;

import m3platformer.AnimatedSprite;
import m3platformer.ColorScheme;
import m3platformer.Command;
import m3platformer.Controller;
import m3platformer.ControllerKey;
import m3platformer.Layer;
import m3platformer.M3Platformer;
import m3platformer.SoundManager;
import m3platformer.Sprite;
import m3platformer.Vector2D;
import m3platformer.SoundManager.SoundType;
import m3platformer.animations.*;

public class PlayerFigure extends Figure implements Movable
{
	Sprite stand, udStand;
	Sprite left, udLeft;
	Sprite right, udRight;

	// if the player is alive
	private boolean alive = false;

	// if the player has reached an EndZone
	private EndZone reachedEnd = null;
	
	// constant speed (not affected by gravity, friction, speed limits)
	// used for being carried by moving objects
	private Vector2D constSpeed = new Vector2D(0,0);
	
	// collected coin count
	private int coinCount = 0;
	private int totalScore = 0;
	
	// easter egg
	private boolean upsideDown;
	
	// Constructor
	public PlayerFigure(M3Platformer parent, Layer layer, Color fillColor, Color borderColor, float x, float y, int w, int h)
	{
		super(parent, layer, fillColor, borderColor, x, y, w, h);
		
		// load sprites
		this.stand = new AnimatedSprite(parent, "data/imgs/alan/idle.gif", 0, -6, 1, 0);
		this.udStand = new AnimatedSprite(parent, "data/imgs/alan/upsideDown/idle.gif", 0, 0, 1, 0);
		this.left = new Sprite(parent, "data/imgs/alan/left.gif", 0, -6, 1, 0);
		this.udLeft = new AnimatedSprite(parent, "data/imgs/alan/upsideDown/left.gif", 0, 0, 1, 0);
		this.right = new Sprite(parent, "data/imgs/alan/right.gif", 0, -6, 1, 0);
		this.udRight = new AnimatedSprite(parent, "data/imgs/alan/upsideDown/right.gif", 0, 0, 1, 0);
		
		this.gravityAffected = this.frictionAffected = true;
	}
	
	@Override
	public void drawShape(int x, int y) {
		if(speed.getX() == 0) 		if(!upsideDown) stand.draw(x, y);	else udStand.draw(x, y);
		else if(speed.getX() < 0)	if(!upsideDown) left.draw(x, y);	else udLeft.draw(x, y);
		else if(speed.getX() > 0)   if(!upsideDown) right.draw(x, y);	else udRight.draw(x, y);
	}
	
	public void move() {
		Vector2D tmp = new Vector2D(constSpeed);
		//applyCollisionsToVect(tmp);
		
		applyMovementPhysics();
		capSpeed();
		
		// slow down gravity if hanging on wall and do not permit leaving wall if not jumping or on bottom
		// TODO: do not use controller here
		if(!M3Platformer.getInstance().getLogic().getController().isKeyPressed(ControllerKey.s) && (getTouchingLeft() || getTouchingRight())) {
			speed.setY(Math.min(speed.getY(), 5));
			
			if(!getTouchingBottom() && speed.getY() > 0 && speed.getX() != 0) {
				speed.setX(0);
			}
		}
		
		applyCollisionsToVect(speed);
		
		speed.add(tmp);
		moveByVector(speed);
		speed.sub(tmp);
	}
	
	public void jump() {
		
		Controller cont = M3Platformer.getInstance().getLogic().getController();
		
		// Walljump, when touching a wall and not on the ground
		if ((getTouchingLeft() || getTouchingRight()) && (cont.isKeyPressed(ControllerKey.d) || cont.isKeyPressed(ControllerKey.a)) && !getTouchingBottom())
		{
			if (getTouchingLeft())	speed.setX( WALLJUMP_HORIZONTAL);
			else					speed.setX(-WALLJUMP_HORIZONTAL);
			
			speed.setY(-WorldObject.JUMP_HEIGHT);
			SoundManager.playJump();
			return;
		}
		
		else if (getTouchingBottom()) {
			this.accelerate(new Vector2D(0,-WorldObject.JUMP_HEIGHT - speed.getY()));
			
			
			
			// Rotate player
			if(this.speed.getX() != 0  && this.getTouchingBottom() && (int)this.parent.random(0, 10) == 0) {
				Animation a;

				// rarely: 180°
				if((int)this.parent.random(0, 30) == 0) {
					a = new Spin(200, (this.speed.getX() > 0 ? 0.5f : -0.5f));
					a.setFinishCmd(new Command() {
						public void call(M3Platformer game) {
							game.getPlayer().toggleUpsideDown();
						}
					});					
				}
				
				// 360°
				else a = new Spin(200, (this.speed.getX() > 0 ? 1f : -1f));
								
				if (!this.isAnimationPlayed(a)) {
					this.addAnimation(a);
				}
				
				SoundManager.playSound(SoundType.FLIP);
			}
			else {
				SoundManager.playJump();
			}
		}
	}
	
	@Override
	public void applyColorScheme(ColorScheme c) {
		// load sprites out of color scheme
		List<Sprite> spList = c.getSpriteList(PlayerFigure.class.getName());
		if(spList == null || spList.size()==0) return;
		
		Sprite first = spList.get(0);
		this.stand = first;
		
		if(spList.size()>1) {
			this.left = spList.get(1);
			if(spList.size()>2) {
				this.right = spList.get(2);
			}
		}
	}

	// hoops... easter egg
	protected void toggleUpsideDown() {
		this.upsideDown = !this.upsideDown;
	}
	
	
	// we reached an end zone -> animation
	public void reachedEndAnimation(Command callback) {
		// drag to center of zone
		SoundManager.playSound(SoundType.TELEPORT);
		Vector2D movement = new Vector2D((float)reachedEnd.getZoneFrame().getCenterX(), (float)reachedEnd.getZoneFrame().getCenterY()).
				sub(this.getPosition()).sub(new Vector2D(this.getDimensions()).scale(0.5f));
		addAnimation(new Move(400, movement, true).
				setFollowUp(new RepeatedAnimation().addAnimation(new Spin(400,1))));
		addAnimation(new Delay(400).setFollowUp(new Scale(1500,1,0).setFinishCmd(callback)));
	}

	// we are expected to die, with an animation
	public void deathAnimation(Command callback)
	{
		// death animation	
		SoundManager.playSound(SoundType.TELEPORT);	
		RepeatedAnimation a = new RepeatedAnimation(-1);
		a.addAnimation(new Blink(100,1));
		addAnimation(a);
		
		a = new RepeatedAnimation(-1);
		a.addAnimation(new Spin(100,1));
		addAnimation(a);
		
		Animation t = new Scale(800, 1f, 0f);
		t.setFinishCmd(callback);
		
		addAnimation(t);
	}
	
	// really die, no animation
	public void kill() {
		this.alive=false;
		this.reachedEnd=null;
	}

	// reset and spawn player (with animation)
	public void spawn(Command callback)
	{		
		this.speed.scale(0);
		this.constSpeed.scale(0);
		this.resetTouching();
		this.clearAnimations();

		this.alive = true;
		this.reachedEnd = null;
		this.upsideDown = false;
		
		Animation s = new Scale(800,0f,1f);
		s.setFinishCmd(callback);
		this.addAnimation(s);
		
		SoundManager.playSound(SoundType.SPAWN);
	}
	
	// hey, I found money!
	public void addCoin() {
		coinCount++;
		SoundManager.playCoin();
	}
	
	// getters and setters
	public Vector2D getConstSpeed() {return new Vector2D(constSpeed);}
	public void setConstSpeed(Vector2D v) { constSpeed = v; }
	public void resetConstSpeed() { constSpeed.scale(0); }

	public int getCoinCount() {
		return coinCount;
	}
	
	public void resetCoinCount() {coinCount=0;}
	

	public int getTotalScore() {
		return totalScore;
	}
	
	public void addToTotalScore(int score) {
		setTotalScore(getTotalScore()+score);
	}

	public void setTotalScore(int totalScore) {
		this.totalScore = totalScore;
	}
	
	public boolean isAlive()
	{
		return this.alive;
	}

	public boolean hasReachedEnd()
	{
		return this.reachedEnd!=null;
	}

	public void reachedEnd(EndZone zone)
	{
		this.reachedEnd = zone;
	}
}
