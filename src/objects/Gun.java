package objects;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import processing.data.XML;

import m3platformer.Behaviour;
import m3platformer.Layer;
import m3platformer.M3Platformer;

public class Gun extends SolidRectangle implements Collidable
{
	private Behaviour behaviour;
	private int delay;
	private int startDelay;
	private int lastShot;
	private int projectileSpeed;
	//private Dimension2D projectileSize;
	private int lastShotPos;

	public Gun(M3Platformer parent, Behaviour behaviour)
	{
		super(parent, Layer.Layer1, new Color(12, 20, 31), new Color(12, 20, 31), new Color(12, 254, 9), new Color(12, 254, 9),
				265, 310, 25, 25);

		this.behaviour = Behaviour.Left;
		this.delay = 2000;
		this.startDelay = 0;
		this.lastShot = parent.millis() + this.startDelay;
		this.projectileSpeed = 10;
		//this.projectileSize = new Dimensio.
		this.lastShotPos = 0;
	}

	protected Gun(M3Platformer parent)
	{
		super(parent);

		this.lastShot = this.parent.millis() + this.startDelay;
	}

	public List<Projectile> tryShot()
	{
		List<Projectile> projectiles = new ArrayList<Projectile>();
		if (this.delay <= this.parent.millis() - this.lastShot + this.startDelay)
		{
			this.lastShot = this.parent.millis() + this.startDelay;
			
			Behaviour direction;
			
			switch (this.behaviour)
			{
			case Top:
			case Bottom:
			case Left:
			case Right:
				projectiles.add(newProjectile(this.behaviour));
				break;
			case LeftRight:
				projectiles.add(newProjectile(Behaviour.Left));
				projectiles.add(newProjectile(Behaviour.Right));
				break;
			case Opposite:
				projectiles.add(newProjectile(this.lastShotPos == 0 ? Behaviour.Left : Behaviour.Top));
				projectiles.add(newProjectile(this.lastShotPos == 0 ? Behaviour.Right : Behaviour.Bottom));
				this.lastShotPos = this.lastShotPos == 0 ? 1 : 0;
				break;
			case Clockwise:
				direction = Behaviour.Left;
				switch (this.lastShotPos)
				{
				case 0:
					direction = Behaviour.Right;
					this.lastShotPos = 1;
					break;
				case 1:
					direction = Behaviour.Bottom;
					this.lastShotPos = 2;
					break;
				case 2:
					direction = Behaviour.Left;
					this.lastShotPos = 3;
					break;
				case 3:
					direction = Behaviour.Top;
					this.lastShotPos = 0;
					break;					
				}
				projectiles.add(newProjectile(direction));
				break;
			case Anticlockwise:
				direction = Behaviour.Left;
				switch (this.lastShotPos)
				{
				case 0:
					direction = Behaviour.Left;
					this.lastShotPos = 1;
					break;
				case 1:
					direction = Behaviour.Bottom;
					this.lastShotPos = 2;
					break;
				case 2:
					direction = Behaviour.Right;
					this.lastShotPos = 3;
					break;
				case 3:
					direction = Behaviour.Top;
					this.lastShotPos = 0;
					break;					
				}
				projectiles.add(newProjectile(direction));
			}
		}
		return projectiles;
	}

	private Projectile newProjectile(Behaviour direction)
	{
		int pw = 10;
		int ph = 10;

		int w = (int) this.getWidth();
		int h = (int) this.getHeight();
		int x = (int) this.getX();
		int y = (int) this.getY();

		switch (direction)
		{
			case Top:
				x += w / 2 - pw / 2;
				y -= ph;
				break;
			case Bottom:
				x += w / 2 - pw / 2;
				y += h;
				break;
			case Left:
				x -= pw;
				y += h / 2 - ph / 2;
				break;
			case Right:
				x += w;
				y += h / 2 - ph / 2;
			default: break;
		}

		return new Projectile(this.parent, direction, this.projectileSpeed, x, y, pw, ph);
	}

	@Override
	public XML serialize()
	{
		XML res = super.serialize();
		res.setString("behaviour", this.behaviour.toString());
		res.setInt("delay", this.delay);
		if(this.startDelay != 0)
			res.setInt("startDelay", this.startDelay);
		res.setInt("projectileSpeed", this.projectileSpeed);

		return res;
	}

	@Override
	public void deserialize(XML xml)
	{
		super.deserialize(xml);
		this.behaviour = Behaviour.valueOf(xml.getString("behaviour"));
		this.delay = xml.getInt("delay");
		this.startDelay = xml.getInt("startDelay");
		this.projectileSpeed = xml.getInt("projectileSpeed");
	}
}
