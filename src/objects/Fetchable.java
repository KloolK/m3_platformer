package objects;

import objects.PlayerFigure;

public interface Fetchable {
	public void fetch(PlayerFigure player);
}
