package objects;

import java.awt.geom.Rectangle2D;

import m3platformer.Layer;
import m3platformer.M3Platformer;
import m3platformer.Sprite;


public class SolidSpriteObject extends SpriteObject implements Collidable, Movable
{
	// Constructor
	public SolidSpriteObject(M3Platformer parent, Layer layer, Sprite sprite, float x, float y)
	{
		super(parent, layer, sprite, x, y);
	}
	
	protected SolidSpriteObject(M3Platformer parent) {
		super(parent);
	}

	public Rectangle2D getCollisionFrame()
	{
		return getFrame();
	}
	
	public void move() {
		defaultMove();
	}
}
