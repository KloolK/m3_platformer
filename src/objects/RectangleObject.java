package objects;

import java.awt.Color;
import java.util.List;

import m3platformer.ColorScheme;
import m3platformer.ColorWrapper;
import m3platformer.Layer;
import m3platformer.M3Platformer;

public class RectangleObject extends WorldObject implements ColorSchemable
{
	Color fill;
	Color fillC;
	Color border;
	Color borderC;

	public RectangleObject(M3Platformer parent, Layer layer, Color fill, Color fillC, Color border, Color borderC, float x, float y, float w, float h)
	{
		super(parent, layer, x, y, w, h);

		this.fill = fill;
		this.border = border;
		this.fillC = fillC;
		this.borderC = borderC;
	}

	// empty default constructor (used for deserialization)
	protected RectangleObject(M3Platformer parent) {
		super(parent);
	}
	
	@Override
	public void applyColorScheme(ColorScheme c) {
		ColorWrapper w = c.getColorWrapper(this.getClass().getName());
		if(w==null) return;
		
		List<Color> cl = w.getColors();
				
		this.fill = cl.get(0);
		this.fillC = cl.get(1);
		this.border = cl.get(2);
		this.borderC = cl.get(3);
	}
	
	@Override
	protected void drawShape(int x, int y)
	{
		int strokeWeight = 1;
		
		if(this.collision)
			this.parent.fill(this.fillC.getRGB(), this.fillC.getAlpha());
		else
			this.parent.fill(this.fill.getRGB(), this.fill.getAlpha());
		
		if(this.collision)
		{
			//strokeWeight = 2;
			this.parent.stroke(this.borderC.getRGB(), this.borderC.getAlpha());
		}
		else
		{
			this.parent.stroke(this.border.getRGB(), this.border.getAlpha());
		}
		
		this.parent.strokeWeight(strokeWeight);
		
		this.parent.rect((float) x+strokeWeight, (float) y+strokeWeight, this.getWidth()-2*strokeWeight, this.getHeight()-2*strokeWeight);
	}
}
