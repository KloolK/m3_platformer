package objects;

import java.lang.reflect.Constructor;

import m3platformer.M3Platformer;
import processing.data.XML;

public class WorldObjectManager {

	// creates new WorldObject from XML
	// returns null on failure
	public static WorldObject fromXML(M3Platformer parent, XML xml) {
		WorldObject object;
		
		try{
			// create new object
			Class<?> clazz = Class.forName(xml.getName());
			Constructor<?> c = clazz.getDeclaredConstructor(M3Platformer.class);
			object = (WorldObject) c.newInstance(parent);

			// load object data
			object.deserialize(xml);
			
			return object;
		}
		catch (java.lang.ClassNotFoundException e) {} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
}
