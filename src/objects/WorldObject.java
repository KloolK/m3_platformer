package objects;

import m3platformer.Layer;
import m3platformer.M3Platformer;
import m3platformer.Path;
import m3platformer.Serializable;
import m3platformer.Vector2D;

import processing.data.XML;

public abstract class WorldObject extends Drawable implements Serializable
{
	// The parent PApplet that we will render ourselves onto
	M3Platformer parent;

	// semantic definition of position and dimensions inside the map: (see Drawable)
	// upper left corner of object, relative to map coordinates
	// size of object, relative to map coordinates
	
	
	// Layer to render in
	protected Layer layer;
	
	// is affected by physical laws?
	boolean gravityAffected = false;
	boolean frictionAffected = false;
	
	// touching other objects?
	protected boolean tLeft = false;
	protected boolean tRight = false;
	protected boolean tBottom = false;
	protected boolean tTop = false;

	// 
	protected boolean collision = false;
	
	// points to next target point
	private Path moveTarget = null;
	
	// objects moves towards player
	protected boolean moveTowardsPlayer = false;
	private float moveTowardsPlayerSpeed = 0.5f;
	
	// current and maximum speed 2D, speed along path
	protected Vector2D speed = new Vector2D();
	private final Vector2D maxSpeed = new Vector2D(7.5F,JUMP_HEIGHT);
	private float pathSpeed = 0.5f;
	
	// movement constants
	public final static float SPEED_CHANGE = 3.5f;
	public final static float GRAVITY = 1.75f;
	public final static float JUMP_HEIGHT = 16f;
	
	public final static float SLOW_RATE = 2.5f;
	public final static float WALLJUMP_HORIZONTAL = 30.0f;
	public final static float MAX_FALLING_SPEED = 20.0f;
	//public final static int NORMAL_FPS = 25;
	
	// Constructor
	public WorldObject(M3Platformer parent, Layer layer, float x, float y, float w, float h)
	{
		this.position = new Vector2D(x, y);
		this.dimensions = new Vector2D(w, h);

		this.parent = parent;
		this.layer = layer;
	}
	
	// empty default constructor (used for deserialization)
	protected WorldObject(M3Platformer parent) {
		this.parent = parent;
		this.position = new Vector2D();
		this.dimensions = new Vector2D();
	}
		
	// move the object according to its current speed vector
	public void moveByVector(Vector2D v) {
		Vector2D fpsIndep = new Vector2D(v);
		//calculateMovingDistance(fpsIndep);
		this.position.add(fpsIndep);
	}
	
	public void defaultMove() {
		
		// move along path?
		if(moveTarget != null) prepareMoveAlongPath();
		else if(moveTowardsPlayer) prepareMoveTowardsPlayer();

		applyMovementPhysics();
		capSpeed();
		applyCollisionsToVect(speed);
		moveByVector(speed);
	}

	// stop on collision (set parameter to zero)
	public void applyCollisionsToVect(Vector2D v) {
		if(tBottom) v.setY(Math.min(0, v.getY()));
		if(tTop)	v.setY(Math.max(0, v.getY()));
		if(tRight)	v.setX(Math.min(0, v.getX()));
		if(tLeft)	v.setX(Math.max(0, v.getX()));
	}
	
	// secures speed limits
	public void capSpeed() {
		float horizontalSpeed = speed.getX();
		float verticalSpeed = speed.getY();
		
		// Cap speed.
		if(gravityAffected) verticalSpeed = Math.min(verticalSpeed, MAX_FALLING_SPEED);
		verticalSpeed = Math.max(verticalSpeed, -maxSpeed.getY());
		if(frictionAffected && Math.abs(verticalSpeed) < GRAVITY) verticalSpeed = 0;
		
		horizontalSpeed = Math.max(-maxSpeed.getX(),horizontalSpeed);
		horizontalSpeed = Math.min( maxSpeed.getX(),horizontalSpeed);

		speed.setXY(horizontalSpeed, verticalSpeed);
	}
	
	// applies gravity and friction, caps speed, stops on collisions
	public void applyMovementPhysics()
	{
		float verticalSpeed = speed.getY();
		float horizontalSpeed = speed.getX();

		// Gravity
		if(gravityAffected) verticalSpeed += GRAVITY;
		
		// slow down horizontal movement
		if(frictionAffected) {
			if(Math.abs(speed.getX()) < SLOW_RATE) horizontalSpeed = 0;
			else if (speed.getX() > 0)  horizontalSpeed -= SLOW_RATE;
			else if (speed.getX() < 0)  horizontalSpeed += SLOW_RATE;
		}
		
		// update speed vector
		this.speed.setXY(horizontalSpeed, verticalSpeed);
	}

	// calculate speed vector according to current target
	public void prepareMoveAlongPath() {
		
		// no path given?
		if(this.moveTarget == null || this.pathSpeed == 0) return;

		// end of path reached?
		else {
			
			Vector2D dist = new Vector2D(this.moveTarget.getPosition());
			dist.sub(this.position);
			if(dist.getLength() < this.pathSpeed) {
				Path next = this.moveTarget.getNext();
				if(next == null) {
					this.speed.scale(0);
					return;
				}
				else this.moveTarget = next;
			}
		}
		
		// move towards new target
		Vector2D direc = new Vector2D(moveTarget.getPosition());
		direc.sub(this.getPosition());
		direc.scaleToLength(this.pathSpeed);
		
		this.speed.setVect(direc);
	}
	
	// calculate speed vector according to current player position
	public void prepareMoveTowardsPlayer() {
		
		// no path given?
		if(!this.moveTowardsPlayer || this.moveTowardsPlayerSpeed == 0) return;

		// not gravity affected? -> flying around
		if(!gravityAffected) {
			// move towards player position
			Vector2D direc = new Vector2D(M3Platformer.getInstance().getPlayer().getPosition());
			direc.sub(this.getPosition());
			direc.scaleToLength(Math.min(this.moveTowardsPlayerSpeed, direc.getLength()));
			
			this.speed.setVect(direc);
		}
		
		// gravity affected -> only move if touching ground
		else if(getTouchingBottom()) {
			
			Vector2D playerPos = M3Platformer.getInstance().getPlayer().getPosition();
			
			// move towards player position
			Vector2D direc = new Vector2D(playerPos);
			direc.sub(this.getPosition());
			direc.setY(0);
			direc.scaleToLength(Math.min(this.moveTowardsPlayerSpeed, direc.getLength()));
			
			this.speed.setVect(direc);
		}
	}
	
	// accelerate object in a direction
	public void accelerate(Vector2D direction) {
		
		// update speed
		//calculateMovingDistance(direction);
		this.speed.add(direction);
	}
	
	
//	 Calculates the pixel distance independent from FPS.
//	public void calculateMovingDistance(Vector2D distance)
//	{
//		distance.scale(NORMAL_FPS / this.parent.frameRate);
//	}
		
	
	// serialization
	public XML serialize() {
		XML res = new XML(this.getClass().getName());
		res.setFloat("x", this.getX());
		res.setFloat("y", this.getY());
		res.setFloat("w", this.getWidth());
		res.setFloat("h", this.getHeight());
		res.setString("layer", this.getLayer().toString());

		if(getMoveTowardsPlayer()) {
			res.setInt("moveTowardsPlayer", 1);
			res.setFloat("moveTowardsPlayerSpeed", moveTowardsPlayerSpeed);
		}

		if(gravityAffected) res.setInt("gravityAffected", 1);
		
		if(this.moveTarget != null) {
			res.addChild(moveTarget.serialize("moveTarget"));
			res.setFloat("pathSpeed", pathSpeed);
		}
		
		return res;
	}
	
	public void deserialize(XML xml) {
		this.position.setXY(xml.getFloat("x"), xml.getFloat("y"));
		this.dimensions.setXY(xml.getFloat("w"), xml.getFloat("h"));
		this.layer = Layer.valueOf(xml.getString("layer"));
		this.moveTowardsPlayer = xml.getInt("moveTowardsPlayer") == 1;
		this.moveTowardsPlayerSpeed = xml.getFloat("moveTowardsPlayerSpeed");
		this.gravityAffected = xml.getInt("gravityAffected") == 1;
		
		XML path = xml.getChild("moveTarget");
		if(path != null) {
			this.moveTarget = Path.deserialize(path);
			this.pathSpeed = xml.getFloat("pathSpeed");
		}
	}
	
	
	// getters and setters
	
	public Vector2D getSpeed() {return new Vector2D(this.speed);}
	
	public void setMovePath(Path target) {this.moveTarget = target;}
	public Path getMovePath() { return this.moveTarget; }
	public void setPathSpeed(float pathSpeed) { this.pathSpeed = pathSpeed; }
	
	public Layer getLayer() {return this.layer;}
	
	public boolean getTouchingLeft() {return tLeft;}
	public boolean getTouchingRight() {return tRight;}
	public boolean getTouchingBottom() {return tBottom;}
	public boolean getTouchingTop() {return tTop;}
	public void setTouchingLeft(boolean t) {this.tLeft = t;}
	public void setTouchingRight(boolean t) {this.tRight = t;}
	public void setTouchingBottom(boolean t) {this.tBottom = t;}
	public void setTouchingTop(boolean t) {this.tTop = t;}
	public void resetTouching() {tLeft = tRight = tBottom = tTop = false;}
	
	public void setCollision(boolean c) { this.collision = c; }
	public boolean getCollision() { return this.collision; }
	
	public boolean isGravityAffected() {return gravityAffected;}
	
	public boolean getMoveTowardsPlayer() {return this.moveTowardsPlayer;}
	public void setMoveTowardsPlayer(boolean p) {this.moveTowardsPlayer = p;}
	
}
