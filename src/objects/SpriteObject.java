/*
 *  WorldObject class.
 *  Represents an object in the map.
 *  
 *  Multimodal Media Madness RWTH 08.06.2012
 *  Jakob Bauer, Jan Bruckner, Michael Deutschen
 */

package objects;

import java.util.List;

import m3platformer.ColorScheme;
import m3platformer.Layer;
import m3platformer.M3Platformer;
import m3platformer.Sprite;

import processing.data.XML;

public abstract class SpriteObject extends WorldObject implements ColorSchemable
{
	// Sprite representing the object
	Sprite sprite;
	
	// Constructor
	public SpriteObject(M3Platformer parent, Layer layer, Sprite sprite, float x, float y)
	{
		super(parent, layer, x, y, sprite.getWidth(), sprite.getHeight());

		setSprite(sprite);
	}
	
	// empty constructor for deserialization
	protected SpriteObject(M3Platformer parent) {
		super(parent);
	}

	// draw the object at the specified position
	protected void drawShape(int x, int y)
	{
		// this.sprite.setPositionX(x);
		// this.sprite.setPositionY(y);

		this.sprite.drawWithoutRotation(x, y);
	}
	
	@Override
	public void applyColorScheme(ColorScheme c) {
		// load sprites out of color scheme
		List<Sprite> spList = c.getSpriteList(this.getClass().getName());
		if(spList == null || spList.size()==0) return;
		
		setSprite(spList.get(0));
	}
	
	public SpriteObject setSprite(Sprite s) {
		this.sprite = s;
		this.dimensions.setXY(s.getWidth(), s.getHeight());
		
		return this;
	}
	
	@Override
	public XML serialize() {
		XML res = super.serialize();
		res.addChild(this.sprite.serialize());
		return res;
	}
	
	@Override
	public void deserialize(XML xml) {
		super.deserialize(xml);
		if(xml.getChild(Sprite.class.getName())!=null) {
			setSprite(Sprite.deserialize(this.parent, xml.getChild(Sprite.class.getName())));
		}
	}
}
