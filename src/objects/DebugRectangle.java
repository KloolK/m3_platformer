package objects;

import java.awt.Color;
import java.awt.geom.Rectangle2D;

import m3platformer.Layer;
import m3platformer.M3Platformer;

public class DebugRectangle extends WorldObject
{
	Color color;

	public DebugRectangle(M3Platformer parent, Layer layer, Color color, float x, float y, float w, float h)
	{
		super(parent, layer, x, y, w, h);

		this.color = color;
	}

	public DebugRectangle(M3Platformer parent, Layer layer, Color color, Rectangle2D rectangle)
	{
		super(parent, layer, (float)rectangle.getX(), (float)rectangle.getY(), (float)rectangle.getWidth(), (float)rectangle.getHeight());

		this.parent = parent;
		this.color = color;
	}

	@Override
	protected void drawShape(int x, int y)
	{
		this.parent.fill(this.color.getRed(), this.color.getGreen(), this.color.getBlue());
		this.parent.rect((float)x, (float)y, getWidth(), getHeight());
	}
}
