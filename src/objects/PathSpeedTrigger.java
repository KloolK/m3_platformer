package objects;

import java.awt.Color;

import processing.data.XML;
import m3platformer.Layer;
import m3platformer.M3Platformer;

public class PathSpeedTrigger extends RectangleZone
{
	private WorldObject triggeredObject;
	private float targetSpeed;
	
	public PathSpeedTrigger(M3Platformer parent, WorldObject trigerredObject, float targetSpeed, float x, float y, float w, float h)
	{
		super(parent, Layer.Layer1, new Color(12, 20, 31), new Color(12, 20, 31), new Color(12, 254, 9), new Color(12, 254, 9), x, y, w, h);
		
		this.triggeredObject = trigerredObject;
		this.targetSpeed = targetSpeed;
	}
	
	protected PathSpeedTrigger(M3Platformer parent)
	{
		super(parent);
	}
	
	public void onTriggered()
	{
		if(this.triggeredObject != null)
			this.triggeredObject.setPathSpeed(this.targetSpeed);
	}
	
	public WorldObject getTriggeredObject()
	{
		return this.triggeredObject;
	}
	
	@Override
	public XML serialize() {
		XML res = super.serialize();
		res.setFloat("targetSpeed", this.targetSpeed);
		
		if(this.triggeredObject != null) res.addChild(this.triggeredObject.serialize());	
		
		return res;
	}
	
	@Override
	public void deserialize(XML xml) {
		super.deserialize(xml);
		this.targetSpeed = xml.getFloat("targetSpeed");
		for(XML child : xml.getChildren()) {
			this.triggeredObject = WorldObjectManager.fromXML(this.parent, child);
			if(this.triggeredObject != null) break;
		}
	}
}
