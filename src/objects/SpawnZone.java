package objects;

import java.awt.Color;

import processing.data.XML;

import m3platformer.Layer;
import m3platformer.M3Platformer;

public class SpawnZone extends RectangleZone
{
	public SpawnZone(M3Platformer parent, float x, float y, float w, float h)
	{
		super(parent, Layer.Invisible, new Color(12, 20, 31), new Color(12, 20, 31), new Color(255, 230, 77), new Color(255, 230, 77), x, y, w, h);
	}

	// empty default constructor (used for deserialization)
	protected SpawnZone(M3Platformer parent)
	{
		super(parent);
	}
	
	@Override
	public void deserialize(XML xml) {
		super.deserialize(xml);
		
		this.layer = Layer.Invisible;
	}
}
