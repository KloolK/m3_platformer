package objects;

import m3platformer.M3Platformer;
import m3platformer.Vector2D;
import m3platformer.animations.Animation;
import m3platformer.Sprite;
import m3platformer.TextSprite;

import java.awt.geom.Rectangle2D;
import java.util.*;

public abstract class Drawable {

	// list of active animations (all running at the same time)
	private final List<Animation> activeAnimations = new LinkedList<Animation>();
	
	// position and dimensions, semantics are defined in subclasses
	protected Vector2D position = new Vector2D(0,0);
	protected Vector2D dimensions= new Vector2D(0,0);

	protected abstract void drawShape(int x, int y);

	// this method should be called in every tick
	// draw object at position x,y (relative to render window)
	public final void draw(int x, int y) {
		
		Vector2D pos = new Vector2D(x,y);
		boolean doDraw = true;
		Rectangle2D frame = this.getFrame();
		M3Platformer applet = M3Platformer.getInstance();
		
		// if there are any animations, normalize drawing position
		boolean translated = activeAnimations.size()>0;
		if(translated) {
			applet.pushMatrix();
			applet.translate(x+(float)frame.getWidth()/2, y+(float)frame.getHeight()/2);
			pos.setXY(-(float)frame.getWidth()/2, -(float)frame.getHeight()/2);
			if(this instanceof Sprite || this instanceof TextSprite) {
				applet.translate(this.getX(), this.getY());
				pos.add(-this.getX(), -this.getY());
			}
		}
		
		// apply pre drawing stuff
		for(Animation a: activeAnimations) {
			doDraw = a.applyPreDraw(this, pos) && doDraw;
		}
		
		// draw
		if(doDraw) drawShape((int)pos.getX(), (int)pos.getY());
		
		// post drawing stuff
		for(Animation a : new LinkedList<Animation>(activeAnimations)) {
			if(a.applyPostDraw(this, pos)) {
				// animation finished
				activeAnimations.remove(a);
				
				// following animation?
				if(a.getFollowUp() != null){
					a.getFollowUp().resetStartTime();
					activeAnimations.add(a.getFollowUp());
				}
			}
		}
		
		// restore drawing position
		if(translated) {
			applet.popMatrix();
		}
	}
	
	// Check if an animation of the same instance is already played.
	public boolean isAnimationPlayed(Animation animation)
	{
		for (Animation a : this.activeAnimations)
		{
			if (a.getClass() == animation.getClass())
				return true;
		}
		return false;
	}

	
	public void addAnimation(Animation a) {
		activeAnimations.add(a);
	}
	
	public void removeAnimation(Animation a) {
		activeAnimations.remove(a);
	}
	
	public void clearAnimations() {
		activeAnimations.clear();
	}
	
	public void restartFirstAnimation() {
		if(this.activeAnimations.size() > 0)
			this.activeAnimations.get(0).resetStartTime();
	}
	

	
	public Vector2D getPosition() {return new Vector2D(this.position);}
	public void setPosition(float x, float y) {this.position.setXY(x, y);}
	
	public Vector2D getDimensions() {return new Vector2D(this.dimensions);}
	public Rectangle2D getFrame() {return new Rectangle2D.Float(this.getX(), this.getY(), this.getWidth(), this.getHeight());}

	public float getX() {return this.position.getX();}
	public float getY() {return this.position.getY();}
	public float getWidth() {return this.dimensions.getX();}
	public float getHeight() {return this.dimensions.getY();}
	public void setWidth(int width) { this.dimensions.setX(width); }
	public void setHeight(int height) { this.dimensions.setY(height); }
}
