package objects;

import java.awt.Color;

import m3platformer.Layer;
import m3platformer.M3Platformer;
import processing.data.XML;

public class TriggerZone extends RectangleZone 
{
	// Stores the id of the trigger.
	private WorldObject worldObject;
	
	// The amount of objects that can be created by the trigger.
	private int maxSpawns;
	// The current amount of objects that have been created by the trigger.
	private int numberOfSpawns;
	
	public TriggerZone(M3Platformer parent, WorldObject worldObject, int maxSpawns, float x, float y, float w, float h)
	{
		super(parent, Layer.Layer1, new Color(12, 20, 31), new Color(12, 20, 31), new Color(12, 254, 9), new Color(12, 254, 9), x, y, w, h);
		
		this.worldObject = worldObject;
		this.maxSpawns = maxSpawns;
		this.numberOfSpawns = 0;
	}
	
	protected TriggerZone(M3Platformer parent) {
		super(parent);
		this.parent = parent;
	}
	
	public WorldObject getWorldObject()
	{
		if (this.numberOfSpawns < this.maxSpawns)
		{
			this.numberOfSpawns++;
			return this.worldObject;
		}
		else
			return null;
	}
	
	//reset numerOfSpawns
	public void reset()
	{
		this.numberOfSpawns = 0;
	}
	
	@Override
	public XML serialize() {
		XML res = super.serialize();
		res.setInt("maxSpawns", this.maxSpawns);
		
		if(this.worldObject != null) res.addChild(this.worldObject.serialize());	
		
		return res;
	}
	
	@Override
	public void deserialize(XML xml) {
		super.deserialize(xml);
		this.maxSpawns = xml.getInt("maxSpawns");
		for(XML child : xml.getChildren()) {
			this.worldObject = WorldObjectManager.fromXML(this.parent, child);
			if(this.worldObject != null) break;
		}
	}
}
