package objects;

import java.awt.geom.Rectangle2D;

import m3platformer.Behaviour;
import m3platformer.M3Platformer;

public class Projectile extends DeathZone implements Movable, Collidable
{
	private Behaviour behaviour;
	private int projectileSpeed;
	
	public Projectile(M3Platformer parent, Behaviour behaviour, int projectileSpeed, float x, float y, float w, float h)
	{
		super(parent, x, y, w, h);
		
		this.behaviour = behaviour;
		this.projectileSpeed = projectileSpeed;
	}

	@Override
	public void move()
	{
		float x = this.getX();
		float y = this.getY();
		
		switch (this.behaviour)
		{
		case Top:
			y -= this.projectileSpeed;
			break;
		case Bottom:
			y += this.projectileSpeed;
			break;
		case Left:
			x -= this.projectileSpeed;
			break;
		case Right:
			x += this.projectileSpeed;
			break;
		default: break;
		}
		
		this.setPosition(x, y);
	}

	@Override
	public Rectangle2D getCollisionFrame()
	{
		return new Rectangle2D.Float(this.getX(), this.getY(), this.getWidth(), this.getHeight());
	}
}
