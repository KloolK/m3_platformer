package objects;

import java.awt.Color;
import java.awt.geom.Rectangle2D;

import m3platformer.Layer;
import m3platformer.M3Platformer;


public class SolidRectangle extends RectangleObject implements Collidable, Movable
{	
	public SolidRectangle(M3Platformer parent, Layer layer, Color fill, Color fillC, Color border, Color borderC, float x, float y, float w, float h)
	{
		super(parent, layer, fill, fillC, border, borderC, x, y, w, h);
	}
	
	// empty default constructor (used for deserialization)
	protected SolidRectangle(M3Platformer parent) {
		super(parent);
	}

	@Override
	public Rectangle2D getCollisionFrame()
	{
		return getFrame();
	}
	
	public void move() {
		defaultMove();
	}
}