package objects;

import java.awt.geom.Rectangle2D;

import processing.data.XML;

import m3platformer.AnimatedSprite;
import m3platformer.Layer;
import m3platformer.M3Platformer;
import m3platformer.Zone;

public class EndZone extends AnimatedSpriteObject implements Zone
{
	public EndZone(M3Platformer parent, float x, float y, float w, float h)
	{
		super(parent, Layer.Layer2, new AnimatedSprite(parent, "data/imgs/portals/blueAnim.gif"), x, y);
	}

	// empty default constructor (used for deserialization)
	protected EndZone(M3Platformer parent)
	{
		super(parent);
	}

	@Override
	public Rectangle2D getZoneFrame() {
		return getFrame();
	}
	
	@Override
	public void deserialize(XML xml) {
		super.deserialize(xml);
		
		this.layer = Layer.Layer2;
	}
}
