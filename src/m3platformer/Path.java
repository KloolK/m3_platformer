package m3platformer;

import processing.data.XML;

public class Path {
	private Vector2D position = new Vector2D();
	protected Path next = null;
	private Path prev = null;
	protected boolean turnIfEnd = false;
	
	public Path(float x, float y, Path next, Path prev, boolean turn) {
		this.position.setXY(x,y);
		this.next = next;
		this.prev = prev;
		this.turnIfEnd = turn;
	}
	
	// recursive method to exchange prev and next on each path object
	// expects prev or next to be null
	public void invertPath() {
		if(this.next != null) invertPath(true);
		else if(this.prev != null) invertPath(false);
	}
	// auxiliary method
	private void invertPath(boolean forward) {
		if(forward && this.next != null) this.next.invertPath(true);
		else if(!forward && this.prev != null) this.prev.invertPath(false);
		
		Path tmp = this.next;
		this.next = this.prev;
		this.prev = tmp;
	}
	
	// move current node to target position and take all other nodes with it (relatively)
	public void moveToPosition(Vector2D pos) {
		Vector2D direc = new Vector2D(pos);
		direc.sub(this.getPosition());
		
		this.move(direc);
	}
	
	// move complete path via direc
	public void move(Vector2D direc) {
		// move following
		if(this.next != null) this.next.move(direc, true);
		
		// move myself and previous points
		this.move(direc, false);
	}
	// auxiliary method
	private void move(Vector2D direc, boolean forward) {
		if(forward && this.next!=null) this.next.move(direc, true);
		if(!forward && this.prev!=null) this.prev.move(direc, false);
		
		this.position.add(direc);
	}

	// serialize path in wrapper with given name:
	// <[NAME]>
	// 	 <m3platformer.Path .... />
	//	 <m3platformer.Path .... />
	//	 ...
	// </[NAME]>
	public XML serialize(String name) {
		XML res = new XML(name);
		Path cur = this;
		boolean first = true;
		while(cur != null) {
			
			// looks familiar.... we've been here before
			if(!first && cur == this) {
				res.setInt("cycle", 1);
				break;
			}
			
			if(first) first = false;
			
			XML child = new XML(cur.getClass().getName());
			child.setFloat("x", cur.getPosition().getX());
			child.setFloat("y", cur.getPosition().getY());
			child.setInt("turn", cur.turnIfEnd ? 1 : 0);
			res.addChild(child);
			
			cur = cur.next; // not getNext(), because of possible turning at end
		}
		
		return res;
	}
	
	// deserializes complete path from XML (same form as serialize)
	// returns first path object
	public static Path deserialize(XML xml) {
		XML[] paths = xml.getChildren();
		Path first = null;
		Path recent = null;
		for(XML x : paths) {
			if(x.getName() != Path.class.getName()) continue;
			
			Path cur = new Path(x.getFloat("x"),x.getFloat("y"), null, null, x.getInt("turn") == 1);
			cur.setPrev(recent);
			
			if(recent != null) recent.setNext(cur);
			if(first == null) first = cur;
			
			recent = cur;
		}
		
		// cyclic path
		if(xml.getInt("cycle") == 1) {
			first.setPrev(recent);
			recent.setNext(first);
		}
				
		return first;
	}
	
	public String toString() {
		return toString(0);
	}
	
	public String toString(int depth) {
		if(depth >= 20) return "";
		return "Path["+this.getPosition().getX()+","+this.getPosition().getY()+"]" + (this.next != null ? " > " + this.next.toString(depth+1) : "");
	}
	
	// getters and setters
	public Vector2D getPosition() {return position;}
	public void setPosition(float x, float y) {	this.position.setXY(x, y); }
	
	public void setNext(Path p) {this.next = p;}
	public void setPrev(Path p) {this.prev = p;}

	public Path getNext() {
		if(turnIfEnd && next == null) {
			this.invertPath();
		}

		return next;
	}
	public Path getPrev() {
		if(turnIfEnd && prev == null) {
			this.invertPath();
		}

		return prev;
	}
	
	public Path getNextRaw()
	{
		return this.next;
	}
	
	public Path getPrevRaw()
	{
		return this.prev;
	}
}
