package m3platformer;

public enum ControllerKey {
	// Player 1.
	w,
	s,
	a,
	d,
	one,
	two,
	q,
	e,
	r,
	z,
	x,
	c,

	// Player 2.
	i,
	k,
	j,
	l,
	eight,
	nine,
	t,
	y,
	u,
	b,
	n,
	m,

	// for map editor
	up,
	down,
	left,
	right,
	plus,
	minus,
	del,
	shift,
	control;
}
