package m3platformer;

import java.awt.geom.Rectangle2D;
import java.util.*;

import objects.Coin;
import objects.Collidable;
import objects.DeathZone;
import objects.EndZone;
import objects.Fetchable;
import objects.Gun;
import objects.Movable;
import objects.PathSpeedTrigger;
import objects.PlayerFigure;
import objects.Projectile;
import objects.RectangleZone;
import objects.TriggerZone;
import objects.Figure;
import objects.WorldObject;

public class Physics
{
	// The current map
	private GameMap map;

	// The controller class
	Controller controller;

	// The player Figure
	protected PlayerFigure player;
	
	// temporary list for objects to be removed
	protected Set<WorldObject> objectsToRemove = new HashSet<WorldObject>();

	// The parent PApplet
	M3Platformer parent;

	public Physics(M3Platformer parent, Controller controller, GameMap map, PlayerFigure player)
	{
		this.parent = parent;
		this.controller = controller;
		this.setMap(map);
		this.player = player;
	}

	public void tick(boolean withoutPlayer)
	{
		// Reset Collision bools
		for (WorldObject object : this.map.getWorldObjects())
		{
			object.setCollision(false);
		}
		
		// Do player movement
		if(!withoutPlayer)
			this.movePlayer();
		
		// move other movable objects
		for (WorldObject object : this.getMap().getWorldObjects())
		{
			if (object instanceof Movable)
			{
				Vector2D oldPos = object.getPosition();
				((Movable) object).move();
				
				// object didnt move
				if(object.getPosition().equals(oldPos)) continue;
				
				if(object instanceof Collidable) {
					this.unstuck((Collidable)object);
				}
			}
		}
		
		checkGunsAndProjectiles();
		
		if(!withoutPlayer) {
			// kill the player when he is outside of the map
			if(!CollisionDetection.RectRect(this.player.getCollisionFrame(), this.map.getSize()))
				M3Platformer.getInstance().getLogic().killPlayer();
			
			// Collect Coins
			CheckCoinCollision(this.player);
			
			// check player collisions
			this.unstuck(this.player);

			// Check Zone Collision
			if (this.CheckCollisionDeathZone(this.player)) {
				M3Platformer.getInstance().getLogic().killPlayer();
				return;
			}
			
			EndZone zone = this.CheckCollisionEndZone(this.player);
			if (zone != null) {
				this.player.reachedEnd(zone);
				return;
			}			
			
			// Check for triggerZone.
			WorldObject worldObject = this.CheckCollisionTriggerZone(this.player);
			if (worldObject != null) getMap().addWorldObject(worldObject);
			
			// Trigger PathSpeedTriggers if player collides with them
			checkPathSpeedTriggers(this.player);
		}
	}

	// move the player figure.
	protected void movePlayer()
	{
		this.player.move();
	}
	
	private void unstuck(Collidable collider) {
		unstuck(collider, 0);
		
		for(WorldObject o : objectsToRemove) {
			getMap().removeWorldObject(o);
		}
		objectsToRemove.clear();
	}

	private void unstuck(Collidable subject, int recDepth) {
		// we can be sure that it is a WorldObject
		WorldObject collider = (WorldObject) subject;
		boolean constSpeedSet = false;
				
		collider.resetTouching();
		Rectangle2D colliderFrame = subject.getCollisionFrame();

		for (WorldObject object : this.getMap().getWorldObjects()) {
			if(object == subject) continue;
			if(object.getLayer() == Layer.Background) continue;
			
			// we will check player collisions later
			if(object instanceof PlayerFigure) continue;
						
			if (object instanceof Collidable) {
				// do not unstuck out of collidable zones (simple enemies etc.)
				if(subject instanceof PlayerFigure && object instanceof Zone) continue;
				
				Rectangle2D frame = ((Collidable) object).getCollisionFrame();
				CollisionResult res = CollisionDetection.getCollisionVector(colliderFrame, frame);
				
				// no collision at all
				if(res == null)
				{
					//object.setCollision(false);
					continue;
				}

				// collision has happened or subject touches object
				else {
					if(subject instanceof PlayerFigure || subject instanceof Projectile) 
						object.setCollision(true);
					
					((WorldObject)subject).setCollision(true);
					
					// object is fetchable item
					if(object instanceof Fetchable && subject instanceof PlayerFigure) {
						this.doFetch((Fetchable) object);
						
						continue;
					}
					
					// real collision -> move subject out of object
					if(!res.touching) {
						
						
						// subject is moving? -> use movement speed to determine where it came from
						Vector2D totalSpeed = new Vector2D(collider.getSpeed());
						if(collider instanceof PlayerFigure) totalSpeed.add(((PlayerFigure)collider).getConstSpeed());
						
						// subject could have moved from outside object over center of it in a single tick
						if(totalSpeed.getY() == 0 || Math.abs(totalSpeed.getX()) > object.getWidth() / 2) {
							
							// correct direction
							if(Math.copySign(1, totalSpeed.getX()) != Math.copySign(1, res.depth.get(0)))
								collider.setPosition(collider.getX() + res.depth.get(0), collider.getY());
							
							// wrong direction (moving through object)
							else {
								collider.setPosition(collider.getX() - res.depth.get(0) - object.getWidth()/2, collider.getY());
							}
						}
						
						else if(totalSpeed.getX() == 0 || Math.abs(totalSpeed.getY()) > object.getHeight() / 2) {
							// correct direction
							if(Math.copySign(1, totalSpeed.getY()) != Math.copySign(1, res.depth.get(1)))
								collider.setPosition(collider.getX(), collider.getY() + res.depth.get(1));
							
							// wrong direction (moving through object)
							else {
								collider.setPosition(collider.getX(), collider.getY() - res.depth.get(1) - object.getHeight()/2);
							}
						}
						
						// subject could not have moved over the center in a single tick
						// -> determine direction by smaller intersection
						else {
							// collision on y-axis
							if (Math.abs(res.depth.get(0)) > Math.abs(res.depth.get(1)))
								collider.setPosition(collider.getX(), collider.getY() + res.depth.get(1));
								
							// collision on x-axis
							else  collider.setPosition(collider.getX() + res.depth.get(0), collider.getY());
						}
						
						
						// TODO: this is just too dirty, is there a better solution?
						// more than 5 collisions after each other? -> probably endless recursion (subject stuck between two objects)
						// resolve collision in the other direction
						if(recDepth > 5) {
							// collision on y-axis -> move an x axis
							if (Math.abs(res.depth.get(0)) > Math.abs(res.depth.get(1)))
								collider.setPosition(collider.getX() + res.depth.get(0), collider.getY());
								
							// collision on x-axis -> move on y axis
							else  collider.setPosition(collider.getX(), collider.getY() + res.depth.get(1));
						}
						
						// recheck all objects for new collisions, maybe caused by recent repositioning of subject
						else {
							this.unstuck(subject, recDepth+1);
							return;
						}
					}
					
					// set collision parameters
					if (res.depth.get(0) < 0) 		collider.setTouchingRight(true);
					else if (res.depth.get(0) > 0) 	collider.setTouchingLeft(true);
					if (res.depth.get(1) < 0) 		collider.setTouchingBottom(true);
					else if (res.depth.get(1) > 0)	collider.setTouchingTop(true);
					
					// TODO: generalize for all objects, not only for player
					// if object is moving horizontally and collider is on it, it has to move along with it
					if(collider instanceof PlayerFigure) {
						PlayerFigure p = (PlayerFigure) collider;
						if(res.depth.get(1) < 0 && object instanceof Movable) {
							if(object.getSpeed().getLength() != 0) {
								p.setConstSpeed(new Vector2D(object.getSpeed().getX(), object.getSpeed().getY()));
								constSpeedSet = true;
							}
						}
					}
				}
			}
		}
		
		// reset constant speed of player
		if(collider instanceof PlayerFigure && !constSpeedSet) {
			PlayerFigure p = (PlayerFigure) collider;
			p.resetConstSpeed();
		}
	}	
	
	protected void doFetch(Fetchable obj) {
		obj.fetch(player);
		objectsToRemove.add((WorldObject)obj);
	}
	
	private boolean CheckCollisionDeathZone(Figure figure)
	{
		Rectangle2D figureFrame = figure.getCollisionFrame();

		for (WorldObject object : this.getMap().getWorldObjects())
		{
			if (object instanceof DeathZone)
			{
				Rectangle2D zoneFrame = ((DeathZone) object).getZoneFrame();
				if (CollisionDetection.RectRect(zoneFrame, figureFrame))
				{
					return true;
				}
			}
		}
		return false;
	}

	private void checkPathSpeedTriggers(Figure figure)
	{
		Rectangle2D figureFrame = figure.getCollisionFrame();

		for (WorldObject object : this.getMap().getWorldObjects())
		{
			if (object instanceof PathSpeedTrigger)
			{
				Rectangle2D zoneFrame = ((PathSpeedTrigger) object).getZoneFrame();
				if (CollisionDetection.RectRect(zoneFrame, figureFrame))
				{
					((PathSpeedTrigger)object).onTriggered();
				}
			}
		}
	}
	
	private void checkGunsAndProjectiles()
	{
		// check Guns
		List<Projectile> newProjectiles = new ArrayList<Projectile>();
		for (WorldObject object : this.getMap().getWorldObjects())
		{
			if (object instanceof Gun)
			{
				List<Projectile> projectiles = ((Gun) object).tryShot();
				if (projectiles.size() > 0)
				{
					newProjectiles.addAll(projectiles);
				}
			}
		}
		for (Projectile projectile : newProjectiles)
		{
			this.map.addWorldObject(projectile);
		}

		// check Projectiles
		List<WorldObject> removeProjectiles = new ArrayList<WorldObject>();
		for (WorldObject object : this.getMap().getWorldObjects())
		{
			if (object instanceof Projectile)
			{				
				if (object.getCollision() || !CollisionDetection.RectRect(((Projectile) object).getCollisionFrame(), this.map.getSize()))
				{
					removeProjectiles.add(object);
				}
			}
		}
		for (WorldObject projectile : removeProjectiles)
		{
			this.map.removeWorldObject(projectile);
		}
	}
	
	private void CheckCoinCollision(PlayerFigure figure)
	{
		Rectangle2D figureFrame = figure.getCollisionFrame();

		for (WorldObject object : this.getMap().getWorldObjects())
		{			
			if (object instanceof Coin)
			{
				Rectangle2D zoneFrame = ((RectangleZone) object).getZoneFrame();
				if (CollisionDetection.RectRect(zoneFrame, figureFrame))
				{
					this.doFetch((Fetchable)object);
				}
			}
		}
	}
	
	private EndZone CheckCollisionEndZone(Figure figure)
	{
		Rectangle2D figureFrame = figure.getCollisionFrame();

		for (WorldObject object : this.getMap().getWorldObjects())
		{
			if (object instanceof EndZone)
			{
				Rectangle2D zoneFrame = ((EndZone) object).getZoneFrame();
				if (CollisionDetection.RectRect(zoneFrame, figureFrame))
				{
					return (EndZone)object;
				}
			}
		}
		return null;
	}

	// Check if a trigger triggered :D
	// Return WorldObject if true, else return null.
	// TODO: collision with more than one zone in the same tick?
	private WorldObject CheckCollisionTriggerZone(Figure figure)
	{
		Rectangle2D figureFrame = figure.getCollisionFrame();

		for (WorldObject object : this.getMap().getWorldObjects())
		{
			if (object instanceof TriggerZone)
			{
				Rectangle2D zoneFrame = ((TriggerZone) object).getZoneFrame();
				if (CollisionDetection.RectRect(zoneFrame, figureFrame))
				{
					return ((TriggerZone) object).getWorldObject();
				}
			}
		}
		return null;
	}

	public GameMap getMap() {
		return map;
	}

	public void setMap(GameMap map) {
		this.map = map;
	}
}
