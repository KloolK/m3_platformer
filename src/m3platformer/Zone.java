package m3platformer;

import java.awt.geom.Rectangle2D;

public interface Zone
{
	public Rectangle2D getZoneFrame();
}
