package m3platformer;

import java.io.File;

import objects.Block;
import objects.Coin;
import objects.DeathZone;
import objects.EndZone;
import objects.Gun;
import objects.PathSpeedTrigger;
import objects.PlayerFigure;
import objects.SimpleEnemy;
import objects.SpawnZone;
import objects.TriggerZone;
import objects.WorldObject;
import processing.core.PApplet;

public class MapEditor
{
	M3Platformer parent;
	GameMap map;
	Camera camera;
	Controller controller;

	WorldObject currentObject;
	Path currentPath;
	CameraPath currentCamPath;

	Menu menu = null;
	StringInput input = null;

	private final int dimensionChangeSlow = 5;
	private final int dimensionChangeFast = 20;
	private final int dimensionChangeFastest = 100;
	private final int gridSize = 5;

	private String currentFile = null;
	private int width = 25;
	private int height = 25;
	private int typeIndex = 0;
	private int dimensionChange = dimensionChangeSlow;
	private float scale = 1.0F;
	private float scaleChange = 0.05F;
	private boolean selectingObject = false; //object selection mode
	private boolean selectingPath = false; //path selection mode
	private boolean selectingCamPath = false; //CameraPath selection mode
	private boolean selectingMap = false;
	private boolean selectingName = false;
	private boolean isNewObject = false; //current object is new
	private boolean isNewPath = false; //current path point is new
	private boolean isNewCamPath = false; //current CameraPath point is new
	private boolean pathMode = false; //in path creation mode
	private boolean camPathMode = false; //in CameraPath creation mode
	private boolean mouseReleased = true;

	private enum ObjectType
	{
		Block, DeathZone, SpawnZone, EndZone, Coin, Gun, PathSpeedTrigger, TriggerZone, SimpleEnemy
	}

	public MapEditor(M3Platformer parent, Controller controller, PlayerFigure player, int drawX, int drawY, int w, int h)
	{
		this.parent = parent;
		this.controller = controller;
		this.map = new GameMap(500, 500, ColorScheme.getDefaultScheme());
		this.camera = new Camera(this.parent, this.map, player, drawX, drawY, w, h);

		newObject();
	}

	public void tick()
	{
		if (this.controller.getKeyJustPressed(ControllerKey.shift))
		{
			if (!this.selectingMap)
			{
				if (dimensionChange == dimensionChangeSlow)
					dimensionChange = dimensionChangeFast;
				else if (dimensionChange == dimensionChangeFast)
					dimensionChange = dimensionChangeFastest;
				else if (dimensionChange == dimensionChangeFastest)
					dimensionChange = dimensionChangeSlow;
			}
		}
		if (this.controller.getKeyJustPressed(ControllerKey.control))
		{
			this.selectingMap = !this.selectingMap;
			if (this.selectingMap)
				createMapMenu();
		}
		if (this.controller.getKeyJustPressed(ControllerKey.two))
		{
			this.camPathMode = !this.camPathMode;
			if (this.camPathMode)
			{
				if (isNewObject)
					delObject();
				newCamPath();
			}
			else
			{
				if (isNewCamPath)
					delCamPath();
				this.currentCamPath = null;
			}
		}
		if (this.controller.getKeyJustPressed(ControllerKey.nine))
		{
			this.pathMode = !this.pathMode;
			if (this.pathMode)
			{
				if (isNewObject)
					delObject();
				newPath();
				if (this.currentPath.getPrev() == null)
				{
					this.currentObject.setMovePath(this.currentPath);
				}
			}
			else
			{
				if (isNewPath)
					delPath();
				this.currentPath = null;
			}
		}
		if (this.parent.mouseButton == PApplet.LEFT)
		{
			if (this.mouseReleased)
			{
				this.mouseReleased = false;

				//if in selection mode, end selection mode. otherwise create a new object/path
				if (!pathMode && !camPathMode)
				{
					if (selectingObject)
						selectingObject = false;
					else
						this.newObject();
				}
				else if (this.pathMode)
				{
					if (selectingPath)
						selectingPath = false;
					else
						this.newPath();
				}
				else
				{
					if (selectingCamPath)
						selectingCamPath = false;
					else
						this.newCamPath();
				}
			}
		}
		else
		{
			this.mouseReleased = true;
		}
		if (this.parent.mouseButton == PApplet.RIGHT)
		{
			if (!pathMode && selectingObject)
			{
				selectingObject = false;
				this.newObject();
			}
		}
		if (this.controller.getKeyJustPressed(ControllerKey.s))
		{
			if (this.selectingName)
				this.input.down();
			else if (!this.selectingMap)
			{
				if (this.camPathMode)
				{
					this.scale -= this.scaleChange;
					if (this.scale < 0)
						this.scale = 0;
					this.currentCamPath.setScale(this.scale);
				}
				else
				{
					this.height += this.dimensionChange;
					this.currentObject.setHeight(this.height);
				}

			}
		}
		if (this.controller.getKeyJustPressed(ControllerKey.d))
		{
			if (this.selectingName)
				this.input.right();
			else
			{
				this.width += this.dimensionChange;
				this.currentObject.setWidth(this.width);
			}
		}
		if (this.controller.getKeyJustPressed(ControllerKey.w))
		{
			if (this.selectingName)
				this.input.up();
			else if (!this.selectingMap)
			{
				if (this.camPathMode)
				{
					this.scale += this.scaleChange;
					if (this.scale >= 10)
						this.scale = 10;
					this.currentCamPath.setScale(this.scale);
				}
				else
				{
					this.height -= this.dimensionChange;
					this.currentObject.setHeight(this.height);
				}

			}
		}
		if (this.controller.getKeyJustPressed(ControllerKey.a))
		{
			if (this.selectingName)
				this.input.left();
			else
			{
				this.width -= this.dimensionChange;
				this.currentObject.setWidth(this.width);
			}

		}
		if (this.controller.getKeyJustPressed(ControllerKey.del))
		{
			if (!this.pathMode && !this.camPathMode)
				delObject();
			else if(!this.camPathMode)
				delPath();
			else if(!this.pathMode)
				delCamPath();
		}
		if (this.controller.getKeyJustPressed(ControllerKey.right))
		{
			if (!this.pathMode && !this.camPathMode)
				nextObject();
			else if (!this.camPathMode)
				nextPath();
			else if (!this.pathMode)
				nextCamPath();
		}
		if (this.controller.getKeyJustPressed(ControllerKey.left))
		{
			if (!this.pathMode && !this.camPathMode)
				prevObject();
			else if (!this.camPathMode)
				prevPath();
			else if (!this.pathMode)
				prevCamPath();
		}
		if (this.controller.getKeyJustPressed(ControllerKey.up))
		{
			if (!this.pathMode && !this.camPathMode)
				prevType();
		}
		if (this.controller.getKeyJustPressed(ControllerKey.down))
		{
			if (!this.pathMode && !this.camPathMode)
				nextType();
		}
		if (this.controller.getKeyJustPressed(ControllerKey.eight))
		{
			if (this.selectingName)
			{
				if (this.input.getString() != "")
				{
					this.currentFile = this.input.getString() + ".map";
					this.selectingName = false;
					saveMap(this.currentFile);
				}
			}
			else if (this.currentFile == null || this.currentFile == "")
			{
				this.input = new StringInput("0");
				this.selectingName = true;
			}
			else
			{
				saveMap(this.currentFile);
			}
		}

		if (this.pathMode && !this.selectingPath && !this.camPathMode)
			this.currentPath.setPosition(round(this.parent.mouseX), round(this.parent.mouseY));
		else if (!this.selectingObject && !this.camPathMode)
			this.currentObject.setPosition(round(this.parent.mouseX), round(this.parent.mouseY));
		else if (this.camPathMode && !this.selectingCamPath)
			this.currentCamPath.setPosition(round(this.parent.mouseX), round(this.parent.mouseY));

		this.camera.renderEditorView();

		this.printStatus();

		if (this.selectingMap)
		{
			if (controller.getKeyJustPressed(ControllerKey.w))
				this.menu.up();
			if (controller.getKeyJustPressed(ControllerKey.s))
				this.menu.down();
			if (controller.getKeyJustPressed(ControllerKey.shift))
				this.menu.exec();

			this.menu.drawShape();
		}

		if (this.selectingName)
		{
			this.parent.fill(255, 0, 0);
			this.parent.textSize(18);
			this.parent.textAlign(PApplet.RIGHT);
			this.parent.text(this.input.getString() + ".map", this.parent.width - 100, 30);
		}

		if (this.selectingObject)
		{
			this.parent.stroke(0, 255, 0);
			this.parent.fill(255, 0, 0);
			this.parent.rect(this.currentObject.getX(), this.currentObject.getY(), 10, 10);
		}

		// draw current object's full path
		if (this.currentObject != null)
		{
			this.parent.stroke(0, 0, 255);
			Path path = this.currentObject.getMovePath();
			Path first = path;
			while (path != null)
			{
				this.parent.fill(0, 255, 0);
				Path next = path.getNextRaw();
				if (next != null)
				{
					this.parent.line(path.getPosition().getX(), path.getPosition().getY(), next.getPosition().getX(), next
							.getPosition().getY());
				}
				this.parent.ellipse(path.getPosition().getX(), path.getPosition().getY(), 8, 8);
				path = next;
				if(path == first) break;
			}
			if (this.currentPath != null)
			{
				this.parent.fill(255, 0, 0);
				this.parent.ellipse(this.currentPath.getPosition().getX(), this.currentPath.getPosition().getY(), 10, 10);
			}
		}

		// draw full CameraPath with scale values
		if (this.camPathMode && this.map.getIntroCamPath() != null)
		{
			this.parent.stroke(0, 0, 255);
			CameraPath path = this.map.getIntroCamPath();
			while (path != null)
			{
				this.parent.fill(0, 255, 0);
				CameraPath next = (CameraPath) path.getNextRaw();
				if (next != null)
				{
					this.parent.line(path.getPosition().getX(), path.getPosition().getY(), next.getPosition().getX(), next
							.getPosition().getY());
				}
				this.parent.ellipse(path.getPosition().getX(), path.getPosition().getY(), 8, 8);
				this.parent.textSize(10);
				this.parent.text(path.getScale(), path.getPosition().getX(), path.getPosition().getY() - 5);
				path = next;
			}
			if (this.currentCamPath != null)
			{
				this.parent.fill(255, 0, 0);
				this.parent.ellipse(this.currentCamPath.getPosition().getX(), this.currentCamPath.getPosition().getY(), 10, 10);
			}
		}
		
		// draw objects inside TriggerZones and PathSpeedTriggers
		for (WorldObject object : this.map.getWorldObjects())
		{
			this.parent.fill(0,0,255);
			this.parent.stroke(255,0,0);
			
			WorldObject innerObject = null;
			
			if(object instanceof TriggerZone)
			{
				TriggerZone zone = (TriggerZone)object;
				zone.reset();
				innerObject = zone.getWorldObject();
			}
			else if(object instanceof PathSpeedTrigger)
			{
				innerObject = ((PathSpeedTrigger)object).getTriggeredObject();
			}
			
			if(innerObject != null)
			{
				this.parent.rect(innerObject.getX(), innerObject.getY(), innerObject.getWidth(), innerObject.getHeight());
				this.parent.line(object.getX(), object.getY(), innerObject.getX(), innerObject.getY());
			}
		}

		// draw reference lines
		if (this.currentObject != null)
		{
			this.parent.stroke(255, 0, 0);
			this.parent.line(0, this.currentObject.getY(), (float) this.map.getSize().getWidth(), this.currentObject.getY());
			this.parent.line(this.currentObject.getX(), 0, this.currentObject.getX(), (float) this.map.getSize().getHeight());
			
			this.parent.stroke(255, 0, 0);
			this.parent.line(0, this.currentObject.getY() + this.currentObject.getHeight(), (float) this.map.getSize().getWidth(), this.currentObject.getY() + this.currentObject.getHeight());
			this.parent.line(this.currentObject.getX() + this.currentObject.getWidth(), 0, this.currentObject.getX() + this.currentObject.getWidth(), (float) this.map.getSize().getHeight());
		}
		if (this.pathMode && this.currentPath != null)
		{
			this.parent.stroke(0, 255, 0);
			this.parent.line(0, this.currentPath.getPosition().getY(), (float) this.map.getSize().getWidth(), this.currentPath
					.getPosition().getY());
			this.parent.line(this.currentPath.getPosition().getX(), 0, this.currentPath.getPosition().getX(), (float) this.map
					.getSize().getHeight());
		}
	}

	public void newCamPath()
	{
		int x = round(this.parent.mouseX);
		int y = round(this.parent.mouseY);

		// we have a current path
		if (this.currentCamPath != null)
		{
			// just moved a selected node, enter selection mode
			if (!isNewCamPath && !selectingCamPath)
			{
				// first node is selected
				if (this.currentCamPath.getNext() == null)
				{
					this.isNewCamPath = true;
					return;
				}
				this.selectingCamPath = true;
				return;
			}
			// add new node to current node
			else
			{
				Path old = this.currentCamPath;
				this.currentCamPath = new CameraPath(x, y, 1.0F, null, old, false);
				old.setNext(this.currentCamPath);
				this.isNewCamPath = true;
				this.selectingCamPath = false;
			}
		}
		else
		{
			// current map has a path already, add a new node to the end
			if (this.map.getIntroCamPath() != null)
			{
				Path last = this.map.getIntroCamPath();
				while (true)
				{
					Path next = last.getNextRaw();
					if (next == null)
						break;
					last = next;
				}
				this.currentCamPath = (CameraPath) last;
				this.isNewCamPath = true;
				newCamPath();
			}
			// create entirely new path
			else
			{
				this.currentCamPath = new CameraPath(x, y, 1.0F, null, null, true);
				this.map.setIntroCamPath(this.currentCamPath);
				this.isNewCamPath = true;
				this.selectingCamPath = false;
			}
		}
	}

	public void newPath()
	{
		int x = round(this.parent.mouseX);
		int y = round(this.parent.mouseY);

		// we have a current path
		if (this.currentPath != null)
		{
			// just moved a selected node, enter selection mode
			if (!isNewPath && !selectingPath)
			{
				// first node is selected
				if (this.currentPath.getNext() == null)
				{
					this.isNewPath = true;
					return;
				}
				this.selectingPath = true;
				return;
			}
			// add new node to current node
			else
			{
				Path old = this.currentPath;
				this.currentPath = new Path(x, y, null, old, false);
				old.setNext(this.currentPath);
				this.isNewPath = true;
				this.selectingPath = false;
			}
		}
		else
		{
			// currentObject has a path already, add a new node to the end
			if (this.currentObject.getMovePath() != null)
			{
				Path last = this.currentObject.getMovePath();
				while (true)
				{
					Path next = last.getNextRaw();
					if (next == null)
						break;
					last = next;
				}
				this.currentPath = last;
				this.isNewPath = true;
				newPath();
			}
			// create entirely new path
			else
			{
				this.currentPath = new Path(x, y, null, null, true);
				this.isNewPath = true;
				this.selectingPath = false;
			}
		}
	}

	public void newObject()
	{
		int x = round(this.parent.mouseX);
		int y = round(this.parent.mouseY);

		switch (ObjectType.values()[this.typeIndex])
		{
		case Block:
			this.currentObject = new Block(this.parent, x, y, this.width, this.height);
			break;
		case DeathZone:
			this.currentObject = new DeathZone(this.parent, x, y, this.width, this.height);
			break;
		case SpawnZone:
			this.currentObject = new SpawnZone(this.parent, x, y, this.width, this.height);
			break;
		case EndZone:
			this.currentObject = new EndZone(this.parent, x, y, this.width, this.height);
			break;
		case Coin:
			this.currentObject = new Coin(this.parent, x, y);
			break;
		case Gun:
			this.currentObject = new Gun(this.parent, Behaviour.Left);
			break;
		case PathSpeedTrigger:
			this.currentObject = new PathSpeedTrigger(this.parent, null, 3.0F, x, y, this.width, this.height);
			break;
		case TriggerZone:
			this.currentObject = new TriggerZone(this.parent, null, 1, x, y, this.width, this.height);
			break;
		case SimpleEnemy:
			this.currentObject = new SimpleEnemy(this.parent, x, y, this.width, this.height);
		}

		this.isNewObject = true;
		this.map.addWorldObject(this.currentObject);
	}

	public void delCamPath()
	{
		if (this.currentCamPath == null)
			return;

		this.selectingCamPath = false;
		this.isNewCamPath = true;

		CameraPath prev = (CameraPath) this.currentCamPath.getPrev();
		if (prev == null)
		{
			this.map.setIntroCamPath(null);
			this.camPathMode = false;
			return;
		}
		this.currentCamPath = prev;
		this.currentCamPath.setNext(null);
	}

	public void delPath()
	{
		if (this.currentPath == null)
			return;

		this.selectingPath = false;
		this.isNewPath = true;

		Path prev = this.currentPath.getPrev();
		if (prev == null)
		{
			this.currentObject.setMovePath(null);
			this.pathMode = false;
			return;
		}
		this.currentPath = prev;
		this.currentPath.setNext(null);
		//this.currentPath.getPrev().setNext(this.currentPath);
	}

	public void delObject()
	{
		if (this.map.getWorldObjects().size() == 1)
			return;

		WorldObject old = this.currentObject;

		prevObject();
		this.map.removeWorldObject(old);
	}

	private void nextCamPath()
	{
		if (this.currentCamPath.getNext() == null)
			return;

		CameraPath old = null;
		if (!selectingCamPath && isNewCamPath)
		{
			old = this.currentCamPath;			
			this.selectingCamPath = true;
			this.isNewCamPath = false;
		}

		this.currentCamPath = (CameraPath) this.currentCamPath.getNext();
		this.scale = this.currentCamPath.getScale();

		if (old != null)
		{
			this.currentCamPath.setPrev(null);
			if (old.getPrev() != null)
				old.getPrev().setNext(null);
		}
	}

	private void nextPath()
	{
		if (this.currentPath.getNext() == null)
			return;

		Path old = null;
		if (!selectingPath && isNewPath)
		{
			old = this.currentPath;
			this.selectingPath = true;
			this.isNewPath = false;
		}

		this.currentPath = this.currentPath.getNext();

		if (old != null)
		{
			this.currentPath.setPrev(null);
			if (old.getPrev() != null)
				old.getPrev().setNext(null);
		}
	}

	private void nextObject()
	{
		if (this.map.getWorldObjects().size() == 1)
			return;

		WorldObject old = null;
		if (!selectingObject && isNewObject)
		{
			old = this.currentObject;
			this.selectingObject = true;
			this.isNewObject = false;
		}

		int i = this.map.getWorldObjects().indexOf(this.currentObject);
		int next = ++i % this.map.getWorldObjects().size();

		this.currentObject = this.map.getWorldObjects().get(next);
		this.height = (int) this.currentObject.getHeight();
		this.width = (int) this.currentObject.getWidth();

		if (old != null)
			this.map.removeWorldObject(old);
	}

	private void prevCamPath()
	{
		if (this.currentCamPath.getPrevRaw() == null)
			return;

		CameraPath prev = (CameraPath) this.currentCamPath.getPrevRaw();

		if (isNewCamPath)
		{
			prev.setNext(null);
			this.selectingCamPath = true;
			this.isNewCamPath = false;
		}

		this.currentCamPath = (CameraPath) this.currentCamPath.getPrevRaw();
		this.scale = currentCamPath.getScale();

	}

	private void prevPath()
	{
		if (this.currentPath.getPrevRaw() == null)
			return;

		Path prev = this.currentPath.getPrevRaw();

		if (isNewPath)
		{
			prev.setNext(null);
			this.selectingPath = true;
			this.isNewPath = false;
		}

		this.currentPath = this.currentPath.getPrevRaw();

	}

	private void prevObject()
	{
		if (this.map.getWorldObjects().size() == 1)
			return;

		WorldObject old = null;
		if (!selectingObject && isNewObject)
		{
			old = this.currentObject;
			this.selectingObject = true;
			this.isNewObject = false;
		}

		int i = this.map.getWorldObjects().indexOf(this.currentObject);

		if (i == 0)
			i = this.map.getWorldObjects().size() - 1;
		else
			i--;

		this.currentObject = this.map.getWorldObjects().get(i);
		this.height = (int) this.currentObject.getHeight();
		this.width = (int) this.currentObject.getWidth();

		if (old != null)
			this.map.removeWorldObject(old);
	}

	private void nextType()
	{
		this.typeIndex = ++this.typeIndex % ObjectType.values().length;

		this.map.removeWorldObject(this.currentObject);
		newObject();
	}

	private void prevType()
	{
		if (this.typeIndex == 0)
			this.typeIndex = ObjectType.values().length - 1;
		else
			--this.typeIndex;

		this.map.removeWorldObject(this.currentObject);
		newObject();
	}

	private void printStatus()
	{
		int x = (int) this.map.getSize().getWidth();

		this.parent.fill(255, 0, 0);
		this.parent.textSize(16);
		this.parent.textAlign(PApplet.RIGHT);

		if (!this.pathMode && !this.camPathMode)
		{
			this.parent.text(this.currentObject.getClass().getSimpleName(), x, 20);
			this.parent.text(this.currentObject.getX() + "," + this.currentObject.getY(), x, 60);
		}
		else if (this.pathMode && !this.camPathMode)
		{
			this.parent.text("PathMode", x, 20);
			this.parent.text(this.currentPath.getPosition().getX() + "," + this.currentPath.getPosition().getY(), x, 60);
		}
		else
		{
			this.parent.text("CameraPathMode", x, 20);
			this.parent.text(this.currentCamPath.getPosition().getX() + "," + this.currentCamPath.getPosition().getY(), x, 60);
			this.parent.text(this.currentCamPath.getScale(), x, 80);
		}
		this.parent.text(this.currentObject.getWidth() + "*" + this.currentObject.getHeight(), x, 40);
	}

	private void createMapMenu()
	{
		File folder = new File(this.parent.dataPath("maps"));

		java.io.FilenameFilter mapFilter = new java.io.FilenameFilter() {
			public boolean accept(File dir, String name)
			{
				return name.toLowerCase().endsWith(".map");
			}
		};

		String[] filenames = folder.list(mapFilter);

		this.menu = new Menu((M3Platformer) this.parent, "Select a map", 20, 20);

		for (int i = 0; i < filenames.length; i++)
		{
			final String name = filenames[i];
			this.menu.addEntry(filenames[i], new Command() {
				public void call(M3Platformer game)
				{
					loadMap(name);
				}
			});
		}
	}

	private void saveMap(String name)
	{
		if (!this.pathMode && !this.camPathMode && this.isNewObject)
			delObject();
		else if (!this.camPathMode && this.isNewPath)
			delPath();
		else if (!this.pathMode && this.isNewCamPath)
			delCamPath();

		this.map.saveToFile(this.parent, "data/maps/" + name);
		this.currentFile = name;
	}

	private void loadMap(String name)
	{
		this.currentObject = null;
		this.currentPath = null;
		this.currentCamPath = null;
		this.selectingMap = false;
		this.selectingObject = false;
		this.selectingPath = false;
		this.selectingCamPath = false;

		this.map = GameMap.loadFromFile(this.parent.dataPath("maps/" + name));
		this.camera.map = this.map;

		this.currentFile = name;

		newObject();
	}

	public String getLatestMap()
	{
		return "data/maps/" + this.currentFile;
	}

	//snap to grid
	private int round(int value)
	{
		return value - (value % this.gridSize);
	}

	public boolean getSelectingMap()
	{
		return this.selectingMap;
	}
}
