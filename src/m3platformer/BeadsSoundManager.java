//package m3platformer;
//
//import beads.AudioContext;
//import beads.Gain;
//import beads.Sample;
//import beads.SampleManager;
//import beads.SamplePlayer;
//import beads.SamplePlayer.LoopType;
//
/**
 * Sound manager using the beads library
 */
//public abstract class BeadsSoundManager {
//	
//	public enum MusicType {
//		INTRO("data/music/nesmes-shortIntro.wav", 180000), 
//		STORY_INTRO("data/music/nesmes-shortIntro.wav", 180000),
//		ANEWGIRL("data/music/aNewGirlAppears.wav", 277000),
//		CHICKENNOODLE("data/music/chickenNoodleCigarettes.wav", 183000),
//		GAMEBOYRAIDERS("data/music/gameboyRaiders.wav", 109000),
//		FUNKBUMP("data/music/funkBump.wav", 240000),
//		HANDHELDHERO("data/music/handheldHero-Short.wav", 153000),
//		REEF("data/music/reef.wav", 300000),
//		POTATO("data/music/potato.wav", 105000),
//		BADMUSIC("data/music/reckoner.wav", 100000),
//		MAP_DEFAULT("data/music/f3ll_1n_l0v3_w17h_a_g1rl.wav", 110000);
//		
//		private String path;
//		private int length; // playing time in msecs
//		MusicType(String path, int length) {
//			this.path = path;
//			this.length = length;
//		}
//		
//		public String getPath() {
//			return path;
//		}
//		
//		public int getLength() {
//			return length;
//		}
//		
//		public Sample createSample() {
//			return SampleManager.sample(sketchPath + path);
//		}
//	}
//	
//	public enum SoundType {
//		JUMP("data/sounds/jump.wav", 0),
//		COIN("data/sounds/coin0.wav", 10),
//		FLIP("data/sounds/flip.wav", 0),
//		MENUCONF("data/sounds/menu_conf.wav", 0),
//		SPAWN("data/sounds/spawn.wav", 0),
//		TELEPORT("data/sounds/teleport.wav", 0),
//		ERROR("data/sounds/error2.wav", 0);
//		
//		private String path;
//		private int lastExec;
////		private Process proc;
//		private int delay;
//		private SamplePlayer pl;
//		private Gain g;
//		SoundType(String path, int delay) {
//			this.path = path;
//			this.delay = delay;
//
//			
//		    pl = new SamplePlayer(ac, SampleManager.sample(sketchPath + path));
//		    pl.setKillOnEnd(false);
//		    pl.pause(true);
//
//			g = new Gain(ac, 2, (float) 0.2);
//		    g.addInput(pl);
//			ac.out.addInput(g);
//		}
//		
//		public void setLastExec() {lastExec = M3Platformer.getInstance().millis();}
//		public boolean isPlayable() {return M3Platformer.getInstance().millis() - lastExec >= delay;}
////		public Process getProc() { return proc; }
////		public void setProc(Process p) { proc = p; }
//		public String getPath() { return this.path; }
//		
////		public void killProc() {
////			if(proc != null) {
////				proc.destroy(); 
////				try {
////					proc.waitFor();
////				} catch (InterruptedException e) {
////					e.printStackTrace();
////				} 
////			}
////		}
//		
//		public void play() {
//			pl.setToLoopStart();
//			pl.start();
//		}
//	}
//	
//	// info about currently playing music
////	private static Runtime run;
////	private static Process musicProc;
//	private static Gain musicGain;
//	private static SamplePlayer musicPlayer;
//	private static MusicType currentlyPlaying;
//	private static AudioContext ac;
//	
//	private static String sketchPath = "/home/arcade/games/M3Platformer/";
//	
//	public static void init() {
////		sketchPath = M3Platformer.getInstance().sketchPath("");
////		run = Runtime.getRuntime();
//		
//		ac = new AudioContext();
//		musicGain = new Gain(ac, 2, (float) 0.2);
//		ac.out.addInput(musicGain);
//		ac.start();
//		
//		SoundType tmp = SoundType.COIN;
//		
////		SampleManager.setBufferingRegime(Sample.Regime.newStreamingRegimeWithAging(5000l, 5000l));
//		
//		musicPlayer = new SamplePlayer(ac, MusicType.INTRO.createSample());
//		musicPlayer.setLoopType(LoopType.LOOP_FORWARDS);
//		musicPlayer.setKillOnEnd(false);
//		musicGain.addInput(musicPlayer);
//	}
//	
//	public static void playSound(SoundType s) {
////		if(!s.isPlayable()) return;
////		s.killProc();
////		s.setLastExec();
////		
////		try {
////			s.setProc(run.exec("mpg321 -q "+sketchPath+s.getPath()+" 2&>1 > /dev/null &"));
////		} catch (Exception e) {
////			e.printStackTrace();
////		}
//		s.play();
//	}
//	
//	public static void playJump() {
//		playSound(SoundType.JUMP);
//	}
//	public static void playCoin() {
//		playSound(SoundType.COIN);
//	}
//	
//	public static void stopMusic() {
//		musicPlayer.pause(true);
//		
//		currentlyPlaying = null;
//	}
//	
//	public static void playIntroMusic() {
//		playMusic(MusicType.INTRO);
//	}
//	
//	public static void playMapMusic() {
//		playMusic(MusicType.MAP_DEFAULT);
//	}
//	
//	public static void playStoryMusic() {
//		playMusic(MusicType.POTATO);
//	}
//	
//	public static void playMusic(MusicType t) {
//		
//		// do not restart song already playing
//		if(currentlyPlaying == t) return;
//		
//		stopMusic();
//		currentlyPlaying = t;
//
//		musicPlayer.setSample(t.createSample());
//		musicPlayer.start();
//
////		try {
////			musicProc = Runtime.getRuntime().exec("aplay "+sketchPath+t.getPath()+" 2&>1 > /dev/null &");
////		} catch (Exception e) {
////			e.printStackTrace();
////		}
////		
////		Command cmd = new Command() {
////			public void call(M3Platformer game, Object o) {
////				SoundManager.reloopMusic((SoundManager.MusicType)o);
////			}
////		};
////		
////		M3Platformer.getInstance().getLogic().registerTimeout(new Timeout(M3Platformer.getInstance(), t, t.getLength(), cmd));
//	}
//	
//	public static void reloopMusic(MusicType t) {
//		// old timeout
//		if(t != currentlyPlaying) return;
//		
//		currentlyPlaying = null;
//		
//		// restart music
//		playMusic(t);
//	}
//}
