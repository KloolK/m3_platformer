package m3platformer;
import processing.data.XML;

public interface Serializable {
	public XML serialize();
	public void deserialize(XML xml);
}
