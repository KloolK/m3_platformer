package m3platformer;

/**Rendering layer for overlapping objects
 *
 */
public enum Layer
{
	Invisible, 
	
	// no collision with background objects
	Background,
	
	// object layers
	Layer3, 
	Layer2, 
	Layer1, 
	
	// here, player is rendered
	
	// in front of player
	Foreground, 
	
	// debugging stuff
	Debug
}
