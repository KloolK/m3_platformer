package m3platformer;

import java.awt.Color;

public enum ScoreMedal {
	
	Bronze(new Color(165,103,5)),
	Silver(new Color(255,255,255)),
	Gold(new Color(254,202,43)), 
	None(new Color(221,217,206));
	
	private Color color;
	
	private ScoreMedal(Color c) {
		this.color = c;
	}
	
	public Color getColor() {
		return color;
	}
}
