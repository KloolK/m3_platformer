package m3platformer;

/** Function pointer wrapper
 * Provides two methods for defining logic to be executed later.
 *
 */
public abstract class Command {
   public void call(M3Platformer game) {}
   public void call(M3Platformer game, Object o) {}
}
