package m3platformer;

import processing.core.PImage;
import processing.data.XML;

/*
 * Sprite using animated gif
 */
public class AnimatedSprite extends Sprite {
	
	private PImage[] frames;
	private int[] delays;     // in msecs
	private int currentFrameStart = 0; // in msecs (time by millis())
	private int currentFrame = 0;
	private boolean pause = false;

	public AnimatedSprite(M3Platformer parent, String spriteName, float x,
			float y, float scale, float rotation) {
		super(parent, spriteName, x, y, scale, rotation);
	}

	public AnimatedSprite(M3Platformer parent, String spriteName, float scale) {
		super(parent, spriteName, scale);
	}

	public AnimatedSprite(M3Platformer parent, String spriteName) {
		super(parent, spriteName);
	}
	
	public AnimatedSprite(AnimatedSprite a) {
		this(a.parent, a.spriteName, a.getX(), a.getY(), a.scale, a.rotation);
	}
	
	// deserialize object from XML
	public static AnimatedSprite deserialize(M3Platformer parent, XML xml) {
		return new AnimatedSprite(parent, xml.getString("name"), xml.getFloat("x"), xml.getFloat("y"), xml.getFloat("scale"), xml.getFloat("rotation"));
	}
	
	@Override
	protected void loadImage(String path) {
		this.frames = ResourceManager.getAnimatedFrames(path);
		this.delays = ResourceManager.getAnimatedDelays(path);
		currentFrameStart = parent.millis();
		this.dimensions = new Vector2D(frames[0].width, frames[0].height);
	}
	
	@Override
	public void drawWithoutRotation(int x, int y) {
		parent.tint(255, alpha);
		parent.image(this.frames[currentFrame], x, y, this.getWidth(), this.getHeight());
		parent.noTint();
		
		if(!pause && parent.millis()-currentFrameStart >= delays[currentFrame])
			jump(currentFrame+1);
	}
	
	@Override
	public PImage getImg() {
		return this.frames[currentFrame];
	}
	
	public void jump(int x) {
		currentFrame = x % frames.length;
		currentFrameStart = parent.millis();
	}
	
	public AnimatedSprite setPause(boolean pause) {
		this.pause = pause;
		
		// chaining
		return this;
	}
}
