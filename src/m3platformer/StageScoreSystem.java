package m3platformer;

import processing.data.XML;

/*
 * This class knowns things about a single stage (=map):
 * - time limits
 * - how to score
 */
public class StageScoreSystem {

	// coin scoring limits
	private int bronzeCoins = 0;
	private int silverCoins = 0;
	private int goldCoins = 0;
	
	// time scoring limits
	private int bronzeTime = 0;
	private int silverTime = 0;
	private int goldTime = 0;
	
	// time limit for a single try in seconds (0 = no limit)
	private int singleTryTimeLimit = 0;
	
	// time limit for all tries in this map
	private int stageTimeLimit = 0;
	
	
	// time left at end of stage
	private int stageTimeLeft = 0;
	
	// best score among last tries of stage
	private int bestTime = Integer.MAX_VALUE;  // seconds played
	private int bestCoins = 0; // coins collected
	
	// last try data
	private int lastTryTime = 0;
	private int lastTryCoins = 0;
	
	
	
	// check new data (if it's better than best so far)
	public void addNewTry(int timePlayed, int coins) {
		setLastTryTime(timePlayed);
		setLastTryCoins(coins);
		
		bestTime = Math.min(timePlayed, bestTime);
		bestCoins = Math.max(coins, bestCoins);
	}
	
	public int getRawCoinScore() {
		return bestCoins * 50;
	}
	
	public int getRawTimeScore() {
		return (singleTryTimeLimit-bestTime) * 20;
	}
	
	public int getRawStageTimeScore() {
		return stageTimeLeft * 2;
	}
	
	public float getTotalMult() {
		return getTimeMult() + getCoinMult();
	}
	
	// calculate score for current stage (with current best try)
	public int getStageScore() {
		float totalMult = getTotalMult();
		int rawScore = getRawCoinScore() + getRawTimeScore();
		
		return Math.round(rawScore * totalMult) + getRawStageTimeScore();
	}
	
	public float getTimeMult() {
		switch(getTimeMedal()) {
			case Gold: return 3;
			case Silver: return 1.5f;
			case Bronze: return 1;
			
			default:
			case None:
				return 0;
		}
	}
	
	public float getCoinMult() {
		switch(getCoinMedal()) {
			case Gold: return 3;
			case Silver: return 1.5f;
			case Bronze: return 1;
			
			default:
			case None:
				return 0;
		}
	}
	
	public ScoreMedal getTimeMedal() {
		return getTimeMedal(bestTime);
	}
	
	public ScoreMedal getTimeMedal(int time) {
		if(time <= goldTime)        return ScoreMedal.Gold;
		else if(time <= silverTime) return ScoreMedal.Silver;
		else if(time <= bronzeTime) return ScoreMedal.Bronze;
		else return ScoreMedal.None;
	}
	
	public ScoreMedal getCoinMedal() {
		return getCoinMedal(bestCoins);
	}
	
	public ScoreMedal getCoinMedal(int coins) {
		if(coins >= goldCoins)        return ScoreMedal.Gold;
		else if(coins >= silverCoins) return ScoreMedal.Silver;
		else if(coins >= bronzeCoins) return ScoreMedal.Bronze;
		else return ScoreMedal.None;
	}
	
	public void reset() {
		this.bestCoins = this.lastTryCoins = 0;
		this.bestTime = this.lastTryTime = 999999;
	}
	
	// load data from xml object
	public void deserialize(XML mainXML) {
		if(mainXML == null) return;
		
		this.setSingleTryTimeLimit(mainXML.getInt("singleTryTimeLimit"));
		this.setStageTimeLimit(mainXML.getInt("levelTimeLimit"));
		this.bronzeCoins = mainXML.getInt("bronzeCoins");
		this.silverCoins = mainXML.getInt("silverCoins");
		this.goldCoins = mainXML.getInt("goldCoins");
		this.bronzeTime = mainXML.getInt("bronzeTime");
		this.silverTime = mainXML.getInt("silverTime");
		this.goldTime = mainXML.getInt("goldTime");
	}
	
	// load data from xml object
	public XML serialize() {
		// name reference to GameMap deserialisation!
		XML mainXML = new XML("scoring");
			
		// save data
		mainXML.setInt("singleTryTimeLimit", getSingleTryTimeLimit());
		mainXML.setInt("levelTimeLimit", getStageTimeLimit());
		mainXML.setInt("bronzeCoins", this.bronzeCoins);
		mainXML.setInt("silverCoins", this.silverCoins);
		mainXML.setInt("goldCoins", this.goldCoins);
		mainXML.setInt("bronzeTime", this.bronzeTime);
		mainXML.setInt("silverTime", this.silverTime);
		mainXML.setInt("goldTime", this.goldTime);
		
		return mainXML;
	}
	
	// create scoring object from xml
	public static StageScoreSystem fromXML(XML xml) {
		StageScoreSystem res = new StageScoreSystem();
		res.deserialize(xml);
		
		return res;
	}
	
	public boolean hasReachedNextStage() {
		return getCoinMedal() != ScoreMedal.None && getTimeMedal() != ScoreMedal.None;
	}

	public int getStageTimeLimit() {
		return stageTimeLimit;
	}

	public void setStageTimeLimit(int levelTimeLimit) {
		this.stageTimeLimit = levelTimeLimit;
	}
	
	public int getSingleTryTimeLimit() {
		return singleTryTimeLimit;
	}

	public void setSingleTryTimeLimit(int timeLimit) {
		this.singleTryTimeLimit = timeLimit;
	}

	public int getBronzeCoins() {
		return bronzeCoins;
	}

	public void setBronzeCoins(int bronzeCoins) {
		this.bronzeCoins = bronzeCoins;
	}

	public int getSilverCoins() {
		return silverCoins;
	}

	public void setSilverCoins(int silverCoins) {
		this.silverCoins = silverCoins;
	}

	public int getGoldCoins() {
		return goldCoins;
	}

	public void setGoldCoins(int goldCoins) {
		this.goldCoins = goldCoins;
	}

	public int getBronzeTime() {
		return bronzeTime;
	}

	public void setBronzeTime(int bronzeTime) {
		this.bronzeTime = bronzeTime;
	}

	public int getSilverTime() {
		return silverTime;
	}

	public void setSilverTime(int silverTime) {
		this.silverTime = silverTime;
	}

	public int getGoldTime() {
		return goldTime;
	}

	public void setGoldTime(int goldTime) {
		this.goldTime = goldTime;
	}

	public int getLastTryTime() {
		return lastTryTime;
	}

	public void setLastTryTime(int lastTryTime) {
		this.lastTryTime = lastTryTime;
	}

	public int getLastTryCoins() {
		return lastTryCoins;
	}

	public void setLastTryCoins(int lastTryCoins) {
		this.lastTryCoins = lastTryCoins;
	}
	

	public int getBestTime() {
		return bestTime;
	}

	public void setBestTime(int bestTime) {
		this.bestTime = bestTime;
	}

	public int getBestCoins() {
		return bestCoins;
	}

	public void setBestCoins(int bestCoins) {
		this.bestCoins = bestCoins;
	}

	public int getStageTimeLeft() {
		return stageTimeLeft;
	}

	public void setStageTimeLeft(int stageTimeLeft) {
		this.stageTimeLeft = stageTimeLeft;
	}
}
