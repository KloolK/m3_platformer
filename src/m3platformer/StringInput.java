package m3platformer;

/**class for string input with joystick 
 * 
 * change last char via up/down keys (scroll through list of characters ... <-> a <-> b <-> <-> c <-> ....)
 * delete/add char via left/right keys
 * 
 */
public class StringInput {
	// highscore name input
	String curInput = "a";
	private final String charsFull    = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-_ ";
	private final String charsReduced = "abcdefghijklmnopqrstuvwxyz0123456789 ";
	private String chars = charsFull;
	
	private int minLen = 0;
	private int maxLen = Integer.MAX_VALUE;
	
	public StringInput(String content, boolean reduced) {
		if(reduced) this.chars = charsReduced;
		if(content != null) setContent(content);
	}
	public StringInput() {}
	public StringInput(String content) {
		this();
		setContent(content);
	}
	
	private void dropLast() {
		if(curInput.length()>0)
			curInput = curInput.substring(0, curInput.length() - 1);
	}
	
	// change last char of input to the previous char in our charlist
	public void up() {
		if(curInput.length() == 0) return;
		
		int charCount = chars.length();
		char curChar = curInput.charAt(curInput.length()-1);
		dropLast();
		
		curInput += chars.charAt((charCount - 1 + chars.indexOf(curChar)) % charCount);
	}
	
	// change last char of input to the next char in our charlist
	public void down() {
		if(curInput.length() == 0) return;
		
		char curChar = curInput.charAt(curInput.length()-1);
		dropLast();
		curInput += chars.charAt((chars.indexOf(curChar)+1) % chars.length());
	}
	
	// add another char at the end of our input
	public void right() {
		if(curInput.length()<maxLen)
			curInput += "a";
	}

	// delete last char of input if any
	public void left() {
		if (curInput.length() > 1) dropLast();
	}
	
	public String getString() {return this.curInput;}
	public void setContent(String con) {if(con==null || con.length()==0) con="a"; this.curInput = con+"";}
	
	public boolean isLengthOkay() {
		return curInput.length() >= minLen && curInput.length() <= maxLen;
	}
	
	public int getMinLen() {
		return minLen;
	}
	public void setMinLen(int minLen) {
		this.minLen = minLen;
	}
	public int getMaxLen() {
		return maxLen;
	}
	public void setMaxLen(int maxLen) {
		this.maxLen = maxLen;
	}
}
