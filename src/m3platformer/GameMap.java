package m3platformer;

import java.awt.geom.Rectangle2D;
import java.util.*;

import processing.data.XML;

import objects.Coin;
import objects.PathSpeedTrigger;
import objects.WorldObject;
import objects.WorldObjectManager;
import objects.ColorSchemable;

/** Represents a map including its WorldObjects.
 *  Supports storing to and restoring from file.
 *  
 *  Multimodal Media Madness RWTH 08.06.2012
 *  Jakob Bauer, Jan Bruckner, Michael Deutschen
 */
public class GameMap
{	
	// Contains all WorldObjects in the map
	List<WorldObject> objectList = new LinkedList<WorldObject>();
	
	// The maps name and background music
	String title;
	SoundManager.MusicType musicType;
	
	final static SoundManager.MusicType DEFAULT_BG_MUSIC = SoundManager.MusicType.MAP_DEFAULT;
	
	private String filename;
	
	// number of coins on map
	private int coinCount = 0;
		
	// The maps size;
	private Rectangle2D size;
	
	private ColorScheme colorScheme;
	private StageScoreSystem scoring;
	
	// camera path for intro
	private CameraPath introCamPath = null;
	
	// background sprite
	private Sprite background = null;
	
	// Constructor
	public GameMap(int w, int h, ColorScheme c)
	{
		// TODO: when reading map from file, this wont work
		this.setSize(new Rectangle2D.Float(0, 0, w, h));
		this.colorScheme = c;
		this.scoring = new StageScoreSystem();
	}
	
	private GameMap() {}
	
	// reset positions of moving objects, remove triggered objects, reset SpawnCounts
	// we also have to recount coins here
	public void restart()
	{
		this.coinCount = 0;
		
		if(filename != null && !filename.equals("")) {
			reparseXMLFile(true);			
		}

		else {
			throw new RuntimeException("Map has not been loaded from a file, restart not possible!");
		}
		
		// add objects inside PathSpeedTriggers to the map
		List<WorldObject> newObjects = new LinkedList<WorldObject>();
		for (WorldObject object : this.objectList)
		{
			if(object instanceof PathSpeedTrigger)
			{
				WorldObject innerObject = ((PathSpeedTrigger)object).getTriggeredObject();
				if(innerObject != null)
					newObjects.add(innerObject);
			}
		}
		for (WorldObject object : newObjects)
		{
			this.addWorldObject(object);
		}
	}
	
	// get the WorldObject list
	public List<WorldObject> getWorldObjects()
	{
		return this.objectList;
	}

	// add an object to map
	public void addWorldObject(WorldObject obj) {
		this.objectList.add(obj);
		
		// apply color scheme if possible
		if(obj instanceof ColorSchemable) ((ColorSchemable)obj).applyColorScheme(this.colorScheme);
		
		// coin?
		if(obj instanceof Coin) coinCount++;
	}

	// remove an object from map
	public void removeWorldObject(WorldObject obj) {
		this.objectList.remove(obj);
	}
	
	// save current object list to XML file
	public void saveToFile(M3Platformer parent, String filename) {
		XML mainXML = new XML("map");
		
		// save map dimensions
		mainXML.setInt("w", (int)this.getSize().getWidth());
		mainXML.setInt("h", (int)this.getSize().getHeight());
		mainXML.setString("colorScheme", getColorScheme().getFilename());
		if(this.musicType != null)
			mainXML.setString("musicType", musicType.toString());
		mainXML.setString("title", this.title);
		
		if(getBackground()!=null) {
			XML child = mainXML.addChild("background");
			child.addChild(getBackground().serialize());
		}
		
		mainXML.addChild(this.scoring.serialize());
		
		if(getIntroCamPath() != null)
			mainXML.addChild(getIntroCamPath().serialize("introCamPath"));
		
		// save objects
		XML xml = mainXML.addChild("objects");
		for(WorldObject o : getWorldObjects()) {
			xml.addChild(o.serialize());
		}
		
		// write to file
		parent.saveXML(mainXML, M3Util.normalizePath(filename));
	}
	
	// load complete map from file
	public static GameMap loadFromFile(String filename) {
		GameMap res = new GameMap();
		res.filename = filename;
		res.reparseXMLFile();
		
		return res;
	}
	
	// parse list of objects from XML
	public void parseXMLObjectList(M3Platformer parent, XML xml) {
		if(xml == null) return;
		
		this.objectList.clear();
		for(XML x : xml.getChildren()) {
			WorldObject object = WorldObjectManager.fromXML(parent, x);
			if(object == null) continue;
						
			// add object to list
			this.addWorldObject(object);
		}
	}
	
	
	public void reparseXMLFile() {
		reparseXMLFile(false);
	}
	
	// (re)parse source file for map
	public void reparseXMLFile(boolean onlyObjectList) {
		M3Platformer parent = M3Platformer.getInstance();
		XML mainXML = parent.loadXML(M3Util.normalizePath(filename));
		
		if(!onlyObjectList) {
			// load map data
			this.setSize(new Rectangle2D.Float(0,0,mainXML.getInt("w"), mainXML.getInt("h")));
			this.scoring = StageScoreSystem.fromXML(mainXML.getChild("scoring"));
			
			// map name
			if(mainXML.hasAttribute("title")) title = mainXML.getString("title");
			
			// camera path for intro
			XML campath = mainXML.getChild("introCamPath");
			if(campath != null) setIntroCamPath(CameraPath.deserialize(campath));
			
			// background image
			XML bg = mainXML.getChild("background");
			if(bg != null) setBackground(Sprite.deserialize(parent, bg.getChild(Sprite.class.getName())));
			else setBackground(new Sprite(parent, "data/imgs/bg1.gif"));
			
			// color scheme
			if(mainXML.hasAttribute("colorScheme"))
				this.setColorScheme(ColorScheme.loadFromFile(mainXML.getString("colorScheme")));
			else
				this.setColorScheme(ColorScheme.getDefaultScheme());
			
			// bg music
			if(mainXML.hasAttribute("musicType"))
				this.musicType = SoundManager.MusicType.valueOf(mainXML.getString("musicType"));
			
			if(musicType == null) this.musicType = DEFAULT_BG_MUSIC;
		}
		
		this.parseXMLObjectList(parent, mainXML.getChild("objects"));
	}

	public Rectangle2D getSize() {
		return size;
	}

	public void setSize(Rectangle2D size) {
		this.size = size;
	}

	public int getCoinCount() {
		return coinCount;
	}
	
	public ColorScheme getColorScheme() {
		return colorScheme;
	}

	public void setColorScheme(ColorScheme colorScheme) {
		this.colorScheme = colorScheme;
	}

	public StageScoreSystem getScoring() {
		return scoring;
	}
	
	public void resetScoring() {
		this.scoring.reset();
	}

	public void setScoring(StageScoreSystem scoring) {
		this.scoring = scoring;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public CameraPath getIntroCamPath() {
		return introCamPath;
	}

	public void setIntroCamPath(CameraPath introCamPath) {
		this.introCamPath = introCamPath;
	}

	public Sprite getBackground() {
		return background;
	}

	public void setBackground(Sprite background) {
		this.background = background;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
