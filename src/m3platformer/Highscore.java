package m3platformer;

import java.util.*;
import processing.data.*;

/**Stores and manages the highscore list.
 * Supports adding of entries, parsing and saving to files.
 *
 */
public class Highscore {

	public class gt<T> implements Comparator<T> {
		public int compare(T o1, T o2) {
			if (!(o1 instanceof HEntry) || !(o2 instanceof HEntry)) {
				throw new ClassCastException();
			}

			HEntry h1 = (HEntry) o1;
			HEntry h2 = (HEntry) o2;
			if (h1.score < h2.score)
				return -1;
			if (h1.score > h2.score)
				return 1;
			if (h1.score == h2.score && h1.name.equals(h2.name))
				return 0;
			return -1;
		}

		public boolean equals(Object o1) {
			return false;
		}
	}

	public class HEntry {
		String name = null;
		int score;

		HEntry(int score, String name) {
			this.name = name;
			this.score = score;
		}
		
		public String getName() { return this.name; }
		public int getScore() { return this.score; }

		public boolean equals(Object o) {
			if (!(o instanceof HEntry))
				return false;
			HEntry m = (HEntry) o;
			return m.name == name && m.score == score;
		}
	}

	private final String highScoreFile;
	private final int MAX_SCORES = 10;
	private SortedSet<HEntry> set = new TreeSet<HEntry>(new gt<HEntry>());

	public Highscore(String file) {
		Table tb;

		this.highScoreFile = file;

		try {
			tb = M3Platformer.getInstance().loadTable(highScoreFile, "header");
			parseTable(tb);
		}

		// no highscore table found -> create new one
		catch (Exception e) {
			tb = new Table();

			tb.addColumn("name");
			tb.addColumn("score");

			TableRow nr = tb.addRow();
			nr.setString("name", "Alan Turing");
			nr.setInt("score", 60000);

			nr = tb.addRow();
			nr.setString("name", "Max Power");
			nr.setInt("score", 50000);

			nr = tb.addRow();
			nr.setString("name", "Edsger Dijkstra");
			nr.setInt("score", 36000);

			nr = tb.addRow();
			nr.setString("name", "Charles Hoare");
			nr.setInt("score", 30000);

			nr = tb.addRow();
			nr.setString("name", "Larry Page");
			nr.setInt("score", 28000);

			nr = tb.addRow();
			nr.setString("name", "Ada Lovelace");
			nr.setInt("score", 25000);

			nr = tb.addRow();
			nr.setString("name", "Dennis Ritchie");
			nr.setInt("score", 20000);

			nr = tb.addRow();
			nr.setString("name", "Haskell Curry");
			nr.setInt("score", 15000);

			nr = tb.addRow();
			nr.setString("name", "Angela Merkel");
			nr.setInt("score", 8000);

			nr = tb.addRow();
			nr.setString("name", "President Obama");
			nr.setInt("score", 5000);

			parseTable(tb);
			saveToFile();
		}
	}

	private void parseTable(Table tb) {
		for (TableRow r : tb.rows()) {
			set.add(new HEntry(r.getInt("score"), r.getString("name")));
		}
	}

	public void saveToFile() {
		Table tb = new Table();
		tb.addColumn("name");
		tb.addColumn("score");

		for (HEntry m : set) {
			TableRow r = tb.addRow();
			r.setString("name", m.name);
			r.setInt("score", m.score);
		}

		M3Platformer.getInstance().saveTable(tb, highScoreFile);
	}

	public HEntry getMaxEntry() {
		return set.last();
	}

	public HEntry getMinEntry() {
		return set.first();
	}

	public Highscore addEntry(int score, String name) {
		HEntry newEntry = new HEntry(score, name);
		if (set.contains(newEntry))
			return this;

		set.add(newEntry);

		// remove entries if there are more than allowed
		while (set.size() > MAX_SCORES)
			set.remove(set.first());
		
		return this;
	}

	public SortedSet<HEntry> getEntries() {
		return set;
	}
	
	// TODO: implement this
	public int getPositionFromScore(int score) {
		int pos = MAX_SCORES+1, i=10;
		
		// from bottom up
		for(HEntry m : getEntries()) {
			if(m.getScore()<score && pos>i) pos=i;
			i--;
		}
		
		return pos;
	}
}
