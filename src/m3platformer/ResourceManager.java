package m3platformer;

import gifAnimation.Gif;
import gifAnimation.GifDecoder;

import java.util.*;
import processing.core.*;


/**Ensures that resources are only loaded once.
 *
 */
public abstract class ResourceManager {
	// static images
	private static Map<String,PImage> imgs = new HashMap<String,PImage>();
	
	// animated gifs
	private static Map<String,PImage[]> gifFrames = new HashMap<String,PImage[]>();
	private static Map<String,int[]> gifDelays = new HashMap<String,int[]>();
	
	// fonts
	private static Map<String,PFont> fonts = new HashMap<String,PFont>();
	
	// preload important resources at setup time
	public static void init() {	
		// static images
		loadImage("data/imgs/developedBy.gif");
		
		loadImage("data/imgs/logo/menuBG.gif");
		loadImage("data/imgs/logo/alanOnly.gif");
		loadImage("data/imgs/logo/alinaOnly.gif");
		loadImage("data/imgs/logo/and.gif");
		loadImage("data/imgs/logo/insertCoin.gif");
		
		loadImage("data/imgs/bg1.gif");
		loadImage("data/imgs/ui.gif");
		
		loadImage("data/imgs/alan/left.gif");
		loadImage("data/imgs/alan/right.gif");
		loadImage("data/imgs/alan/rightSad.gif");
		loadImage("data/imgs/alan/leftLookingRight.gif");
		loadImage("data/imgs/alina/rightAngry.gif");
		
		loadImage("data/imgs/icons/clockBronze.gif");
		loadImage("data/imgs/icons/clockSilver.gif");
		loadImage("data/imgs/icons/clockNone.gif");
		loadImage("data/imgs/icons/clockGold.gif");
		
		// animated gifs
		loadGif("data/imgs/alan/lyingWink.gif");
		loadGif("data/imgs/alan/idle.gif");
		loadGif("data/imgs/coinFlip.gif");
		
		// fonts
		loadFont("data/fonts/dice12Smooth.vlw");
		loadFont("data/fonts/8bitwonder11Smooth.vlw");
		loadFont("data/fonts/8bitwonder12Smooth.vlw");
	}
	
	// loads image from path and adds it to map
	private static PImage loadImage(String path) {		
		M3Platformer app = M3Platformer.getInstance();
		PImage img = app.loadImage(path);
		imgs.put(path, img);
		
		return img;
	}
	
	// get image from map
	// if image has not been loaded yet, load it
	public static PImage getImg(String path) {
		path = M3Util.normalizePath(path);
		PImage img = imgs.get(path);
		if(img == null) {
			img = loadImage(path);
		}
		
		return img;
	}
	
	
	// loads animated gif from path and adds it to maps
	private static void loadGif(String path) {
		if(!gifFrames.containsKey(path)) {
			
			M3Platformer app = M3Platformer.getInstance();
			GifDecoder gifDecoder = new GifDecoder();
			gifDecoder.read(app.createInput(path));
			PImage[] frames = Gif.extractFrames(gifDecoder);
			int[] delays = Gif.extractDelays(gifDecoder);
			
			gifFrames.put(path, frames);
			gifDelays.put(path, delays);
		}
	}
	
	// get frames of animated gif from map
	// if image has not been loaded yet, load it
	public static PImage[] getAnimatedFrames(String path) {
		path = M3Util.normalizePath(path);
		PImage[] frames = gifFrames.get(path);
		if(frames == null) {
			loadGif(path);
			frames = gifFrames.get(path);
		}
		
		return frames;
	}
	
	// get delays of animated gif from map
	// if image has not been loaded yet, load it
	public static int[] getAnimatedDelays(String path) {
		path = M3Util.normalizePath(path);
		int[] delays = gifDelays.get(path);
		if(delays == null) {
			loadGif(path);
			delays = gifDelays.get(path);
		}
		
		return delays;
	}

	
	// loads font file and puts it into map
	private static PFont loadFont(String path) {
		path = M3Util.normalizePath(path);
		
		M3Platformer app = M3Platformer.getInstance();
		PFont font = app.loadFont(path);
		fonts.put(path, font);
		
		return font;
	}

	// return font from map
	public static PFont getFont(String path) {
		path = M3Util.normalizePath(path);
		PFont font = fonts.get(path);
		if(font == null) {
			font = loadFont(path);
		}
		
		return font;
	}
}
