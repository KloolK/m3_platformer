package m3platformer;

import java.io.File;

public class M3Util {
	private M3Util() {}
	
	public static String normalizePath(String path) {
		char del = File.separatorChar;
		path = path.replace('\\', del);
		path = path.replace('/', del);
		return path;
	}
}
