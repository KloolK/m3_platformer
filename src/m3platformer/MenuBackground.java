package m3platformer;

public abstract class MenuBackground {

	protected M3Platformer parent;

	public MenuBackground() {
		parent = M3Platformer.getInstance();
	}
	
	public abstract void draw();

}