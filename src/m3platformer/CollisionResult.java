package m3platformer;

import java.util.Vector;

public class CollisionResult {
	public final Vector<Float> depth;
	public final boolean touching;
	
	public CollisionResult(Vector<Float> depth, boolean touch) {
		this.depth = depth;
		this.touching = touch;
	}
}