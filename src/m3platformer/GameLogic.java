package m3platformer;

import java.util.*;

import m3platformer.animations.Blink;
import m3platformer.animations.FadeImage;

import objects.PlayerFigure;
import objects.SpawnZone;
import objects.WorldObject;

import ui.*;

/** Main game flow controller
 * 
 * This class controlls the logic of the gameplay by setting the current {@link GameMode}.
 * It starts rendering, handles inputs and controlls game mode transitions. Most important class of the architecture.
 *
 *  Multimodal Media Madness RWTH 08.06.2012
 *  @author Jakob Bauer, Jan Bruckner, Michael Deutschen
 */
public class GameLogic
{
	// controlled modules
	protected M3Platformer parent;
	protected Physics physics;
	private Camera camera;
	private Controller controller;
	protected PlayerFigure player;
	public GameMap currentMap;
	MapEditor editor;
	protected Menu mainMenu;
	
	// user interface
	protected GameIntro gameIntro;
	protected StageIntro stageIntro;
	protected StageClear tryScoring;
	protected StageScore stageScoring;
	protected StoryIntro storyIntro;
	protected GameOver gameOver;
	protected PauseMenu pauseMenu;
	protected InGameUI ui;
	protected Sprite timeUp;
	protected HighscoreScreen highscoreScreen;
	protected Credits credits;
	
	// highscore
	protected Highscore highscore;
	
	// black screen for transitions
	protected Sprite black;

	// timeouts
	final Set<Timeout> timeouts = new HashSet<Timeout>();
	final Set<Timeout> timeoutsToAdd = new HashSet<Timeout>();

	// current game mode
	protected GameMode gameMode;
	
	// game mode transitions
	protected GameMode targetMode; // used for transitions
	protected Command transitionCmd;
	
	// link to currently focused StringInput
	private StringInput currentInput = null;
	
	// ingame time of the current stage
	protected int elapsedStageTime = 0;
	
	// ingame time of the current try
	protected int elapsedTryTime = 0;
	
	// time the last tick with gamemode running was executed
	// used for measuring ingame time
	private int lastRunningTickTime = 0;
	
	// stage/map list
	List<String> stageList = new LinkedList<String>();
	private int currentStageIndex = 0;
	private int currentStageNumber = 0;	// different from currentStageIndex+1 if there are bonus stages which are not counted
	
	// credits start time
	private int creditsStartTime = 0;
	
	// debug time measurement
	int[] _duration = new int[10];

	// with only one map
	@Deprecated
	public GameLogic(M3Platformer parent, Physics physics, Camera camera, Controller controller, PlayerFigure player, GameMap map,
			MapEditor editor, Menu mainMenu)
	{
		this.parent = parent;
		this.physics = physics;
		this.setCamera(camera);
		this.setController(controller);
		this.player = player;
		this.currentMap = map;
		this.editor = editor;

		setGameMode(GameMode.MainMenu);
		this.mainMenu = mainMenu;
		this.currentStageIndex = 0;
		this.setCurrentStageNumber(1);
	}
	
	
	// standard: with map list
	public GameLogic(M3Platformer parent, Physics physics, Camera camera, Controller controller, PlayerFigure player, MapEditor editor, Menu mainMenu)
	{
		this.parent = parent;
		this.physics = physics;
		this.setCamera(camera);
		this.setController(controller);
		this.player = player;
		this.editor = editor;
		this.mainMenu = mainMenu;

		// init user interfaces and intros
		this.ui = new InGameUI(this);
		ui.setPosition(0, parent.height-ui.getHeight());
		this.highscoreScreen = new HighscoreScreen(this);
		this.highscore = new Highscore("data/highscore.csv");
		this.timeUp = new Sprite(parent, "data/imgs/timeUp.gif");
		timeUp.setPosition((parent.getWidth() - timeUp.getWidth())/2, (parent.getHeight() - timeUp.getHeight())/2);
		timeUp.setAlpha(0);
		
		black = new Sprite(parent, "data/imgs/black.gif",0,0,1,0);
		black.setAlpha(0);
		
		// standard map list
		this.currentStageIndex = 0;
		this.setCurrentStageNumber(1);
		this.stageList.add("data/maps/tutorial.map");
		this.stageList.add("data/maps/easy1.map");
		this.stageList.add("data/maps/easy2.map");
		this.stageList.add("data/maps/jan0.map");
		this.stageList.add("data/maps/maze1.map");
		this.stageList.add("data/maps/jakob0.map");
		this.stageList.add("data/maps/arena1.map");
		this.stageList.add("data/maps/blockShift1.map");
		this.stageList.add("data/maps/getHigh1.map");
		this.stageList.add("data/maps/speed3.map");
		this.stageList.add("data/maps/speed2.map");
		this.stageList.add("data/maps/arena2.map");
		this.stageList.add("data/maps/jan1.map");
		this.stageList.add("data/maps/speed1.map");
		this.stageList.add("data/maps/speed4.map");
		this.stageList.add("data/maps/stickland.map");
		this.stageList.add("data/maps/lovetrain.map");
		this.stageList.add("data/maps/bulletstorm.map");
		this.stageList.add("data/maps/endZone.map");
	}
	
	
	
	/* ------------ tick/tock ------------- */
	
	/*
	 * rendering for every game mode
	 */
	public void tick()
	{
		this.checkTimeouts();
		
		int startTime = this.parent.millis();
		
		switch (this.gameMode)
		{
		default:
		case Intro:
			if(gameIntro == null) break;
			
			gameIntro.draw(0,0);
			break;
			
		case IntroHighscore:
			highscoreScreen.draw(0, 0);
			highscoreScreen.drawCoinMsg();
			break;
			
		case Highscore:
			highscoreScreen.draw(0, 0);
			highscoreScreen.drawReturnButton();
			break;
			
		case HighscoreInput:
			highscoreScreen.draw(0, 0);
			highscoreScreen.drawInputInstructions();
			break;
			
		case MainMenu:
			if(mainMenu == null) break;
			
			mainMenu.drawShape();
			break;
			
		case StoryIntro:
			storyIntro.draw(0, 0);
			break;
			
		case StageIntro:
			// only move objects, not player
			this.physics.tick(true);
			
			// render view
			this.getCamera().setViewDimensionsCanvas(parent.width, parent.height);
			this.getCamera().setDrawPoint(new Vector2D(0,0));
			this.getCamera().renderView();
			
			// render stage welcome screen if necessary
			if(camera.isPauseCamPath()) stageIntro.draw(0,0);
			
			break;
			
		case FreezeGame:
			// render view
			this.getCamera().setViewDimensionsCanvas(parent.width, parent.height-30);
			this.getCamera().setDrawPoint(new Vector2D(0,0));
			this.getCamera().renderView();
			
			ui.draw(0, 0);
			
			this.timeUp.draw(0, 0);
			
			break;
			
		case PauseMenu:
			// render view
			this.getCamera().setViewDimensionsCanvas(parent.width, parent.height-30);
			this.getCamera().setDrawPoint(new Vector2D(0,0));
			this.getCamera().renderView();
			
			ui.draw(0, 0);
			
			pauseMenu.draw(0, 0);
			
			break;
			
		case Running:
			
			this.getCamera().setViewDimensionsCanvas(parent.width, parent.height-30);
			this.getCamera().setDrawPoint(new Vector2D(0,0));
			
			// measure time
			elapsedStageTime += parent.millis()-lastRunningTickTime;
			elapsedTryTime += parent.millis()-lastRunningTickTime;
			lastRunningTickTime = parent.millis();
			
			// try time up?
			int tryTimeLeft = currentMap.getScoring().getSingleTryTimeLimit() - elapsedTryTime/1000;
			if(currentMap.getScoring().getSingleTryTimeLimit() <= 0) tryTimeLeft = 999;
			if(tryTimeLeft <= 0) {
				setGameMode(GameMode.FreezeGame);
				timeUp.setAlpha(255);
				timeUp.addAnimation(new Blink(3000,2).setFinishCmd(new Command() {
					public void call(M3Platformer game) {
						game.getLogic().timeUp.setAlpha(0);
						game.getLogic().killPlayer();
					}
				}));
				break;
			}
			
			// stage time up?
			int stageTimeLeft = currentMap.getScoring().getStageTimeLimit() - elapsedStageTime/1000;
			if(currentMap.getScoring().getStageTimeLimit() > 0 && stageTimeLeft <= 0) {
				setGameMode(GameMode.FreezeGame);
				timeUp.setAlpha(255);
				timeUp.addAnimation(new Blink(3000,2).setFinishCmd(new Command() {
					public void call(M3Platformer game) {
						game.getLogic().timeUp.setAlpha(0);
						endCurrentStage();
					}
				}));
				break;
			}
			
			// do physics
			this.physics.tick(false);
			
			
			this._duration[0] += this.parent.millis() - startTime;
			startTime = this.parent.millis();
			
			// render view
			this.getCamera().renderView();
			
			
			this._duration[1] += this.parent.millis() - startTime;
			startTime = this.parent.millis();
			
			// draw user interface
			ui.draw(0, 0);
						
			break;
		case SingleTryScoring:
			
			camera.renderView();
			tryScoring.draw(0,0);			
			
			break;
			
		case StageScoring:
						
			stageScoring.draw(0, 0);
			
			break;
			
		case Credits:
			
			this.credits.draw(0, 0);
			
			break;
			
		case GameOver:
			
			this.gameOver.draw(0, 0);
			
			break;
			
		//map editor
		case MapEditor:
			this.editor.tick();
			if (getController().getKeyJustPressed(ControllerKey.e) && !this.editor.getSelectingMap())
			{
				this.setGameMode(GameMode.Running);
				this.changeStage(this.editor.getLatestMap());
			}
			break;
		}

		this._duration[2] += this.parent.millis() - startTime;
		startTime = this.parent.millis();
		
		black.draw(0,0);
	}
	

	/*
	 * input handling and game mode changes
	 */
	public void tock()
	{
		int startTime = this.parent.millis();

		// transition running -> do not accept any input
		if(targetMode != null) return;
		
		switch (this.gameMode)
		{
		case Intro:
		case IntroHighscore:
			if(this.controller.getKeyJustPressed(ControllerKey.c)) {
				this.clearTimeouts();
				startGameModeTransition(GameMode.MainMenu, null);
			}
			break;
		
		case Highscore:
			if(this.controller.getKeyJustPressed(ControllerKey.one)) {
				startGameModeTransition(GameMode.MainMenu, null);
			}
			break;
			
		case HighscoreInput:
			if(this.controller.getKeyJustPressed(ControllerKey.one)) {
				if(currentInput.isLengthOkay()) {
					endHighscoreInput();
				}
			}
			
			if(this.controller.getKeyJustPressed(ControllerKey.w)) currentInput.up();
			if(this.controller.getKeyJustPressed(ControllerKey.a)) currentInput.left();
			if(this.controller.getKeyJustPressed(ControllerKey.s)) currentInput.down();
			if(this.controller.getKeyJustPressed(ControllerKey.d)) currentInput.right();
			break;
			
		case StoryIntro:
			if(this.controller.getKeyJustPressed(ControllerKey.one)) {
				clearTimeouts();
				startNewGame();
			}
			break;
			
		case StageIntro:
			if(getController().getKeyJustPressed(ControllerKey.one)) {
				if(camera.isPauseCamPath()) {
					camera.setPauseCamPath(false);
					camera.jumpToCurrentPathPoint();
				}
				else nextTry();
			}
			break;
		case MainMenu:
			if(mainMenu == null) break;
			
			if(getController().getKeyJustPressed(ControllerKey.w)) mainMenu.up();
			if(getController().getKeyJustPressed(ControllerKey.s)) mainMenu.down();
			if(getController().getKeyJustPressed(ControllerKey.one)) mainMenu.exec();
			break;
			
		case PauseMenu:
			if(mainMenu == null) break;
			
			if(getController().getKeyJustPressed(ControllerKey.one)) unpauseGame();
			if(currentMap.getScoring().hasReachedNextStage() && getController().getKeyJustPressed(ControllerKey.q))
				endCurrentStage();
			
			break;
		
		case Running:
			int deltaX = 0;
			int deltaY = 0;

			if (this.controller.getKeyJustPressed(ControllerKey.one)) pauseGame();
			if (this.controller.isKeyPressed(ControllerKey.d)) deltaX += 1;
			if (this.controller.isKeyPressed(ControllerKey.a)) deltaX += -1;
			if (this.controller.isKeyPressed(ControllerKey.w)) player.jump(); //deltaY += -1;
			//if (this.controller.s()) deltaY += 1;

			Vector2D acceleration = new Vector2D(deltaX * WorldObject.SPEED_CHANGE, deltaY * WorldObject.JUMP_HEIGHT);
			this.player.accelerate(acceleration);
			
			if (this.player.hasReachedEnd() == true) {
				this.setGameMode(GameMode.FreezeGame);
				
				camera.setFollowPlayer(false);
				this.player.reachedEndAnimation(new Command() {
					public void call(M3Platformer game) {
						game.getLogic().getPlayer().setPosition(-100, -100);
						game.getLogic().endCurrentTry();
						game.getLogic().getCamera().setFollowPlayer(true);
					}
				});
			}
			
//			if (getController().getKeyJustPressed(ControllerKey.q))
//				this.setGameMode(GameMode.MapEditor);
//			
//			if(getController().getKeyJustPressed(ControllerKey.up))
//				this.camera.setScale(this.camera.getScale() * 1.33f);
//				
//			if(getController().getKeyJustPressed(ControllerKey.down))
//				this.camera.setScale(this.camera.getScale() / 1.33f);
			
			
			// check player status in FreezeGame and in Running!
			case FreezeGame:
			if (this.player.isAlive() == false && currentMap!=null) this.nextTry();
			
			break;
			
		case SingleTryScoring:
			if (this.getController().getKeyJustPressed(ControllerKey.q)) {
				this.setGameMode(GameMode.Running);
				this.nextTry();
			}
			if (this.getCurrentMap().getScoring().hasReachedNextStage() && this.getController().getKeyJustPressed(ControllerKey.one)) {
				endCurrentStage();
			}
			break;
			
		case StageScoring:
			if (this.getController().getKeyJustPressed(ControllerKey.one)) {
				
				if(!stageScoring.hasEnded()) stageScoring.jumpToEnd();

				// no map left, game finished
				else if(!this.switchToNextStage()) {
					startCredits();
				}
			}
			break;
			
		case GameOver:
			if (this.getController().getKeyJustPressed(ControllerKey.c)) {
				// new coin -> restart stage
				this.changeStage(currentMap.getFilename());
			}
			break;
			
		case Credits:
			if(this.getController().getKeyJustPressed(ControllerKey.one) && parent.millis()-creditsStartTime > 83000)
				endCredits();
			
			break;
			
		default: break;
		}

		this._duration[3] += this.parent.millis() - startTime;
		
		// reset controller states
		this.getController().resetJustPressed();
	}
	
	/*
	 * checks, executes and removes finished timeouts
	 */
	protected void checkTimeouts() {
		// check timeouts
		Set<Timeout> tDel = new HashSet<Timeout>();
		for (Timeout t : timeouts)
		{
			if (t.check())
			{
				if (t.cmd != null)
					t.call();
				else
					gameMode = t.targetStatus;

				tDel.add(t);
			}
		}

		for (Timeout t : tDel)
			timeouts.remove(t);
		tDel = null;

		for (Timeout t : timeoutsToAdd)
			timeouts.add(t);

		timeoutsToAdd.clear();
	}

	/*
	 * registers and starts a new timeout
	 * in fact, the timeout is only staged for registration in the next tick
	 */
	public void registerTimeout(Timeout t) {
		timeoutsToAdd.add(t);
	}
	
	private void clearTimeouts() {
		timeouts.clear();
	}
	

	
	/* ------------- map/stage management ----------- */

	/*
	 * current try ended because of reaching an end zone or the try time limit
	 */
	public void endCurrentTry() {
		if(player.hasReachedEnd()) {
			this.currentMap.getScoring().addNewTry(elapsedTryTime/1000, player.getCoinCount());
			this.setGameMode(GameMode.SingleTryScoring);
			this.tryScoring = new StageClear(this);
		}
	}
	
	// current stage ended because of stage time limit or manually switching to next one
	public void endCurrentStage() {
		
		StageScoreSystem s = currentMap.getScoring();
		s.setStageTimeLeft(s.getStageTimeLimit()-this.getElapsedStageTime()/1000);
		if(!s.hasReachedNextStage())
			this.gameOver();
		else {			
			player.addToTotalScore(s.getStageScore());
			stageScoring = new StageScore(this);
			this.setGameMode(GameMode.StageScoring);
		}
	}
	
	/*
	 * gets next stage, increases stage counter, calls changeStage
	 */
	private boolean switchToNextStage() {
		if(this.currentMap == null) changeStage(stageList.get(currentStageIndex));
		
		else {
			currentStageIndex++;
			
			setCurrentStageNumber(getCurrentStageNumber() + 1);
			
			if(currentStageIndex >= stageList.size()) return false;
			
			changeStage(stageList.get(currentStageIndex));
		}
		
		return true;
	}
	
	/*
	 * opens and prepares stage with given path, starts stage intro
	 */
	private void changeStage(String path)
	{
		GameMap newMap = GameMap.loadFromFile(path);

		this.elapsedStageTime = 0;
		this.currentMap = newMap;		
		this.physics.setMap(newMap);
		this.camera.map = newMap;
		this.stageIntro = new StageIntro(this);
		
		this.player.applyColorScheme(currentMap.getColorScheme());
		
		this.currentMap.resetScoring();
		
		// play music
		SoundManager.playMusic(currentMap.musicType);
		
		// start map intro
		this.currentMap.restart();
		this.player.setPosition(-500, -500);
		setGameMode(GameMode.StageIntro);
		
		// first path-node = starting position for camera
		float x,y,scale;
		x = (float)currentMap.getSize().getWidth()/2;
		y = (float)currentMap.getSize().getHeight()/2;
		Vector2D dimCam = camera.getViewDimensionsCanvas();
		scale = (float)Math.min(1, Math.max(dimCam.getX()/currentMap.getSize().getWidth(), dimCam.getY()/currentMap.getSize().getHeight()));

		CameraPath overView = new CameraPath(x, y, scale, currentMap.getIntroCamPath(), null, false);
		overView.setMovementSpeedFromHere(8);
		overView.setScaleSpeedFromHere(0.01F);
		overView.setDelay(3000);
		
		this.camera.setCamPath(overView);
		this.camera.setPauseCamPath(true);
		this.camera.jumpToCurrentPathPoint();
		
		this.camera.setPathFinishCmd(new Command() {
			public void call(M3Platformer game) {
				game.getLogic().nextTry();
			}
		});
	}
	
	/*
	 * Restarts the current map, stops camera path, resets current try scores
	 */
	protected void nextTry()
	{
		this.currentMap.restart();
		
		this.camera.resetScale();
		this.camera.setCamPath(null);
		this.camera.setPathFinishCmd(null);
		
		this.setGameMode(GameMode.FreezeGame); // will be changed to running after player spawning animation
		this.player.resetCoinCount();
		this.elapsedTryTime = 0;
		
		Vector2D spawnPoint = this.findPlayerSpawn();
		if(spawnPoint != null)
			this.player.setPosition((int) spawnPoint.getX() - (int) this.player.getWidth() / 2, (int) spawnPoint.getY() - (int) this.player.getHeight() / 2);
		else this.player.setPosition(50, 50);

		this.player.spawn(new Command() {
			public void call(M3Platformer game) {
				game.getLogic().setGameMode(GameMode.Running);
			}
		});
	}
	
	/*
	 * fade in / out game modes
	 */
	public void startGameModeTransition(GameMode targetMode, Command transitionCmd) {
		this.targetMode = targetMode;
		this.transitionCmd = transitionCmd;
		black.addAnimation(new FadeImage(500, true).setFinishCmd(new Command() {
			public void call(M3Platformer game) {
				game.getLogic().endGameModeTransition();
			}
		}).setFollowUp(new FadeImage(500, false)));
	}
	
	/*
	 * called in the middle of a game mode transition (black screen)
	 */
	private void endGameModeTransition() {
		if(transitionCmd != null) transitionCmd.call(parent);
		setGameMode(targetMode);
		targetMode = null;
	}

	/*
	 * opens pause menu
	 */
	public void pauseGame() {
		this.pauseMenu = new PauseMenu(this);
		setGameMode(GameMode.PauseMenu);
	}
	
	/*
	 * close pause menu and resume game
	 */
	public void unpauseGame() {
		setGameMode(GameMode.Running);
	}
	
	/*
	 * starts story animation
	 */
	public void startStoryIntro() {
		this.storyIntro = new StoryIntro(this);
		startGameModeTransition(GameMode.StoryIntro, new Command() {
			public void call(M3Platformer game) {
				SoundManager.playStoryMusic();
			}
		});
	}
	
	/*
	 * starts a new game, beginning with the first map in the stage list
	 */
	public void startNewGame() {
		SoundManager.playMapMusic();
		this.currentMap = null;
		this.player.setTotalScore(0);
		switchToNextStage();
	}
	
	// player failed, game is over
	public void gameOver() {
		this.setGameMode(GameMode.FreezeGame);
		this.gameOver = new GameOver(this);
		
		this.player.deathAnimation(new Command(){
			public void call(M3Platformer game) {
				game.getLogic().setGameMode(GameMode.GameOver);
				SoundManager.playMusic(SoundManager.MusicType.REEF);
			}
		});
	}
	
	// game over timeout ended
	public void endGameOver() {
		SoundManager.playIntroMusic();
		if(highscore.getPositionFromScore(player.getTotalScore())<11) openHighscoreInput();
		else startIntro();
	}
	
	/*
	 * starts the game intro (developed by..., logo, insert coin)
	 */
	public void startIntro() {
		this.gameIntro = new GameIntro(this);
		
		if(gameMode == null) setGameMode(GameMode.Intro);
		else startGameModeTransition(GameMode.Intro, null);
		
		registerTimeout(new Timeout(parent, 40000, new Command() {
			public void call(M3Platformer game) {
				game.getLogic().switchToHighscoreIntro();
			}
		}));

		// start background music
		SoundManager.playIntroMusic();
	}
	
	private void switchToHighscoreIntro() { switchToHighscoreIntro(null); }
	private void switchToHighscoreIntro(Command cmd) {
		startGameModeTransition(GameMode.IntroHighscore, cmd);
		
		registerTimeout(new Timeout(parent, 40000, new Command() {
			public void call(M3Platformer game) {
				game.getLogic().startIntro();
			}
		}));
	}
	
	public void openHighscore() { openHighscore(null); }
	public void openHighscore(Command cmd) {
		startGameModeTransition(GameMode.Highscore, cmd);
	}
	
	public void openHighscoreInput() {
		highscoreScreen.setInputPosition(highscore.getPositionFromScore(player.getTotalScore()));
		currentInput = new StringInput("ALAN", true); // reduced character set because of font
		currentInput.setMaxLen(18);
		currentInput.setMinLen(3);
		startGameModeTransition(GameMode.HighscoreInput, null);
	}
	
	public void endHighscoreInput() {
		switchToHighscoreIntro(new Command() {
			public void call(M3Platformer game) {
				GameLogic l = game.getLogic();
				l.highscore.addEntry(player.getTotalScore(), currentInput.getString());
				l.highscore.saveToFile();
				l.highscoreScreen.setInputPosition(-1);
				l.currentInput = null;
				
				l.player.setTotalScore(0);
			}
		});
	}
	
	public void startCredits() {
		this.credits = new Credits(this);
		creditsStartTime = parent.millis();
		startGameModeTransition(GameMode.Credits, new Command() {
			public void call(M3Platformer game) {
				SoundManager.playMusic(SoundManager.MusicType.HANDHELDHERO);
			}
		});
	}
	
	public void endCredits() {
		// highscore?
		if(highscore.getPositionFromScore(player.getTotalScore())<11) openHighscoreInput();
		
		// no highscore
		else {
			this.gameMode = null;
			startIntro();
		}
	}
	
	/*
	 * looks for a player spawnpoint on the current map
	 * returns null if nothing found
	 */
	private Vector2D findPlayerSpawn() {
		for (WorldObject object : this.currentMap.getWorldObjects())
		{
			if (object instanceof SpawnZone)
			{
				Vector2D spawnZone = ((SpawnZone) object).getPosition();
				Vector2D dim = ((SpawnZone) object).getDimensions();
				dim.scale(0.5f);
				spawnZone.add(dim);
				return spawnZone;
			}
		}
		
		return null;
	}
	
	/*
	 * starts death animation of player, ends with player.alive=false
	 */
	public void killPlayer() {
		this.setGameMode(GameMode.FreezeGame);
		
		this.player.deathAnimation(new Command(){
			public void call(M3Platformer game) {
				game.getLogic().getPlayer().kill();
			}
		});
	}

	
	
	
	
	/* --------- getters and setters ---------- */
	
	
	public Camera getCamera() {
		return camera;
	}

	public void setCamera(Camera camera) {
		this.camera = camera;
	}

	public StringInput getCurrentInput() {
		return currentInput;
	}

	public void setCurrentInput(StringInput currentInput) {
		this.currentInput = currentInput;
	}

	public PlayerFigure getPlayer() {
		return player;
	}

	public void setPlayer(PlayerFigure player) {
		this.player = player;
	}

	public GameMode getGameMode() {
		return gameMode;
	}

	public void setGameMode(GameMode gameMode) {
		this.gameMode = gameMode;
		
		// time measurements
		if(gameMode == GameMode.Running) {
			lastRunningTickTime = parent.millis();
		}
	}

	public Controller getController() {
		return controller;
	}

	public void setController(Controller controller) {
		this.controller = controller;
	}

	public int getElapsedStageTime() {
		return elapsedStageTime;
	}

	public void setElapsedStageTime(int elapsedStageTime) {
		this.elapsedStageTime = elapsedStageTime;
	}

	public int getElapsedTryTime() {
		return elapsedTryTime;
	}

	public void setElapsedTryTime(int elapsedTryTime) {
		this.elapsedTryTime = elapsedTryTime;
	}

	public GameMap getCurrentMap() {
		return currentMap;
	}

	public void setCurrentMap(GameMap currentMap) {
		this.currentMap = currentMap;
	}

	public GameIntro getGameIntro() {
		return gameIntro;
	}

	public void setGameIntro(GameIntro gameIntro) {
		this.gameIntro = gameIntro;
	}

	public Menu getMainMenu() {
		return mainMenu;
	}

	public int getCurrentStageNumber() {
		return currentStageNumber;
	}

	public void setCurrentStageNumber(int currentStageNumber) {
		this.currentStageNumber = currentStageNumber;
	}

	public StageIntro getStageIntro() {
		return stageIntro;
	}

	public StageClear getTryScoring() {
		return tryScoring;
	}

	public StageScore getStageScoring() {
		return stageScoring;
	}

	public StoryIntro getStoryIntro() {
		return storyIntro;
	}

	public GameOver getGameOver() {
		return gameOver;
	}


	public Highscore getHighscore() {
		return highscore;
	}


	public Credits getCredits() {
		return credits;
	}
}
