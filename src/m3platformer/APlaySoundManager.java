//package m3platformer;
//
//import java.io.File;
//
//
///**
// * This sound manager uses the linux program aplay to play music and sounds in the background.
// * Only works with wav files (you will have to convert the mp3s).
// */
//public abstract class APlaySoundManager {
//	
//	public enum MusicType {
//		INTRO("data/music/nesmes-shortIntro.wav", 180000), 
//		STORY_INTRO("data/music/nesmes-shortIntro.wav", 180000),
//		ANEWGIRL("data/music/aNewGirlAppears.wav", 277000),
//		CHICKENNOODLE("data/music/chickenNoodleCigarettes.wav", 183000),
//		GAMEBOYRAIDERS("data/music/gameboyRaiders.wav", 109000),
//		FUNKBUMP("data/music/funkBump.wav", 240000),
//		HANDHELDHERO("data/music/handheldHero-Short.wav", 153000),
//		REEF("data/music/reef.wav", 300000),
//		POTATO("data/music/potato.wav", 105000),
//		BADMUSIC("data/music/reckoner.wav", 100000),
//		MAP_DEFAULT("data/music/f3ll_1n_l0v3_w17h_a_g1rl.wav", 110000);
//		
//		private String path;
//		private int length; // playing time in msecs
//		MusicType(String path, int length) {
//			this.path = path;
//			this.length = length;
//		}
//		
//		public String getPath() {
//			return path;
//		}
//		
//		public int getLength() {
//			return length;
//		}
//	}
//	
//	public enum SoundType {
//		JUMP("data/sounds/jump.wav", 250),
//		COIN("data/sounds/coin0.wav", 250),
//		FLIP("data/sounds/flip.wav", 250),
//		MENUCONF("data/sounds/menu_conf.wav", 250),
//		SPAWN("data/sounds/spawn.wav", 250),
//		TELEPORT("data/sounds/teleport.wav", 250),
//		ERROR("data/sounds/error2.wav", 250);
//		
//		private String path;
//		private int lastExec;
//		private Process proc;
//		private int delay;
//		SoundType(String path, int delay) {
//			this.path = path;
//			this.delay = delay;
//		}
//		
//		public void setLastExec() {lastExec = M3Platformer.getInstance().millis();}
//		public boolean isPlayable() {return M3Platformer.getInstance().millis() - lastExec >= delay;}
//		public Process getProc() { return proc; }
//		public void setProc(Process p) { proc = p; }
//		public String getPath() { return this.path; }
//		
//		public void killProc() {
//			if(proc != null) {
//				proc.destroy(); 
//				try {
//					proc.waitFor();
//				} catch (InterruptedException e) {
//					e.printStackTrace();
//				} 
//			}
//		}
//	}
//	
//	// info about currently playing music
//	private static Process musicProc;
//	private static MusicType currentlyPlaying;
//	private static File devNull;
//	
//	private static String sketchPath = "/home/arcade/games/M3Platformer/";
//	
//	public static void init() {
//		sketchPath = M3Platformer.getInstance().sketchPath("");
//		devNull = new File("/dev/null");
//	}
//	
//	public static void playSound(SoundType s) {
//		if(!s.isPlayable()) return;
//		s.killProc();
//		s.setLastExec();
//		
//		try {
//			ProcessBuilder pb = new ProcessBuilder("aplay", "-q", sketchPath+s.getPath());
//			pb.redirectErrorStream(true);
//			pb.redirectOutput(devNull);
//			s.setProc(pb.start());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//	
//	public static void playJump() {
//		playSound(SoundType.JUMP);
//	}
//	public static void playCoin() {
//		playSound(SoundType.COIN);
//	}
//	
//	public static void stopMusic() {
//		if(musicProc != null) {
//			musicProc.destroy();
//			try {
//				musicProc.waitFor();
//			}catch(Exception e) {
//				e.printStackTrace();
//			}
//			musicProc = null;
//		}
//		
//		currentlyPlaying = null;
//	}
//	
//	public static void playIntroMusic() {
//		playMusic(MusicType.INTRO);
//	}
//	
//	public static void playMapMusic() {
//		playMusic(MusicType.MAP_DEFAULT);
//	}
//	
//	public static void playStoryMusic() {
//		playMusic(MusicType.POTATO);
//	}
//	
//	public static void playMusic(MusicType t) {
//		
//		// do not restart song already playing
//		if(currentlyPlaying == t) return;
//		
//		stopMusic();
//		currentlyPlaying = t;
//		
//		try {
//			ProcessBuilder pb = new ProcessBuilder("aplay", "-q", sketchPath+t.getPath());
//			pb.redirectErrorStream(true);
//			pb.redirectOutput(devNull);
//			musicProc = pb.start();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		Command cmd = new Command() {
//			public void call(M3Platformer game, Object o) {
//				APlaySoundManager.reloopMusic((APlaySoundManager.MusicType)o);
//			}
//		};
//		
//		M3Platformer.getInstance().getLogic().registerTimeout(new Timeout(M3Platformer.getInstance(), t, t.getLength(), cmd));
//	}
//	
//	public static void reloopMusic(MusicType t) {
//		// old timeout
//		if(t != currentlyPlaying) return;
//		
//		currentlyPlaying = null;
//		
//		// restart music
//		playMusic(t);
//	}
//}
