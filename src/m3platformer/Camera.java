package m3platformer;

import java.awt.geom.Rectangle2D;
import objects.DebugRectangle;
import objects.Figure;
import objects.WorldObject;


/** Rendering of maps and their objects in running game
 *  Provides a camera to render a only the visible part of a map.
 *  Camera can follow paths, scale view and move over the map.
 *  Standard mode is to center the view at the player position with respect to the map borders.
 *  
 *  Background images are "moved" according to 
 *  the relative position of the camera in the map and the image size.
 *
 */
public class Camera
{
	static float DEFAULT_SCALE = 1f;
	
	// The current map
	protected GameMap map;
	
	// The camera path to follow (if != null, this overrides the player-at-center concept)
	protected CameraPath camPath = null;
	protected Command pathFinishCmd = null;
	protected Vector2D pathPosition = new Vector2D(0,0); // current virtual view center position along the path, not limited by map borders, NOT UPPER LEFT CORNER!
	protected float pathSpeed = 2f;
	protected float scaleSpeed = 0.01f;
	protected boolean pauseCamPath = false;

	// The player Figure
	protected Figure player;

	// The rectangle the camera sees
	protected Vector2D viewPosition;   // upper left corner of camera, relative to map coordinates
	protected Vector2D viewDimensionsMap; // size of camera view, scaled in map coordinates
	protected Vector2D viewDimensionsCanvas; // size of camera view, absolute in canvas coordinates

	// The point where to draw the view onto the canvas
	protected Vector2D drawPoint;	   // upper left corner of camera rendering area, relative to canvas coordinates

	// The parent PApplet that we will render ourselves onto
	protected M3Platformer parent;
	
	private float scale = DEFAULT_SCALE;
	
	// >1 -> movement in PIXELATION pixels
	private final int PIXELATION = 1;
	
	int[] _duration = new int[10];

	private boolean followPlayer = true;

	// Constructor
	public Camera(M3Platformer parent, GameMap map, Figure player, float drawX, float drawY, float w, float h)
	{
		this.parent = parent;
		this.map = map;
		this.player = player;
		this.drawPoint = new Vector2D(drawX, drawY);
		this.viewPosition = new Vector2D(0, 0);
		this.viewDimensionsMap = new Vector2D(w, h);
		
		this.viewDimensionsCanvas = new Vector2D(w,h);
	}

	// Renders the current view
	public void renderView()
	{
		int startTime = this.parent.millis();
		int durCount = 0;

		// reposition camera
		this.repositionView();
		
		// clear canvas, reset window size
		this.parent.resetSize();
		this.viewDimensionsMap.setVect(viewDimensionsCanvas);
		this.viewDimensionsMap.scale(1/getScale());
		
		this.parent.background(12,20,31);
		renderMapBackground();
				
		// scale view
		parent.pushMatrix();
		this.parent.scale(this.getScale());
		
		this._duration[durCount++] += this.parent.millis() - startTime; startTime = this.parent.millis();

		// render background and object layer
		parent.fill(255);
		this._duration[durCount++] += this.parent.millis() - startTime; startTime = this.parent.millis();
		renderLayer(Layer.Background);
		renderLayer(Layer.Layer3);		
		renderLayer(Layer.Layer2);
		renderLayer(Layer.Layer1);
		this._duration[durCount++] += this.parent.millis() - startTime; startTime = this.parent.millis();
		
		// draw player
		this.draw(this.player);
		
		// render foreground and debugging stuff
		this._duration[durCount++] += this.parent.millis() - startTime; startTime = this.parent.millis();
		renderLayer(Layer.Foreground);
		renderLayer(Layer.Debug);
		this._duration[durCount++] += this.parent.millis() - startTime; startTime = this.parent.millis();
		
		// remove all DebugObjects
		for (int i = 0; i < this.map.getWorldObjects().size(); i++)
		{
			WorldObject object = this.map.getWorldObjects().get(i);
			if (object instanceof DebugRectangle)
			{
				this.map.removeWorldObject(object);
			}
		}
		this._duration[durCount++] += this.parent.millis() - startTime; startTime = this.parent.millis();
		
		
		// reset scale to normal
		parent.popMatrix();
	}
	
	// Renders the current view for the mapeditor
	public void renderEditorView()
	{
		int startTime = this.parent.millis();			
		int durCount = 0;			
		
		this.parent.size((int)this.map.getSize().getWidth(), (int)this.map.getSize().getHeight());
		this.viewDimensionsMap.setXY((float)this.map.getSize().getWidth(), (float)this.map.getSize().getHeight());
		
		//this.centerView();
		this._duration[durCount++] += this.parent.millis() - startTime; startTime = this.parent.millis();
		
		//clear canvas
		this.parent.background(12,20,31);
		
		parent.fill(255);
		this._duration[durCount++] += this.parent.millis() - startTime; startTime = this.parent.millis();
		//renderMapBackground();
		renderLayer(Layer.Background);
		renderLayer(Layer.Layer3);		
		renderLayer(Layer.Layer2);
		renderLayer(Layer.Layer1);
		this._duration[durCount++] += this.parent.millis() - startTime; startTime = this.parent.millis();
		
		// draw player
//			this.draw(this.player);
		this._duration[durCount++] += this.parent.millis() - startTime; startTime = this.parent.millis();
		renderLayer(Layer.Foreground);
		renderLayer(Layer.Debug);
		this._duration[durCount++] += this.parent.millis() - startTime; startTime = this.parent.millis();			
		
		this._duration[durCount++] += this.parent.millis() - startTime; startTime = this.parent.millis();
	}
	
	protected void renderMapBackground() {
		// no sprite given?
		Sprite bg = map.getBackground();
		if(bg==null) return;
		
		// sprite big enough?
		float bgHeight = bg.getHeight();
		float bgWidth = bg.getWidth();
		float mapHeight = (float)map.getSize().getHeight();
		float mapWidth = (float)map.getSize().getWidth();
		float cameraHeight = this.getHeight();
		float cameraWidth  = this.getWidth();
		
		// move in ratio to current view position in map
		int x = (int) this.drawPoint.getX();
		int y = (int) this.drawPoint.getY();
		
		float rX = (float) (this.viewPosition.getX() / (mapWidth - cameraWidth));
		float rY = (float) (this.viewPosition.getY() / (mapHeight - cameraHeight));
		
		x -= rX * (bgWidth-cameraWidth*getScale());
		y -= rY * (bgHeight-cameraHeight*getScale());

		// pixelate!
		x = x/PIXELATION; x = x*PIXELATION;
		y = y/PIXELATION; y = y*PIXELATION;
		
		bg.drawWithoutRotation(x, y);
	}

	// Render the specified layer
	protected void renderLayer(Layer layer)
	{
		for (WorldObject object : this.map.getWorldObjects())
		{
			if (layer == Layer.Debug || CollisionDetection.RectRect(this.getView(), object.getFrame()))
			{
				if (object.getLayer() == layer)
					this.draw(object);
			}
		}
	}

	// draw an object
	protected void draw(WorldObject object)
	{
		int x = (int)object.getX();
		int y = (int)object.getY();

		// pixelate!
		x = x/PIXELATION; x = x*PIXELATION;
		y = y/PIXELATION; y = y*PIXELATION;
				
		x = x - (int) this.viewPosition.getX() + (int) this.drawPoint.getX();
		y = y - (int) this.viewPosition.getY() + (int) this.drawPoint.getY();
		
		object.draw(x, y);
	}

	// Center view at the player's position / move along current path
	protected void repositionView()
	{
		float x = viewPosition.getX();
		float y = viewPosition.getY();
		
		// currently not following any path
		if(camPath == null && this.followPlayer) {
			x = (int)(this.player.getX()) - ((int)this.getWidth() / 2) + (int) this.player.getWidth() / 2;
			y = (int)(this.player.getY()) - ((int)this.getHeight() / 2) + (int) this.player.getHeight() / 2;
		}
		
		// following path
		else if(camPath != null){
			x = pathPosition.getX();
			y = pathPosition.getY();
			
			if(!pauseCamPath) {
				// current path not reached yet
				boolean goToNext = true;
				Vector2D dist = new Vector2D(this.camPath.getPosition());
				dist.sub(pathPosition);
				if(dist.getLength() >= this.pathSpeed) {
					
					// move towards target
					Vector2D direc = new Vector2D(camPath.getPosition());
					direc.sub(pathPosition);
					direc.scaleToLength(this.pathSpeed);
					x += direc.getX();
					y += direc.getY();
					
					pathPosition.setXY(x, y);
					
					goToNext = false;
				}
				
				// scaling needed?
				if(Math.abs(this.getScale() - camPath.getScale()) >= scaleSpeed) {
					if(this.getScale() < camPath.getScale()) 	setScale(getScale()+scaleSpeed);
					else										setScale(getScale()-scaleSpeed);
					
					goToNext = false;
				}
				else setScale(camPath.getScale());
				
				// we reached position and scale of current target
				if(goToNext) {
					jumpToCurrentPathPoint();
				}
			}
			
			// move x,y to upper left corner for setting viewPos
			x -= getWidth()/2;
			y -= getHeight()/2;
		}
				
		this.setViewPos(x, y);
	}
	
	public void startToNextPathPoint() {
		if(this.camPath==null) return;
		
		// speed for movement to next point
		this.pathSpeed = this.camPath.getMovementSpeedFromHere();
		this.scaleSpeed = this.camPath.getScaleSpeedFromHere();
				
		// start movement to next point
		this.setCamPath((CameraPath)this.camPath.getNext());
		
		// end reached
		if(this.camPath == null) {
			if(pathFinishCmd != null) pathFinishCmd.call(parent);
		}
	}
	
	// jump view and scale to path point p, delay further movement if necessary
	// and start movement to next path point if any
	protected void jumpToCurrentPathPoint() {
				
		// there is a point to go
		if(this.camPath != null) {
			
			// apply stuff from current point
			this.setScale(this.camPath.getScale());
			
			// avoid virtual-only-movement
			Vector2D tmp = limitViewPosition(camPath.getPosition().getX()-getWidth()/2, camPath.getPosition().getY()-getHeight()/2, (float)map.getSize().getWidth(), (float)map.getSize().getHeight());
			tmp.add(new Vector2D(getWidth()/2, getHeight()/2));
			this.pathPosition.setVect(tmp);
			
			if(!pauseCamPath) {
				// delay
				if(this.camPath.getDelay() > 0) {
					this.pathSpeed = this.scaleSpeed = 0;
					parent.getLogic().registerTimeout(new Timeout(parent, this.camPath.getDelay(), new Command() {
						public void call(M3Platformer game) {
							game.getLogic().getCamera().startToNextPathPoint();
						}
					}));
				}
				
				// no delay
				else startToNextPathPoint();
			}
		}
	}

	// sets the view's position
	protected void setViewPos(float x, float y)
	{
		// check the coordinates so the view stays inside the map
		this.viewPosition.setVect(limitViewPosition(x, y, (float)this.map.getSize().getWidth(), (float)this.map.getSize().getHeight()));
	}

	// ensure taht the coordinates are valid, meaning they won't move the view
	// outside of the map
	public Vector2D limitViewPosition(float x, float y, float wLimit, float hLimit)
	{		
		if (x < 0)
			x = 0;
		else if (x > wLimit - this.getWidth())
			x = (int) (wLimit - this.getWidth());
		if (y < 0)
			y = 0;
		else if (y > hLimit - this.getHeight())
			y = (int) (hLimit - this.getHeight());
		
		return new Vector2D(x,y);
	}

	// sets the view's size
	public void setViewSize(float w, float h)
	{
		this.viewDimensionsMap.setXY(w, h);
	}

	public float getWidth()
	{
		return (float)this.viewDimensionsMap.getX();
	}

	public float getHeight()
	{
		return (float)this.viewDimensionsMap.getY();
	}

	public Rectangle2D getView() {
		return new Rectangle2D.Float(viewPosition.getX(), viewPosition.getY(), viewDimensionsMap.getX(), viewDimensionsMap.getY());
	}
	
	public void setViewDimensionsCanvas(float x, float y) {
		this.viewDimensionsCanvas.setXY(x, y);
	}
	
	public Vector2D getViewDimensionsCanvas() {
		return this.viewDimensionsCanvas;
	}

	public float getScale() {
		return scale;
	}

	public void setScale(float scale) {
		this.scale = scale;
		this.viewDimensionsMap.setXY(viewDimensionsCanvas.getX()/scale, viewDimensionsCanvas.getY()/scale);
	}
	

	public CameraPath getCamPath() {
		return camPath;
	}

	public void setCamPath(CameraPath camPath) {
		this.camPath = camPath;
	}

	public Command getPathFinishCmd() {
		return pathFinishCmd;
	}

	public void setPathFinishCmd(Command pathFinishCmd) {
		this.pathFinishCmd = pathFinishCmd;
	}
	
	public void resetScale() {
		setScale(DEFAULT_SCALE);
	}

	public Vector2D getDrawPoint() {
		return drawPoint;
	}

	public void setDrawPoint(Vector2D drawPoint) {
		this.drawPoint = drawPoint;
	}

	public boolean isPauseCamPath() {
		return pauseCamPath;
	}

	public void setPauseCamPath(boolean pauseCamPath) {
		this.pauseCamPath = pauseCamPath;
	}

	public boolean isFollowPlayer() {
		return followPlayer;
	}

	public void setFollowPlayer(boolean followPlayer) {
		this.followPlayer = followPlayer;
	}
}
