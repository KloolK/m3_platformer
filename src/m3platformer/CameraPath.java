package m3platformer;

import processing.data.XML;

/*
 * path for camera movement
 * 
 * IMPORTANT: path position corresponds to the center of the view, NOT top left corner!
 */
public class CameraPath extends Path {

	// camera scale for current path point
	private float scale = 1;
	
	// camera movement speed from here to next path point
	private float movementSpeedFromHere = 2f;
	
	// camera scale speed from here on
	private float scaleSpeedFromHere = 0.02f;
	
	// delay for camera before leaving this point (in ms)
	private int delay = 0;
	
	public CameraPath(float x, float y, float scale, Path next, Path prev, boolean turn) {
		super(x, y, next, prev, turn);
		
		this.setScale(scale);
	}
	
	// do not invert camera paths!
	@Override
	public void invertPath() {
		return;
	}
	
	@Override
	public XML serialize(String name) {
		XML res = new XML(name);
		CameraPath cur = this;
		while(cur != null) {
			XML child = new XML(cur.getClass().getName());
			child.setFloat("x", cur.getPosition().getX());
			child.setFloat("y", cur.getPosition().getY());
			child.setInt("turn", cur.turnIfEnd ? 1 : 0);
			child.setFloat("scale", cur.getScale());
			child.setFloat("movementSpeed", cur.getMovementSpeedFromHere());
			child.setFloat("scaleSpeed", cur.getScaleSpeedFromHere());
			child.setInt("delay", cur.getDelay());
			res.addChild(child);
			
			cur = (CameraPath) cur.next; // not getNext(), because of possible turning at end
		}
		
		return res;
	}
	
	// TODO: find solution to inherit this from Path class
	// deserializes complete path from XML (same form as serialize)
	// returns first path object
	public static CameraPath deserialize(XML xml) {
		XML[] paths = xml.getChildren();
		CameraPath first = null;
		CameraPath recent = null;
		for(XML x : paths) {
			if(x.getName() != CameraPath.class.getName()) continue;
			
			CameraPath cur = new CameraPath(x.getFloat("x"),x.getFloat("y"),x.getFloat("scale"), null, null, x.getInt("turn") == 1);
			
			if(x.hasAttribute("delay")) cur.setDelay(x.getInt("delay"));
			if(x.hasAttribute("scaleSpeed")) cur.setScaleSpeedFromHere(x.getFloat("scaleSpeed"));
			if(x.hasAttribute("movementSpeed")) cur.setMovementSpeedFromHere(x.getFloat("movementSpeed"));
			cur.setPrev(recent);
			
			if(recent != null) recent.setNext(cur);
			if(first == null) first = cur;
			
			recent = cur;
		}
				
		return first;
	}

	public float getScale() {
		return scale;
	}

	public void setScale(float scale) {
		this.scale = scale;
	}

	public float getMovementSpeedFromHere() {
		return movementSpeedFromHere;
	}

	public void setMovementSpeedFromHere(float movementSpeedFromHere) {
		this.movementSpeedFromHere = movementSpeedFromHere;
	}

	public float getScaleSpeedFromHere() {
		return scaleSpeedFromHere;
	}

	public void setScaleSpeedFromHere(float scaleSpeedFromHere) {
		this.scaleSpeedFromHere = scaleSpeedFromHere;
	}

	public int getDelay() {
		return delay;
	}

	public void setDelay(int delay) {
		this.delay = delay;
	}	
}
