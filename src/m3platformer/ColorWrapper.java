package m3platformer;

import java.util.*;
import java.awt.Color;

import processing.data.XML;

public class ColorWrapper implements Serializable {
	private List<Color> colors = new LinkedList<Color>();
	
	public ColorWrapper() {}

	public void addColor(Color c) {colors.add(c);}
	
	public List<Color> getColors() {
		return colors;
	}

	@Override
	public XML serialize() {
		XML xml = new XML("colorWrapper");
		
		for(Color c : colors) {
			XML child = xml.addChild("color");
			child.setInt("r", c.getRed());
			child.setInt("g", c.getBlue());
			child.setInt("b", c.getGreen());
			child.setInt("alpha", c.getAlpha());
		}
		
		return xml;
	}

	@Override
	public void deserialize(XML xml) {
		this.colors.clear();
		for(XML x : xml.getChildren("color")) {
			this.addColor(new Color(x.getInt("r"),x.getInt("g"),x.getInt("b"),x.getInt("a")));
		}
	}	
	
	public static ColorWrapper fromXML(XML xml) {
		ColorWrapper res = new ColorWrapper();
		res.deserialize(xml);
		
		return res;
	}
}
