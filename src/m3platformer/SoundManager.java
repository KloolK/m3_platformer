package m3platformer;

import ddf.minim.*;

/**
 * This sound manager uses the processing library minim for sound output.
 * Since this made a lot of problems on the raspberry pi on which the game should run in the beginning, 
 * we additionally wrote 2 other sound managers using other solutions.
 *
 */
public abstract class SoundManager {
	
	public enum MusicType {
		INTRO("data/music/nesmes-shortIntro.mp3", 180000), 
		STORY_INTRO("data/music/nesmes-shortIntro.mp3", 180000),
		ANEWGIRL("data/music/aNewGirlAppears.mp3", 277000),
		CHICKENNOODLE("data/music/chickenNoodleCigarettes.mp3", 183000),
		GAMEBOYRAIDERS("data/music/gameboyRaiders.mp3", 109000),
		FUNKBUMP("data/music/funkBump.mp3", 240000),
		HANDHELDHERO("data/music/handheldHero-Short.mp3", 153000),
		BADMUSIC("data/music/reckoner.mp3", 100000),
		MAP_DEFAULT("data/music/f3ll_1n_l0v3_w17h_a_g1rl.mp3", 110000),
		REEF("data/music/reef.mp3", 300000),
		POTATO("data/music/potato.mp3", 105000);
		
		private String path;
		private int length; // playing time in msecs
		private AudioPlayer player;
		MusicType(String path, int length) {
			this.path = path;
			this.length = length;
			//player = minim.loadFile(sketchPath+path);
		}
		
		public String getPath() {
			return path;
		}
		
		public int getLength() {
			return length;
		}
		
		public AudioPlayer getPlayer() {
			return minim.loadFile(sketchPath+path);
//			return this.player;
		}
	}
	
	public enum SoundType {
		JUMP("data/sounds/jump.wav", 0),
		COIN("data/sounds/coin0.wav", 10),
		FLIP("data/sounds/flip.wav", 0),
		MENUCONF("data/sounds/menu_conf.wav", 0),
		SPAWN("data/sounds/spawn.wav", 0),
		TELEPORT("data/sounds/teleport.wav", 0),
		ERROR("data/sounds/error2.wav", 0);
		
		private String path;
		private int lastExec;
		private int delay;
		private AudioPlayer player;
		SoundType(String path, int delay) {
			this.path = path;
			this.delay = delay;
			this.player = minim.loadFile(sketchPath+path);
			player.setGain(20);
		}
		
		public void setLastExec() {lastExec = M3Platformer.getInstance().millis();}
		public boolean isPlayable() {return M3Platformer.getInstance().millis() - lastExec >= delay;}
		public String getPath() { return this.path; }
		public AudioPlayer getPlayer() { return this.player; }
	}
	
	// info about currently playing music
	private static MusicType currentlyPlaying;
	private static Minim minim;
	private static AudioPlayer musicPlayer;
	
	private static String sketchPath = ""; //"/home/arcade/games/M3Platformer/";
	
	public static void init() {
//		sketchPath = M3Platformer.getInstance().sketchPath("");
		
		minim = new Minim(M3Platformer.getInstance());
		
		SoundType tmp = SoundType.COIN;
		MusicType tmp2 = MusicType.INTRO;
	}
	
	public static void playSound(SoundType s) {
		if(!s.isPlayable()) return;
		s.setLastExec();
		
		s.getPlayer().play(0);
	}
	
	public static void playJump() {
		playSound(SoundType.JUMP);
	}
	public static void playCoin() {
		playSound(SoundType.COIN);
	}
	
	public static void stopMusic() {
		if(currentlyPlaying != null && musicPlayer!=null) {
			musicPlayer.pause();
			currentlyPlaying = null;
		}
	}
	
	public static void playIntroMusic() {
		playMusic(MusicType.INTRO);
	}
	
	public static void playMapMusic() {
		playMusic(MusicType.MAP_DEFAULT);
	}
	
	public static void playStoryMusic() {
		playMusic(MusicType.POTATO);
	}
	
	public static void playMusic(MusicType t) {
		
		// do not restart song already playing
		if(currentlyPlaying == t) return;
		
		stopMusic();
		currentlyPlaying = t;
		
		musicPlayer = t.getPlayer();
		musicPlayer.loop(0);
	}
}
