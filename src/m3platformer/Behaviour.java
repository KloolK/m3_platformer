package m3platformer;

public enum Behaviour
{
	Top, Bottom, Left, Right, LeftRight, Opposite, Clockwise, Anticlockwise
}
