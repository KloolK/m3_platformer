package m3platformer;

import java.util.*;
import m3platformer.animations.*;
import objects.DeathZone;
import objects.Drawable;
import objects.WorldObject;

// simple hard coded main menu background
public class MainMenuBackground extends MenuBackground {
	private Sprite bg;
	private boolean hasTimeout = false;
	private Set<Drawable> objects = new HashSet<Drawable>();
	
	// sprites for animations
	private Sprite alanSadRight;
	private Sprite alinaAngryRight;
	private Sprite alanIdle;
	private Sprite alanLeftLookingRight;
	private Sprite alanLyingWink;
	
	// command to start a new animation
	private Command startCmd = new Command() {
		public void call(M3Platformer game) {
			((MainMenuBackground)game.getLogic().getMainMenu().getBg()).startAnimation();
		}
	};
	
	private Command timeoutCmd = new Command() {
		public void call(M3Platformer game) {
			((MainMenuBackground)game.getLogic().getMainMenu().getBg()).startTimeout();
		}
	};
	
	public MainMenuBackground() {		
		bg = new Sprite(parent, "data/imgs/logo/menuBG.gif");
		alanIdle = new AnimatedSprite(parent, "data/imgs/alan/idle.gif", 2f);
		alanSadRight = new Sprite(parent, "data/imgs/alan/rightSad.gif", 2f);
		alinaAngryRight = new Sprite(parent, "data/imgs/alina/rightAngry.gif", 2f);
		alanLeftLookingRight = new Sprite(parent, "data/imgs/alan/leftLookingRight.gif", 2f);
		alanLyingWink = new AnimatedSprite(parent, "data/imgs/alan/lyingWink.gif", 2f);
	}
	
	
	// do some funny stuff
	public void startAnimation() {
		objects.clear();
		Animation a,b,d;
		
		float x = parent.random(0, 3.999999f);
		int tmp = (int)x;
		
		switch(tmp) {
		
			// chasing the rabbit
			case 0:
				alanSadRight.setPosition(-180, 200 - (int)parent.random(20));
				alinaAngryRight.setPosition(-50, 195 - (int)parent.random(30));
				
				a = new Move(8000, new Vector2D(630,0));
				alinaAngryRight.addAnimation(a);
				
				a = new Move(8000, new Vector2D(580,0));
				a.setFinishCmd(timeoutCmd);
				alanSadRight.addAnimation(a);
				
				objects.add(alanSadRight);
				objects.add(alinaAngryRight);
				break;
				
			// what's up? bottom
			case 1:
				
				alanIdle.setPosition(20 + (int)parent.random(40), parent.height+30);
				a = new Move(4000, new Vector2D(0,-60), true);
				d = new Delay(1500);
				b = new Move(800, new Vector2D(0,60), true);
				a.setFollowUp(d);
				d.setFollowUp(b);
				b.setFinishCmd(timeoutCmd);
				alanIdle.addAnimation(a);
				((AnimatedSprite)alanIdle).jump(0);
				
				objects.add(alanIdle);
				break;
				
			// whats up? right
			case 2:
					
				alanLyingWink.setPosition(parent.width + 30, 145 + (int)parent.random(40));
				a = new Move(3000, new Vector2D(-60,0), true);
				d = new Delay(2500);
				b = new Move(800, new Vector2D(60,0), true);
				a.setFollowUp(d);
				d.setFollowUp(b);
				b.setFinishCmd(timeoutCmd);
				alanLyingWink.addAnimation(a);
				((AnimatedSprite)alanLyingWink).jump(0);
				
				objects.add(alanLyingWink);
				break;
			
			// escape!
			case 3:
				
				alanLeftLookingRight.setPosition(parent.width+50, 180 - (int)parent.random(20));
								
				a = new Move(5000, new Vector2D(-580,0));
				a.setFinishCmd(timeoutCmd);
				alanLeftLookingRight.addAnimation(a);
				
				// death zone
				Drawable deathZone;
				
				for(int i=0; i<3; i++) {
					int size = 5 + (int)parent.random(0,10);
					deathZone = new DeathZone(M3Platformer.getInstance(), parent.width+110 + (int)parent.random(-10,20), 190+ (int)parent.random(-10,25), size, size);
					a = new Move(5000, new Vector2D(-580,0));
					deathZone.addAnimation(a);
					objects.add(deathZone);
				}
				
				objects.add(alanLeftLookingRight);
				break;
		}
		
		
	}
	
	// start a new timeout for the next animation
	public void startTimeout() {
		parent.getLogic().registerTimeout(new Timeout(M3Platformer.getInstance(), 5000, this.startCmd));
	}
	
	
	// draw background and animated objects
	@Override
	public void draw() {
		bg.draw(0,0);
		
		// objects?
		if(objects.size() > 0) {
			for(Drawable d : objects) {
				if(d instanceof WorldObject)
					d.draw((int)d.getX(), (int)d.getY());
				else if (d instanceof Sprite)
					d.draw(0,0);
			}
		}
		
		// register first timeout
		if(!hasTimeout) {
			hasTimeout = true;
			startTimeout();
		}
	}
}
