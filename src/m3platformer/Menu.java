package m3platformer;

import java.util.*;

import m3platformer.SoundManager.SoundType;


/**Simple on screen menu
 * Provides up/down and execute methods using the {@link Command}s
 *
 */
class Menu {
	
  /**single menu entry
   * uses command-object to store information about what to do if entry is chosen
   */
  class MEntry {
     final String name;
     boolean focus = false;
     final Command cmd;
     final Menu menu;
     
     MEntry(String name, Command cmd, Menu menu) {
       this.name = name; this.cmd = cmd; this.menu = menu;
     }
     
     void exec() { this.cmd.call(this.menu.game); }
  }
  
  // attributes
  final LinkedList<MEntry> entries = new LinkedList<MEntry>();
  M3Platformer game;
  String title;
  int offset = 0;
  int marginTitle = 200;
  MenuBackground bg;
  
  Menu(M3Platformer game, String title) {
      this.title = title;
      this.game = game;
  }
  
  Menu(M3Platformer game, String title, int offset, int marginTitle) {
      this(game, title);
      this.offset = offset;
      this.marginTitle = marginTitle;
  }
  
  void addEntry(String name, Command cmd) {
      MEntry tmp = new MEntry(name, cmd, this);
      tmp.focus = entries.size() == 0;
      entries.add(tmp);
  }
  
  void reset() {
    for(MEntry m: entries) m.focus = false;
    entries.getFirst().focus = true;
  }
  
  private int getFocusIndex() { 
    for(MEntry m: entries) { if(m.focus) return entries.indexOf(m); }
    return -1;
  }
  
  void up() {
    int i = getFocusIndex();
    entries.get(i).focus = false;
    
    if(i == 0) entries.getLast().focus = true;
    else entries.get(i-1).focus = true;
  }
  
  void down() {
    int i = getFocusIndex();
    entries.get(i).focus = false;
    
    if(i == entries.size() -1) entries.getFirst().focus = true;
    else entries.get(i+1).focus = true;
  }
  
  void exec() {
	  SoundManager.playSound(SoundType.MENUCONF);
	  
    entries.get(getFocusIndex()).exec();
//    this.reset();
  }
  
 
  void drawShape() {	
	  
	  if(bg == null) game.background(0);
	  else bg.draw();
	  
      // title
      //game.fill(game.titleColors[game.titleColor]);
      //game.textFont(game.titleFont);
	  game.fill(236,155,1);
      game.textAlign(M3Platformer.CENTER, M3Platformer.BOTTOM);
      game.text(title, game.width/2, this.offset);
      
      //game.fill(game.textColor);
      // entries
      game.textFont(game.normalTextFont);
      game.stroke(game.textColor);
      game.strokeWeight(3);
      int offset=this.offset + this.marginTitle;
      for(MEntry m : entries) {
    	  if(m.focus) {
        	game.fill(255,247,0); 
          }
    	  
    	  else game.fill(236,155,1);
    		  
    	  game.text(m.name, game.width/2, offset);
                    
          offset+=this.offset;
      }
  }

	public MenuBackground getBg() {
		return bg;
	}
	
	public void setBg(MenuBackground bg) {
		this.bg = bg;
	}
}
