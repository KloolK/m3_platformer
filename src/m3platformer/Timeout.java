package m3platformer;

public class Timeout {
   final int end;
   final GameMode targetStatus;
   final Command cmd;
   final Object cmdObj;
   final M3Platformer game;
   final int id;
   static int maxTimeoutId = 0;
   
   public Timeout(M3Platformer game, int msec, GameMode status) {
      id = maxTimeoutId++;
      this.game = game;
      end = game.millis() + msec;
      this.targetStatus = status;
      game = null;
      cmd = null;
      this.cmdObj = null;
   }
   
   public Timeout(M3Platformer game, int msec, Command cmd) {
      id = maxTimeoutId++;
      end = game.millis() + msec;
      this.cmd = cmd;
      this.cmdObj = null;
      this.game = game;
      this.targetStatus = GameMode.MainMenu;
   }
   
   public Timeout(M3Platformer game, Object o, int msec, Command cmd) {
      id = maxTimeoutId++;
      end = game.millis() + msec;
      this.cmd = cmd;
      this.cmdObj = o;
      this.game = game;
      this.targetStatus = GameMode.MainMenu;
   }
   
   void call() {
      if(cmdObj != null)
    	  this.cmd.call(this.game, cmdObj);
      else this.cmd.call(this.game);
   }
   
   boolean check() {
      return game.millis() >= end;
   }
   
   public boolean equals(Object o) {
      return (o instanceof Timeout) && ((Timeout) o).id == this.id; 
   }
}
