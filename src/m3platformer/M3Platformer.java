package m3platformer;

import java.awt.Color;

import objects.PlayerFigure;
import processing.core.PApplet;
import processing.core.PFont;

/** Processing sketch file
 * 
 * Interface for processing. Sets setup, draw and input methods.
 * Singleton class.
 *
 *  Multimodal Media Madness RWTH 08.06.2012
 *  @author Jakob Bauer, Jan Bruckner, Michael Deutschen
 */
public class M3Platformer extends PApplet
{
	private GameLogic logic;
	public Controller controller;
	protected static M3Platformer instance;
	
	// normal window size
	private final int standardWidth = 320;
	private final int standardHeight = 240;
	
	// fonts and colors
	final int textColor = 0x666666;
	PFont normalTextFont;
	PFont titleFont;

	public M3Platformer() {
		super();
		if(instance != null)
			throw new RuntimeException("Double Instance of PApplet!");

		instance = this;
	}

	public static M3Platformer getInstance() {
		return instance;
	}

	public static void main(String args[])
	{
		PApplet.main(new String[] { m3platformer.M3Platformer.class.getName() });
	}

	public void setup()
	{
		size(320,240);
		
		// Define window size.
		resetSize();
		noSmooth();
		
		// Define FPS.
		frameRate(25);
		frame.setTitle("Alan and Alina");
				
		// preload files
		SoundManager.init();
		ResourceManager.init();
		
		normalTextFont = titleFont = ResourceManager.getFont("data/fonts/8bitwonder12Smooth.vlw");
		
		GameMap map = null; //GameMap.loadFromFile("data/maps/jan0.map");
		PlayerFigure player = new PlayerFigure(this, Layer.Layer1, new Color(12,20,31), new Color(111,195,223), 60, 10, 15, 25);
//		player.applyColorScheme(map.getColorScheme());
		Camera camera = new Camera(this, map, player, 0, 30, width, height-30);
				
		this.controller = new Controller(this);
		Physics physics = new Physics(this, controller, map, player);
		MapEditor editor = new MapEditor(this, controller, player, 0, 0, width, height);
		
		// create main menu
		Menu mM = new Menu(this, "", 17, 150);
		mM.addEntry("STORY MODE", new Command() {
			public void call(M3Platformer game) {
				game.getLogic().startStoryIntro();
			}
		});
		
//		mM.addEntry("MAP EDITOR", new Command() {
//			public void call(M3Platformer game) {
//				game.getLogic().startGameModeTransition(GameMode.MapEditor, null);
//			}
//		});
		
		mM.addEntry("HIGHSCORE", new Command() {
			public void call(M3Platformer game) {
				game.getLogic().openHighscore();
			}
		});
		
		mM.addEntry("CREDITS", new Command() {
			public void call(M3Platformer game) {
				game.getLogic().startCredits();
			}
		});
		
		mM.setBg(new MainMenuBackground());
		
		
				
		this.setLogic(new GameLogic(this, physics, camera, controller, player, editor, mM));
		this.getLogic().startIntro();
	}

	public void draw()
	{
		// tick-tock
		this.getLogic().tick();
		this.getLogic().tock();
	}

	// Called every time a key is pressed.
	public void keyPressed()
	{
		this.controller.keyPressed();
	}

	// Called every time a key is released.
	public void keyReleased()
	{
		this.controller.keyReleased();
	}
	
	public void fill(Color c) {
		super.fill(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha());
	}
	
	public void resetSize() {
		size(standardWidth, standardHeight);
	}
	
	public PlayerFigure getPlayer() {
		return this.getLogic().player;
	}

	public GameLogic getLogic() {
		return logic;
	}

	public void setLogic(GameLogic logic) {
		this.logic = logic;
	}
}