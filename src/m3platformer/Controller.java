package m3platformer;
import processing.core.PApplet;

/** Controller wrapper
 * Checks and stores which buttons are pressed and which were just pressed in the current tick.
 */
public class Controller
{
	M3Platformer parent;

	// Key status'.
	private boolean[] keyStatus = new boolean[ControllerKey.values().length];
	private boolean[] keyJustPressed = new boolean[ControllerKey.values().length];

	// Constructor.
	public Controller(M3Platformer parent)
	{
		this.parent = parent;
	}

	// Called when key is pressed.
	public void keyPressed()
	{
		this.switchButton(true);
	}

	// Called when key is released.
	public void keyReleased()
	{
		this.switchButton(false);
	}
	
	// sets key state
	private void setJustPressed(ControllerKey key, boolean onoff) {
		keyStatus[key.ordinal()] = onoff;
		if(onoff) keyJustPressed[key.ordinal()] = true;
	}

	// Switches the status of last pressed/released key.
	private void switchButton(boolean tof)
	{
		if (this.parent.key != PApplet.CODED)
		{
			// key name = key char?
			try{
				ControllerKey key = ControllerKey.valueOf(""+this.parent.key);
				setJustPressed(key, tof);
			}
			
			// key not found -> special key?
			catch(IllegalArgumentException e) {
				switch (this.parent.key)
				{
					case PApplet.DELETE:
						setJustPressed(ControllerKey.del, tof);
					break;
					
					case '1':
						setJustPressed(ControllerKey.one, tof);
					break;
					
					case '2':
						setJustPressed(ControllerKey.two, tof);
					break;
					
					case '8':
						setJustPressed(ControllerKey.eight, tof);
					break;
					
					case '9':
						setJustPressed(ControllerKey.nine, tof);
					break;
				}
			}
		}
		
		
		//check keyCode variable for special Keys
		else
		{
			switch (this.parent.keyCode)
			{
			case PApplet.UP:
				setJustPressed(ControllerKey.up, tof);
				break;
			case PApplet.DOWN:
				setJustPressed(ControllerKey.down, tof);
				break;
			case PApplet.LEFT:
				setJustPressed(ControllerKey.left, tof);
				break;
			case PApplet.RIGHT:
				setJustPressed(ControllerKey.right, tof);
				break;
			case PApplet.SHIFT:
				setJustPressed(ControllerKey.shift, tof);
				break;
			case PApplet.CONTROL:
				setJustPressed(ControllerKey.control, tof);
				break;
			}
		}
	}

	public void resetJustPressed() {
		for(int i=0;i<keyJustPressed.length;i++) keyJustPressed[i] = false;
	}

	/*
	 * FOLLOWING: GETTER OF ALL USED KEYS!
	 */
	public boolean isKeyPressed(ControllerKey key) {
		return keyStatus[key.ordinal()];
	}

	// only true if key has been pressed in this tick for first time (not been pressed longer)
	public boolean getKeyJustPressed(ControllerKey key) {
		return keyJustPressed[key.ordinal()];
	}
}
