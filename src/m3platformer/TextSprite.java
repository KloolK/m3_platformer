package m3platformer;

import java.awt.Color;

import objects.Drawable;
import processing.core.PApplet;
import processing.core.PFont;

public class TextSprite extends Drawable {

	private M3Platformer parent;
	private PFont font;
	private Color color;
	private int size;
	private String content;
	private int alpha = 255;
	
	private int alignX;
	private int alignY;
	
	// IMPORTANT: position is interpreted according to alignment!
	// standard alignment is LEFT, TOP
	
	public TextSprite(String content, PFont font, Color color, int size) {
		this.font = font;
		this.color = color;
		this.size = size;
		this.content = content;
		
		parent = M3Platformer.getInstance();
		parent.textFont(font);
		parent.textSize(size);
		alignX = PApplet.LEFT;
		alignY = PApplet.TOP;
		
		this.dimensions = new Vector2D(parent.textWidth(content), size);
	}
	
	@Override
	protected void drawShape(int x, int y) {
		parent.fill(color.getRed(), color.getGreen(), color.getBlue(), alpha);
		parent.textFont(font);
		parent.textSize(size);
		parent.textAlign(alignX, alignY);
		parent.text(content, x+getX(), y+getY());
	}
	
	public void setAlpha(int alpha) {
		this.alpha = alpha;
	}
	
	public void setAlign(int x, int y) {
		this.alignX = x;
		this.alignY = y;
	}
}
