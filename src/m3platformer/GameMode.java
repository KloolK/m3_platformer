package m3platformer;

/** Current state of the game
 *
 */
public enum GameMode {
	
	// game intro
	Intro,
	IntroHighscore, // highscore output during intro
	
	// main menu is displayed
	MainMenu,
	
	// map editor is running
	MapEditor,
	
	// highscore
	Highscore,
	HighscoreInput,
	
	// story animation
	StoryIntro,
	
	// stage intro (camera path)
	StageIntro,
	
	// game is running
	Running,
	
	// game is freezed (death animation running?)
	FreezeGame,
	
	// pause menu (freezed game and menu rendered)
	PauseMenu,
	
	// scoring screens
	SingleTryScoring,   // single try finished
	StageScoring,		// single stage cleared
	Credits,       // all stages cleared
	
	// player failed ;)
	GameOver;
}
