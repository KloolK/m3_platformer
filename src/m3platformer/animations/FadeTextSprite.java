package m3platformer.animations;

import objects.Drawable;
import m3platformer.TextSprite;
import m3platformer.Vector2D;

/*
 * animation to fade in/out a textsprite (do not use to fade other stuff!)
 */
public class FadeTextSprite extends Animation {
	
	private boolean fadeIn; // fade direction (in / out)
	
	public FadeTextSprite(int duration, boolean fadeIn) {
		super(duration);
		this.fadeIn = fadeIn;
	}
	
	@Override
	public boolean applyPreDraw(Drawable obj, Vector2D pos) {
		
		super.applyPreDraw(obj, pos);
		
		float alpha = 1;
		if(fadeIn) alpha *=     getPercentage();
		else 	   alpha *= 1 - getPercentage();
		
		// discrete alpha
		alpha = (int)(alpha * 20) / (float)20;
		
		((TextSprite) obj).setAlpha((int) (alpha*255));
		
		return true;
	}
}
