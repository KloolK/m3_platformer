package m3platformer.animations;

import objects.Drawable;
import m3platformer.M3Platformer;
import m3platformer.Vector2D;

/*
 * animation to spin object around a specified amount of times
 */
public class Spin extends Animation {

	// may also be negative or float
	private float timesTotal;
	
	private float curAngle = 0;
	
	public Spin(int duration, float times) {
		super(duration);
		
		this.timesTotal = times;
	}

	@Override
	public boolean applyPreDraw(Drawable obj, Vector2D pos) {
		super.applyPreDraw(obj, pos);
		
		M3Platformer applet = M3Platformer.getInstance();
		curAngle = (getPercentage() * M3Platformer.TWO_PI * timesTotal) % M3Platformer.TWO_PI;
		
		applet.rotate(curAngle);
		
		// draw it!
		return true;
	}
}
