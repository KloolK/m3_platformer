package m3platformer.animations;

import java.util.*;

import objects.Drawable;
import m3platformer.Vector2D;

/*
 * repeats a list of animations (one animation running at a time)
 */
public class RepeatedAnimation extends Animation {
	
	// list of animations to be repeated
	private final List<Animation> list = new LinkedList<Animation>();
	private Animation current = null;
	private int timesTotal = 0;
	private int timesFinished = 0;
	
	public RepeatedAnimation() {
		this(-1);
	}

	// timesTotal <= 0 -> endless repetition
	public RepeatedAnimation(int timesTotal) {
		// dummy, duration value is not important here (not used anywhere)
		super(100);
		
		this.timesTotal = timesTotal;
	}
	
	@Override
	public boolean applyPreDraw(Drawable obj, Vector2D pos) {
		// no animations to apply
		if(list.size() == 0) return true;
		
		// no animation started yet
		if(current==null) {
			current = list.get(0);
			current.resetStartTime();
		}
		
		return current.applyPreDraw(obj, pos);
	}
	
	@Override
	public boolean applyPostDraw(Drawable obj, Vector2D pos) {
		// no animations to apply
		if(list.size() == 0) return true;
		
		// start next animation
		if(current.applyPostDraw(obj, pos)) {
			
			// if list is completed, we will begin with the first of the list next applyPreDraw
			int curIndex = list.indexOf(current);
			if(list.size()-1>curIndex) {
				current = list.get(curIndex+1);
				current.resetStartTime();
			}
			
			else {
				current = null;
				timesFinished++;
			}
		}
		
		// we will never finish, animation list is repeated!
		return (timesTotal>0) && (timesFinished>=timesTotal);
	}
	
	public RepeatedAnimation addAnimation(Animation a) {
		list.add(a);
		
		// chaining
		return this;
	}
}
