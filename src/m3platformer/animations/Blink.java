package m3platformer.animations;

import objects.Drawable;
import m3platformer.M3Platformer;
import m3platformer.Vector2D;

/*
 * animation to make object blink
 * duration is interpreted as timesTotal*(on+off) phase
 */
public class Blink extends Animation {
	
	private int timesTotal;
	
	public Blink(int duration, int timesTotal) {
		super(duration);
		this.timesTotal = timesTotal;
	}
	
	@Override
	public boolean applyPreDraw(Drawable obj, Vector2D pos) {
		
		super.applyPreDraw(obj, pos);
		
		int curTime = M3Platformer.getInstance().millis();
		return (Math.min(curTime - startTime, duration-1) % (duration/timesTotal)) < (duration/timesTotal/2);
	}
}
