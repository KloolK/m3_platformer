package m3platformer.animations;

import objects.Drawable;
import m3platformer.M3Platformer;
import m3platformer.Vector2D;

public class Scale extends Animation {
	
	private final Vector2D startScale;
	private final Vector2D targetScale;

	public Scale(int duration, float startScale, float targetScale) {
		super(duration);
		
		this.targetScale = new Vector2D(targetScale, targetScale);
		this.startScale = new Vector2D(startScale, startScale);
	}
	
	public Scale(int duration, Vector2D startScale, Vector2D targetScale) {
		super(duration);
		
		this.startScale = startScale;
		this.targetScale = targetScale;
	}

	@Override
	public boolean applyPreDraw(Drawable obj, Vector2D pos) {
		
		super.applyPreDraw(obj, pos);
		
		M3Platformer applet = M3Platformer.getInstance();
		
		float fx = (startScale.getX() > targetScale.getX() ? easeOutExpo(getPercentage()) : easeInExpo(getPercentage()));
		float fy = (startScale.getY() > targetScale.getY() ? easeOutExpo(getPercentage()) : easeInExpo(getPercentage()));
		applet.scale(startScale.getX() - fx * (startScale.getX()-targetScale.getX()), startScale.getY() - fy * (startScale.getY()-targetScale.getY()));
		
		return true;
	}
}
