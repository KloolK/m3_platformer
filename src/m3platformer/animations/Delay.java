package m3platformer.animations;

/*
 * dummy animation for delaying an animation chain
 * does nothing but wait till duration is over
 */
public class Delay extends Animation {
	public Delay(int duration) {
		super(duration);
	}
}
