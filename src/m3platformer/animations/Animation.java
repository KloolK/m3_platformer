package m3platformer.animations;

import objects.Drawable;
import m3platformer.Command;
import m3platformer.M3Platformer;
import m3platformer.Vector2D;

public abstract class Animation {
	
	// time after which the whole animation is completed in msecs
	protected int duration = 0;
	
	// starting time of the animation in msecs
	protected int startTime = -1;
	
	// animation to be started after finishing this one
	protected Animation followUp = null;
	
	// finish action
	protected Command finishCmd = null;
	
	public static boolean speedup=false;
	
	public Animation(int duration) {
		this.duration = duration;
		if(speedup) {
			this.duration = duration/10;
		}
	}
	
	
	// method to be called before drawing the object
	// returns true iff the object should be drawn
	// WARNING: uses side effects to change draw position of object
	public boolean applyPreDraw(Drawable obj, Vector2D pos){
		if(startTime <= 0) this.startTime = M3Platformer.getInstance().millis();
		M3Platformer.getInstance().pushMatrix();
		
		return true;
	}
	
	// method to be called after drawing the object
	// returns true iff the animation has completed
	public boolean applyPostDraw(Drawable obj, Vector2D pos) {
		
		M3Platformer.getInstance().popMatrix();
		
		boolean finished = M3Platformer.getInstance().millis() >= startTime+duration;
		
		// execute finish command
		if(finished && finishCmd!=null) finishCmd.call(M3Platformer.getInstance());
		
		return finished;
	}
	
	
	
	// easing (by jquery ui)
	public static float easeInElastic(float f) {
		
		f = Math.max(0, Math.min(1,f));
		
		float b = 0; float c = 1; float t=f; float d=1;
        float s=1.70158f;float p=0;float a=c;
        
        if (t==0) return b;  
        if ((t/=d)==1) return b+c;  
        if (p==0) p=d*0.3f;
        
        if (a < Math.abs(c)) { a=c; s=p/4; }
        else s = (float) (p/(2*Math.PI) * Math.asin (c/a));
        return -(float)(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
    }
	
	public static float easeInBack(float f) {
		f = Math.max(0, Math.min(1,f));
		float s = 1.70158f;
        return (float)(1*(f/=1)*f*((s+1)*f - s));
	}
	
	public static float easeInExpo(float f) {
		f = Math.max(0, Math.min(1,f));
		return (float)((f==0) ? 0 : Math.pow(2, 10 * (f/1 - 1)));
	}
	
	public static float easeOutExpo(float f) {
		f = Math.max(0, Math.min(1,f));
		return (float)((f==1) ? 1 : (-Math.pow(2, -10 * f) + 1));
	}
	
	
	public float getPercentage() {
		int curTime = M3Platformer.getInstance().millis();
		return Math.min(1, Math.max(0, (float)(curTime - startTime) / duration));
	}
	

	public int getDuration() {
		return duration;
	}

	public Animation setDuration(int duration) {
		this.duration = duration;
		
		// chaining
		return this;
	}

	public Animation getFollowUp() {
		return followUp;
	}

	public Animation setFollowUp(Animation followUp) {
		this.followUp = followUp;
		
		// chaining
		return this;
	}
	

	public int getStartTime() {
		return startTime;
	}

	public Animation resetStartTime() {
		this.startTime = -1;
		
		// chaining
		return this;
	}
	
	public Animation setFinishCmd(Command cmd) {
		this.finishCmd = cmd;
		
		// chaining
		return this;
	}
}
