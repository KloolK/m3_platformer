package m3platformer.animations;

import objects.Drawable;
import m3platformer.Sprite;
import m3platformer.Vector2D;

/*
 * animation to fade in/out a sprite (only works for images!)
 * IMPORTANT: effect keeps applied after animation
 */
public class FadeImage extends Animation {
	
	private boolean fadeIn; // fade direction (in / out)
	
	public FadeImage(int duration, boolean fadeIn) {
		super(duration);
		this.fadeIn = fadeIn;
	}
	
	@Override
	public boolean applyPreDraw(Drawable obj, Vector2D pos) {
		
		super.applyPreDraw(obj, pos);
		
		float alpha = 1;
		if(fadeIn) alpha *=     getPercentage();
		else 	   alpha *= 1 - getPercentage();
		
		// discrete alpha
		alpha = (int)(alpha * 20) / (float)20;
		((Sprite) obj).setAlpha(alpha*255);
		
		return true;
	}
}
