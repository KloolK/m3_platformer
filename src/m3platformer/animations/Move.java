package m3platformer.animations;

import objects.Drawable;
import m3platformer.Vector2D;

/*
 * animation to move object using a vector
 * does not really move the object but only its drawing position (collision is not affected!)
 */
public class Move extends Animation {
	
	private Vector2D direction;
	private boolean stayThere; // should the object stay at target position after animation?
	
	public Move(int duration, Vector2D direction) {
		this(duration, direction, false);
	}
	
	public Move(int duration, Vector2D direction, boolean stayThere) {
		super(duration);
		this.direction = direction;
		this.stayThere = stayThere;
	}
	
	@Override
	public boolean applyPreDraw(Drawable obj, Vector2D pos) {
		
		super.applyPreDraw(obj, pos);
		
		Vector2D tmp = new Vector2D(direction);		
		tmp.scale(getPercentage());
		pos.add(tmp);
		
		return true;
	}
	
	@Override
	public boolean applyPostDraw(Drawable obj, Vector2D pos) {
		boolean finish = super.applyPostDraw(obj, pos);
		if(finish && stayThere) {
			Vector2D objPos = obj.getPosition();
			objPos.add(direction);
			obj.setPosition(objPos.getX(), objPos.getY());
		}
		
		return finish;
	}

	public Vector2D getDirection() {
		return direction;
	}
}
