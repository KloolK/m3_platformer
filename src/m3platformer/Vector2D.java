package m3platformer;

public class Vector2D {
	private float x = 0;
	private float y = 0;
	
	public Vector2D() {}
	public Vector2D(float x, float y) {
		setXY(x,y);
	}
	
	// copy contructor
	public Vector2D(Vector2D v) {
		setVect(v);
	}
	
	// scales vector
	public Vector2D scale(float factor) {
		setXY(getX() * factor, getY() * factor);
		
		return this;
	}
	
	// scales vector to given length
	public Vector2D scaleToLength(float newLen) {
		float len = this.getLength();
		if(len == 0) return this;
		this.scale(newLen / len);
		
		return this;
	}
	
	// normalizes vector to length 1
	public Vector2D normalize() {
		return scaleToLength(1);
	}
	
	// adds another vector
	public Vector2D add(Vector2D v) {
		setXY(getX() + v.getX(), getY() + v.getY());
		return this;
	}
	public Vector2D sub(Vector2D v) {
		setXY(getX() - v.getX(), getY() - v.getY());
		return this;
	}
	
	// adds constants
	public Vector2D add(float x, float y) {
		setXY(getX()+x, getY()+y);
		return this;
	}
	
	// two vectors are equal if there components are
	public boolean equals(Object o) {
		if(!(o instanceof Vector2D)) return false;
		return ((Vector2D)o).getX() == getX() && ((Vector2D)o).getY() == getY();
	}
	
	// equality in int accuracy
	public boolean equalsInt(Object o) {
		if(!(o instanceof Vector2D)) return false;
		return (int)((Vector2D)o).getX() == (int)getX() && (int)((Vector2D)o).getY() == (int)getY();
	}
	
	// calculates euclidic length of vector
	public float getLength() {
		return (float)Math.sqrt(getX()*getX() + getY()*getY());
	}
	
	// getters and setters
	public float getX() {return x;}
	public float getY() {return y;}
	public Vector2D setX(float x) {this.x = x; return this;}
	public Vector2D setY(float y) {this.y = y; return this;}
	public Vector2D setVect(Vector2D v) {setXY(v.x,v.y); return this;}
	public Vector2D setXY(float x, float y) {setX(x); setY(y); return this;}
	
	public String toString() {
		return "Vector2D["+this.getX()+","+this.getY()+"]";
	}
}
