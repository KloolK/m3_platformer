package m3platformer;

import java.util.*;

import processing.data.XML;

/*
 * used to specify colors or sprites of worldobjects for serialisation
 */
public class ColorScheme {
	
	// hashmap: ClassName -> ColorWrappers
	private Map<String,List<ColorWrapper>> colorMap = new HashMap<String,List<ColorWrapper>>();
	
	// sprite map
	private Map<String,List<Sprite>> spriteMap = new HashMap<String,List<Sprite>>();
	
	// path+name of scheme file
	private String filename;
	
	// singleton default scheme
	private static ColorScheme defScheme;
	
	
	private ColorScheme() {
		// load default entries
		this.parseXMLFile("data/colorSchemes/default.cs");
	}
	
	public static ColorScheme getDefaultScheme() {
		if(defScheme == null) defScheme = new ColorScheme();
		return defScheme;
	}
	
	// returns colorwrapper for worldobject of specified type
	public ColorWrapper getColorWrapper(String clazz) {
		List<ColorWrapper> l = colorMap.get(clazz);
		if(l == null) return null;
		
		int i = (int) (M3Platformer.getInstance().random(0, l.size() - 0.0001f));
		return l.get(i);
	}
	
	// returns spritelist for worldobject of specified type
	public List<Sprite> getSpriteList(String clazz) {
		List<Sprite> l = spriteMap.get(clazz);
		return l;
	}
	
	// _not_ used by deserialize() !
	public void addColorWrapper(String clazz, ColorWrapper obj) {
		List<ColorWrapper> l;
		if(!colorMap.containsKey(clazz)) {
			l = new LinkedList<ColorWrapper>();
			colorMap.put(clazz, l);
		}
		
		else  l = colorMap.get(clazz);
		
		l.add(obj);
	}
	
	protected XML serialize() {
		XML xml = new XML("ColorScheme");
		
		for(Map.Entry<String,List<ColorWrapper>> me : colorMap.entrySet()) {
			XML child = xml.addChild(me.getKey());
			for(ColorWrapper cw : me.getValue()) {
				child.addChild(cw.serialize());
			}
		}
		
		for(Map.Entry<String,List<Sprite>> me : spriteMap.entrySet()) {
			XML child = xml.addChild(me.getKey());
			XML grandChild = child.addChild("spriteList");
			for(Sprite sp : me.getValue()) {
				grandChild.addChild(sp.serialize());
			}
		}
		
		return xml;
	}
	
	public void deserialize(XML xml) {
		for(XML obj: xml.getChildren()) {
			String clazz = obj.getName();
			
			// does worldobject class exist?
			try{ Class.forName(obj.getName()); }
			catch (java.lang.ClassNotFoundException e) { continue; } 
			
			// colors?
			if(obj.getChildren("colorWrapper").length > 0) {
				List<ColorWrapper> l = new LinkedList<ColorWrapper>();
				for(XML cw: obj.getChildren("colorWrapper")) {
					l.add(ColorWrapper.fromXML(cw));
				}
				colorMap.put(clazz, l);
			}
			
			// sprites
			if(obj.getChildren("spriteList").length > 0) {
				List<Sprite> l = new LinkedList<Sprite>();
				XML spList = obj.getChild("spriteList");
				for(XML sp: spList.getChildren()) {
					if(sp == null) continue;
					
					// simple sprite
					if(sp.getName() == Sprite.class.getName())
						l.add(Sprite.deserialize(M3Platformer.getInstance(), sp));
					
					// animated sprite
					else if(sp.getName() == AnimatedSprite.class.getName())
						l.add(AnimatedSprite.deserialize(M3Platformer.getInstance(), sp));
				}
				spriteMap.put(clazz, l);
			}
		}
	}
	
	// static method to create ColorScheme from file
	public static ColorScheme loadFromFile(String filename) {
		ColorScheme res = new ColorScheme();
		res.parseXMLFile(filename);
		
		return res;
	}
	
	public void saveToFile(String filename) {
		M3Platformer.getInstance().saveXML(serialize(), M3Util.normalizePath(filename));
	}
	
	public void parseXMLFile(String filename) {
		XML mainXML = M3Platformer.getInstance().loadXML(M3Util.normalizePath(filename));
		this.setFilename(filename);
		this.deserialize(mainXML);
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}
}
