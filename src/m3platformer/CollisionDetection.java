package m3platformer;

import java.awt.geom.Rectangle2D;
import java.util.Vector;

abstract class CollisionDetection
{
	/*
	 * returns null at no collision or CollisionResult res with following parameters
	 * - if collsion has happend (with intersection!=0), res.depth is the collision vector, res.touching is false
	 * - if objects are only touching, res.touching is true and res.depth contains information about how the objects are positioned to each other
	 */
	public static CollisionResult getCollisionVector(Rectangle2D rec1, Rectangle2D rec2)
	{
		double x1 = rec1.getCenterX(), x2 = rec2.getCenterX();
		double y1 = rec1.getCenterY(), y2 = rec2.getCenterY();
		double w1 = rec1.getWidth(), w2 = rec2.getWidth();
		double h1 = rec1.getHeight(), h2 = rec2.getHeight();

		// Calculate centers.
		Vector<Float> centerA = new Vector<Float>(2);
		Vector<Float> centerB = new Vector<Float>(2);
		centerA.add((float) x1);
		centerA.add((float) y1);
		centerB.add((float) x2);
		centerB.add((float) y2);
		// Calculate current and minimum-non-intersecting distances between centers.
		float distanceX = centerA.get(0) - centerB.get(0);
		float distanceY = centerA.get(1) - centerB.get(1);
		float minDistanceX = (int) (w1 / 2 + w2 / 2);
		float minDistanceY = (int) (h1 / 2 + h2 / 2);

		// If we are not intersecting at all, return null.
		if (Math.abs(distanceX) > minDistanceX || Math.abs(distanceY) > minDistanceY || (Math.abs(distanceX) == minDistanceX && Math.abs(distanceY) == minDistanceY))
			return null;
		
		// if we are touching, return (0,0)
		else if(Math.abs(distanceX) == minDistanceX || Math.abs(distanceY) == minDistanceY) {
			Vector<Float> res = new Vector<Float>(2);
			res.add(Math.abs(distanceX) == minDistanceX ? distanceX/Math.abs(distanceX) : 0);
			res.add(Math.abs(distanceY) == minDistanceY ? distanceY/Math.abs(distanceY) : 0);
			return new CollisionResult(res, true);
		}

		// Calculate and return intersection depths.
		float depthX = distanceX > 0 ? minDistanceX - distanceX : -minDistanceX - distanceX;
		float depthY = distanceY > 0 ? minDistanceY - distanceY : -minDistanceY - distanceY;

		Vector<Float> res = new Vector<Float>(2);
		res.add(depthX);
		res.add(depthY);
		return new CollisionResult(res, false);
	}

	/* 
	RECT/RECT COLLISION FUNCTION
	Jeff Thompson // v0.9 // November 2011 // www.jeffreythompson.org

	Takes 8 arguments:
	  + x,y position of object 1 - in this case "you"
	  + width and height of object 1 - also "you"
	  + x,y position of object 2 - in this case the static rectangle
	  + width and height of object 2
	  
	*/
	public static boolean RectRect(Rectangle2D rec1, Rectangle2D rec2)
	{
		// TODO: kacke mit den casts??
		//int x1 = (int)rec1.getCenterX(), x2 = (int)rec2.getCenterX();
		//int y1 = (int)rec1.getCenterY(), y2 = (int)rec2.getCenterY();
		//int w1 = (int)rec1.getWidth(), w2 = (int)rec2.getWidth();
		//int h1 = (int)rec1.getHeight(), h2 = (int)rec2.getHeight();
		double x1 = rec1.getCenterX(), x2 = rec2.getCenterX();
		double y1 = rec1.getCenterY(), y2 = rec2.getCenterY();
		double w1 = rec1.getWidth(), w2 = rec2.getWidth();
		double h1 = rec1.getHeight(), h2 = rec2.getHeight();

		// test for collision
		if (x1 + w1 / 2 >= x2 - w2 / 2 && x1 - w1 / 2 <= x2 + w2 / 2 && y1 + h1 / 2 >= y2 - h2 / 2 && y1 - h1 / 2 <= y2 + h2 / 2)
		{
			return true; // if a hit, return true
		} else
		{ // if not, return false
			return false;
		}
	}
}