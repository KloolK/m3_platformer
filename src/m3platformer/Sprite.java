/*
 *  Sprite class.
 *  Represents a single image with basic functionality.
 *  
 *  Multimodal Media Madness RWTH 07.06.2012
 *  Jakob Bauer, Jan Bruckner, Michael Deutschen
 */

package m3platformer;

import processing.core.PImage;

import objects.Drawable;
import processing.data.XML;

public class Sprite extends Drawable
{
	// The parent PApplet that we will render ourselves onto
	M3Platformer parent;
	
	// The file name of the image (e.g. background.jpg).
	String spriteName;
	
	// The image that shall be displayed.
	PImage img;
	
	// semantic definition of position: upper left corner and size of the image, relative to the wrapping object's position
	
	
	// The scale of the image.
	float scale = 1;
	
	// alpha value, [0, 255]
	float alpha = 255;
	
	// The rotation of the image. Value: 0 - 360.
	float rotation = 0;
	
	// Basic Constructor.
	public Sprite(M3Platformer parent, String spriteName)
	{
		// Construct at upper left corner, size 100, not scaled, not rotated.
		this(parent, spriteName, 0, 0, 1.0f, 0.0f);
	}

	// Extended Constructor.
	public Sprite(M3Platformer parent, String spriteName, float x, float y, float scale, float rotation)
	{
		this.parent = parent;
		this.spriteName = spriteName;
		
		loadImage(spriteName);

		this.position = new Vector2D(x, y);
		this.setScale(scale);
		this.rotation = rotation;
	}
	
	// Test Constructor.
	public Sprite(M3Platformer parent, String spriteName, float scale)
	{
		this(parent, spriteName, 0, 0, scale, 0);
	}
	
	protected void loadImage(String path) {
		setImg(ResourceManager.getImg(path));
		this.dimensions = new Vector2D(getImg().width, getImg().height);
	}
	
	// set rotation (Value in degree).
	public void setRotation(float rotation)
	{
		this.rotation = rotation;
	}
	
	// Return rotation (Value in degree).
	public double getRotation()
	{
		return this.rotation;
	}
	
	// set scalation of the sprite.
	// Also updates width and height of image.
	public void setScale(float scale)
	{
		// return to normal sizes
		if(this.scale != 0)
			this.dimensions.scale(1/this.scale);
		
		// scale according to new scale factor
		this.scale = scale;
		this.dimensions.scale(this.scale);
	}
	
	// Returns current scale.
	public float getScale()
	{
		return this.scale;
	}
	
	// set width of the sprite.
	public void setWidth(float width)
	{
		this.dimensions.setX(width);
	}
	
	// set height of the sprite.
	public void setHeight(float height)
	{
		this.dimensions.setY(height);
	}
	
	// Returns middle x position.
	private float getMiddleX(float x)
	{
		return x + this.getWidth() / 2;
	}
	
	// Returns middle y position.
	private float getMiddleY(float y)
	{
		return y + this.getHeight() / 2;
	}
	
	// draw the sprite.
	@Override
	protected void drawShape(int x, int y)
	{
		parent.pushMatrix();
		parent.translate(this.getMiddleX(getX() + x) , this.getMiddleY(getY() + y));
		parent.rotate((float)Math.toRadians(this.rotation));
		drawWithoutRotation((int)-(this.getWidth() / 2), (int)-(this.getHeight() / 2));
		parent.popMatrix();
	}
	
	// A faster drawing method. Lacks rotation functionality.
	public void drawWithoutRotation(int x, int y)
	{
		parent.tint(255, alpha);
		parent.image(getImg(), x, y, this.getWidth(), this.getHeight());
		parent.noTint();
	}
	
	
	// serialize object to XML in order to save to file
	public XML serialize() {
		XML res = new XML(this.getClass().getName());
		res.setString("name", this.spriteName);
		res.setFloat("x", this.getX());
		res.setFloat("y", this.getY());
		res.setFloat("rotation", this.rotation);
		res.setFloat("scale", this.scale);
		return res;
	}
	
	// deserialize object from XML
	public static Sprite deserialize(M3Platformer parent, XML xml) {
		return new Sprite(parent, M3Util.normalizePath(xml.getString("name")), xml.getFloat("x"), xml.getFloat("y"), xml.getFloat("scale"), xml.getFloat("rotation"));
	}

	public PImage getImg() {
		return img;
	}

	public void setImg(PImage img) {
		this.img = img;
	}

	public float getAlpha() {
		return alpha;
	}

	public void setAlpha(float alpha) {
		this.alpha = alpha;
	}
}
