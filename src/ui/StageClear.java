package ui;

import processing.core.PApplet;
import processing.core.PFont;
import m3platformer.GameLogic;
import m3platformer.GameMap;
import m3platformer.ResourceManager;
import m3platformer.ScoreMedal;
import m3platformer.Sprite;
import m3platformer.StageScoreSystem;
import m3platformer.animations.Blink;
import m3platformer.animations.RepeatedAnimation;

public class StageClear extends UIScreen {
	
	private Sprite box;
	private Sprite getNextStage;
	private Sprite gotNextStage;
	private Sprite nextStageActive;
	private Sprite nextStageInactive;
	private Sprite[] clocks = new Sprite[ScoreMedal.values().length];
	private Sprite[] coins  = new Sprite[ScoreMedal.values().length];
	
	private PFont numberFont;
	
	public StageClear(GameLogic l) {
		super(l);
		
		// load images
		box = new Sprite(parent, "data/imgs/stageClear/box.gif");
		getNextStage = new Sprite(parent, "data/imgs/stageClear/getNextStage.gif", 55, 140, 1, 0);
		gotNextStage = new Sprite(parent, "data/imgs/stageClear/gotNextStage.gif", 55, 140, 1, 0);
		nextStageActive = new Sprite(parent, "data/imgs/stageClear/nextStageButtonActive.gif", 163, 194, 1, 0);
		nextStageInactive = new Sprite(parent, "data/imgs/stageClear/nextStageButtonInactive.gif", 163, 194, 1, 0);
		
		for(ScoreMedal m : ScoreMedal.values()) {
			clocks[m.ordinal()] = new Sprite(parent, "data/imgs/icons/clock"+m.toString()+".gif");
			coins[m.ordinal()] = new Sprite(parent, "data/imgs/icons/coin"+m.toString()+".gif");
		}
		
		// load fonts
		numberFont   = ResourceManager.getFont("data/fonts/dice12Smooth.vlw");
		
		// draw stuff
		objects.add(box);
		
		// next stage button
		boolean nextStageReached = logic.getCurrentMap().getScoring().hasReachedNextStage();
		Sprite nextStageButton = nextStageReached ? nextStageActive : nextStageInactive;
		objects.add(nextStageButton);
		
		// message
		Sprite msg = nextStageReached ? gotNextStage : getNextStage;
		if(nextStageReached) {
			RepeatedAnimation r = new RepeatedAnimation();
			r.addAnimation(new Blink(2000, 1));
			msg.addAnimation(r);
		}
		objects.add(msg);
		
		// medals
		Sprite medal = clocks[logic.getCurrentMap().getScoring().getTimeMedal().ordinal()];
		medal.setPosition(246, 92);
		objects.add(medal);
		medal = coins[logic.getCurrentMap().getScoring().getCoinMedal().ordinal()];
		medal.setPosition(246, 115);
		objects.add(medal);
		
		drawUntil = objects.size();
	}
	
	@Override
	public void drawShape(int x, int y) {
		
		// load data
		GameMap map = logic.getCurrentMap();
		StageScoreSystem score = map.getScoring();
		
		// semi-transparent overlay
		parent.fill(0,0.6f*255);
		parent.stroke(0);
		parent.rect(0,0,parent.width,parent.height);
		
		// draw objects
		super.drawShape(x, y);
		
		// time limit
		parent.textFont(numberFont);
		parent.textAlign(PApplet.LEFT, PApplet.TOP);
		parent.textSize(12);
		parent.fill(245,43,22);
		parent.text(secondsToString(score.getStageTimeLimit()-logic.getElapsedStageTime()/1000), 207, 164);

		// last try
		parent.textAlign(PApplet.CENTER, PApplet.TOP);
		ScoreMedal cur = score.getTimeMedal(score.getLastTryTime());
		parent.fill(cur.getColor());
		parent.text(secondsToString(score.getLastTryTime()), 128, 93);
		
		cur = score.getCoinMedal(score.getLastTryCoins());
		parent.fill(cur.getColor());
		parent.text(score.getLastTryCoins(), 128, 116);
		
		// best try
		cur = score.getTimeMedal();
		parent.fill(cur.getColor());
		parent.text(secondsToString(score.getBestTime()), 187, 93);
		
		cur = score.getCoinMedal();
		parent.fill(cur.getColor());
		parent.text(score.getBestCoins(), 187, 116);
	}
}
