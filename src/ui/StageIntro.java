package ui;

import processing.core.PApplet;
import processing.core.PFont;
import m3platformer.GameLogic;
import m3platformer.GameMap;
import m3platformer.ResourceManager;
import m3platformer.ScoreMedal;
import m3platformer.Sprite;
import m3platformer.StageScoreSystem;

public class StageIntro extends UIScreen {
	
	private Sprite box;
	private Sprite stage;
	private Sprite[] digits = new Sprite[10];
	
	private PFont subtitleFont;
	private PFont numberFont;
	
	public StageIntro(GameLogic l) {
		super(l);
		
		// load images
		box = new Sprite(parent, "data/imgs/stageIntro/box.gif");
		stage = new Sprite(parent, "data/imgs/stageIntro/stage.gif");
		for(int i = 0;i<=9;i++) digits[i] = new Sprite(parent, "data/imgs/stageIntro/"+i+".gif");
		
		// load fonts
		subtitleFont = ResourceManager.getFont("data/fonts/8bitwonder10Smooth.vlw");
		numberFont   = ResourceManager.getFont("data/fonts/dice12Smooth.vlw");
		
		// draw stuff
		objects.add(box);
		
		// title
		int stageNum = logic.getCurrentStageNumber();
		int top = 28;
		int left = parent.getWidth()/2 - 2;
		int width = (int)stage.getWidth() + 6;
		Sprite digitLeft = digits[(stageNum > 9 ? (int)stageNum/10 : stageNum)];
		Sprite digitRight = (stageNum>9 ? digits[(stageNum%10)] : null);
		width += digitLeft.getWidth();
		if(digitRight != null) width += digitRight.getWidth();
		left -= width/2;

		stage.setPosition(left, top);
		digitLeft.setPosition(left+(int)stage.getWidth()+6, top);
		objects.add(stage);
		objects.add(digitLeft);
		if(digitRight != null) {
			digitRight.setPosition(left+6+(int)stage.getWidth()+(int)digitLeft.getWidth(), top);
			objects.add(digitRight);
		}
		
		drawUntil = 3;
	}
	
	@Override
	public void drawShape(int x, int y) {
		
		// load data
		GameMap map = logic.getCurrentMap();
		StageScoreSystem score = map.getScoring();
		
		// semi-transparent overlay
		parent.fill(0,0.6f*255);
		parent.stroke(0);
		parent.rect(0,0,parent.width,parent.height);
		
		// draw objects
		super.drawShape(x, y);
		
		// subtitle
		int top, left, width;
		if(map.getTitle() != null && !map.getTitle().equals("")) {
			top = 59+y;
			left = x+parent.getWidth()/2;
			parent.fill(111,195,223);
			parent.textSize(10);
			parent.textFont(subtitleFont);
			parent.textAlign(PApplet.CENTER, PApplet.TOP);
			parent.text(map.getTitle(), left, top);
			
			// dashes
			width = (int) parent.textWidth(map.getTitle());
			parent.textFont(numberFont);
			parent.textSize(12);
			parent.textAlign(PApplet.RIGHT, PApplet.TOP);
			parent.text("-", left-width/2 - 3, top);
			
			parent.textAlign(PApplet.LEFT, PApplet.TOP);
			parent.text("-", left+width/2 + 3, top);
		}
		
		// time limits
		parent.textAlign(PApplet.LEFT, PApplet.TOP);
		parent.textFont(numberFont);
		parent.textSize(12);
		parent.fill(245,43,22);
		parent.text(secondsToString(score.getStageTimeLimit()), x+109, y+108);
		
		parent.fill(221,217,206);
		parent.text(secondsToString(score.getSingleTryTimeLimit()), x+208, y+109);
		
		// objectives
		parent.fill(ScoreMedal.Gold.getColor());
		parent.text(secondsToString(score.getGoldTime()), x+110, y+160);
		parent.text(padCoinNum(score.getGoldCoins()), x+212, y+160);
		
		parent.fill(ScoreMedal.Silver.getColor());
		parent.text(secondsToString(score.getSilverTime()), x+110, y+176);
		parent.text(padCoinNum(score.getSilverCoins()), x+212, y+176);
		
		parent.fill(ScoreMedal.Bronze.getColor());
		parent.text(secondsToString(score.getBronzeTime()), x+110, y+192);
		parent.text(padCoinNum(score.getBronzeCoins()), x+212, y+192);
	}
	
	private static String padCoinNum(int coins) {
		if(coins<10) return " "+coins;
		else return ""+coins;
	}
}
