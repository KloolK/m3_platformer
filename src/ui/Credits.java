package ui;

import m3platformer.Command;
import m3platformer.GameLogic;
import m3platformer.M3Platformer;
import m3platformer.Sprite;
import m3platformer.Vector2D;
import m3platformer.animations.*;

/**hard coded credits cutscene
 */
public class Credits extends UIScreen {
	
	private Sprite alan;
	private Sprite alina;
	
	private Sprite congrats, youSaved, developedBy, music, partOf;
	
	private Sprite box, borderLeft, borderRight, borderTop, borderBottom, black;
	
	public Credits(GameLogic l) {
		super(l);
		
		// load sprites
		box = new Sprite(parent, "data/imgs/credits/box.gif", 0, 0, 1, 0);
		borderLeft = new Sprite(parent, "data/imgs/credits/borderLeft.gif", 0, 0, 1, 0);
		borderRight = new Sprite(parent, "data/imgs/credits/borderRight.gif", 310, 0, 1, 0);
		borderTop = new Sprite(parent, "data/imgs/credits/borderTop.gif", 0, -1, 1, 0);
		borderBottom = new Sprite(parent, "data/imgs/credits/borderBottom.gif", 0, 232, 1, 0);
		black = new Sprite(parent, "data/imgs/black.gif", 0, 0, 1, 0);
		
		alan = new Sprite(parent, "data/imgs/credits/alan.gif", 115, 311, 1, 0);
		alina = new Sprite(parent, "data/imgs/credits/alina.gif", 160, 305, 1, 0);
		youSaved = new Sprite(parent, "data/imgs/credits/youSaved.gif", 64, 391, 1, 0);
		
		developedBy = new Sprite(parent, "data/imgs/credits/developedBy.gif",63,330,1,0);
		music = new Sprite(parent, "data/imgs/credits/music.gif", 43, 263, 1, 0);
		partOf = new Sprite(parent, "data/imgs/credits/partOf.gif", 58, 271, 1, 0);

		congrats = new Sprite(parent, "data/imgs/credits/congrats.gif", 0, 0, 1, 0);
		
		drawUntil = 999;
		
		customStep(1);
	}
	
	private void customStep(int step) {
		
		int moveDur = 5000;
		int stayDur = 10000;
		
		switch(step) {
			case 1:
//				objects.add(congrats);
//				congrats.setAlpha(0);
//				congrats.addAnimation(new FadeImage(500, true).setFollowUp(new Delay(5000).setFollowUp(new FadeImage(500, false).setFollowUp(followUp))));
				
				objects.add(box);
				objects.add(youSaved);
				objects.add(alan);
				objects.add(alina);
				
				alan.addAnimation(new Move(moveDur, new Vector2D(0,-240), true).setFollowUp(new Delay(stayDur).setFollowUp(new Move(moveDur, new Vector2D(0,-240), true))));
				youSaved.addAnimation(new Move(moveDur, new Vector2D(0,-240), true).setFollowUp(new Delay(stayDur).setFollowUp(new Move(moveDur, new Vector2D(0,-240), true))));
				alina.addAnimation(new Move(moveDur, new Vector2D(0,-240), true).
						setFollowUp(new Delay((stayDur-1000)/2).
						setFollowUp(new Move(1000, new Vector2D(-25,0), true).
						setFollowUp(new Delay((stayDur-1000)/2-100). // TODO: WFT??
						setFollowUp(new Move(moveDur, new Vector2D(0,-240), true).
								setFinishCmd(new Command() {
									public void call(M3Platformer game) {
										game.getLogic().getCredits().customStep(2);
									}
								}))))));
			break;
			
			case 2:
				objects.remove(alina);
				objects.remove(alan);
				objects.remove(youSaved);
				
				objects.add(developedBy);
				developedBy.addAnimation(new Move(moveDur, new Vector2D(0,-240), true).setFollowUp(new Delay(stayDur).setFollowUp(new Move(moveDur, new Vector2D(0,-240), true).setFinishCmd(new Command() {
									public void call(M3Platformer game) {
										game.getLogic().getCredits().customStep(3);
									}
								}))));
			break;
			
			case 3:
				objects.remove(developedBy);
				objects.add(music);
				
				music.addAnimation(new Move(moveDur, new Vector2D(0,-240), true).setFollowUp(new Delay(stayDur).setFollowUp(new Move(moveDur, new Vector2D(0,-240), true).setFinishCmd(new Command() {
									public void call(M3Platformer game) {
										game.getLogic().getCredits().customStep(4);
									}
								}))));
			break;
			
			case 4:
				objects.remove(music);
				objects.add(partOf);
				objects.add(black);
				
				
				black.setAlpha(0);
				black.addAnimation(new Delay(moveDur+stayDur).setFollowUp(new FadeImage(moveDur, true).setFinishCmd(new Command() {
					public void call(M3Platformer game) {
						game.getLogic().getCredits().customStep(5);
					}
				})));
				
				partOf.addAnimation(new Move(moveDur, new Vector2D(0,-240), true));
			break;
			
			case 5:
				objects.remove(partOf);
				objects.remove(box);
				objects.remove(black);
				objects.add(congrats);
				
				congrats.setAlpha(0);
				
				congrats.addAnimation(new FadeImage(moveDur, true).
								setFollowUp(new Delay(20000).
								setFollowUp(new FadeImage(moveDur, false).
								setFinishCmd(new Command() {
									public void call(M3Platformer game) {
										game.getLogic().endCredits();
									}
								}))));
			break;
		}
	}
	
	@Override
	public void drawShape(int x, int y) {
		
		super.drawShape(x, y);
		
		if(objects.contains(box)) {
			borderLeft.draw(x-1, y);
			borderRight.draw(x, y);
			borderTop.draw(x, y);
			borderBottom.draw(x, y);
		}
		
		if(objects.contains(black)) {
			black.draw(x,y);
		}
	}
}
