package ui;

import m3platformer.AnimatedSprite;
import m3platformer.Command;
import m3platformer.GameLogic;
import m3platformer.M3Platformer;
import m3platformer.SoundManager;
import m3platformer.Sprite;
import m3platformer.Timeout;
import m3platformer.Vector2D;
import m3platformer.SoundManager.MusicType;
import m3platformer.animations.*;

/**hard coded story animation
 */
public class StoryIntro extends UIScreen {
	
	private Sprite alanIdle, alanLookingRight, alanSuit, alanSadRight, alanAngryRight;
	private Sprite alinaIdle, alinaAngry, alinaAngryRight;
	
	private Sprite text1, text2, text3, text4, text5, text6, text7;
	private Sprite text8, text9, text10, text11, text12, text13;
	private Sprite portalTitle, portalTitle2;
	
	private Sprite plus, equals;
	
	private Sprite box, borderLeft, borderRight, black;
	private Sprite defectPortal, workingPortal, redPortal;
	
	public StoryIntro(GameLogic l) {
		super(l);
		
		// load sprites
		box = new Sprite(parent, "data/imgs/storyIntro/box.gif", 0, 0, 1, 0);
		borderLeft = new Sprite(parent, "data/imgs/storyIntro/borderLeft.gif", 0, 0, 1, 0);
		borderRight = new Sprite(parent, "data/imgs/storyIntro/borderRight.gif", 310, 0, 1, 0);
		black = new Sprite(parent, "data/imgs/black.gif", 0, 0, 1, 0);
		
		plus = new Sprite(parent, "data/imgs/storyIntro/plus.gif", 84, 130, 1, 0);
		equals = new Sprite(parent, "data/imgs/storyIntro/equals.gif", 195, 130, 1, 0);
		defectPortal = new AnimatedSprite(parent, "data/imgs/portals/defect.gif", 113, 101, 2.06f, 0);
		workingPortal = new AnimatedSprite(parent, "data/imgs/portals/blueAnim.gif", 221, 101, 2.06f, 0);
		redPortal = new AnimatedSprite(parent, "data/imgs/portals/red.gif", 221, 101, 2.06f, 0);
		text1 = new Sprite(parent, "data/imgs/storyIntro/text1.gif", 76, 74, 1, 0);
		text2 = new Sprite(parent, "data/imgs/storyIntro/text2.gif", 53, 74, 1, 0);
		text3 = new Sprite(parent, "data/imgs/storyIntro/text3.gif", 78, 74, 1, 0);
		text4 = new Sprite(parent, "data/imgs/storyIntro/text4.gif", 59, 80, 1, 0);
		text5 = new Sprite(parent, "data/imgs/storyIntro/text5.gif", 37, 80, 1, 0);
		text6 = new Sprite(parent, "data/imgs/storyIntro/text6.gif", 33, 80, 1, 0);
		text7 = new Sprite(parent, "data/imgs/storyIntro/text7.gif", 55, 80, 1, 0);
		text8 = new Sprite(parent, "data/imgs/storyIntro/text8.gif", -80, 90, 1, 0);
		text9 = new Sprite(parent, "data/imgs/storyIntro/text9.gif", 95, 23, 1, 0);
		text10 = new Sprite(parent, "data/imgs/storyIntro/text10.gif", 69, 48, 1, 0);
		text11 = new Sprite(parent, "data/imgs/storyIntro/text11.gif", 76, 62, 1, 0);
		text12 = new Sprite(parent, "data/imgs/storyIntro/text12.gif", 56, 47, 1, 0);
		text13 = new Sprite(parent, "data/imgs/storyIntro/text13.gif", 36, 207, 1, 0);
		
		portalTitle = new Sprite(parent, "data/imgs/storyIntro/portalTitle.gif", 326, 85, 1, 0);
		portalTitle2 = new Sprite(parent, "data/imgs/storyIntro/portalTitle2.gif", 195, 85, 1, 0);
		
		alanIdle = new AnimatedSprite(parent, "data/imgs/storyIntro/alanIdle.gif", 144, 103, 1, 0);
		alanLookingRight = new AnimatedSprite(parent, "data/imgs/storyIntro/alanLookingRight.gif", 0, 0, 1, 0);
		alanSuit = new Sprite(parent, "data/imgs/storyIntro/alanSuit.gif", 0, 0, 1, 0);
		alanSadRight = new Sprite(parent, "data/imgs/storyIntro/alanSadRight.gif", -80, 114, 1, 0);
		alanAngryRight = new Sprite(parent, "data/imgs/storyIntro/alanAngryRight.gif", 0, 0, 1, 0);
		alinaIdle = new AnimatedSprite(parent, "data/imgs/storyIntro/alinaIdle.gif", 164, 109, 1, 0);
		alinaAngry = new Sprite(parent, "data/imgs/storyIntro/alinaAngry.gif", 164, 109, 1, 0);
		alinaAngryRight = new Sprite(parent, "data/imgs/storyIntro/alinaAngryRight.gif", 164, 109, 1, 0);
				
		drawUntil = 999;
		
		customStep(1);
	}
	
	private void customStep(int step) {
		
		switch(step) {
			case 1:
				objects.add(text1);
				alanIdle.setAlpha(0);
				alanIdle.addAnimation(new Delay(500).setFollowUp(new FadeImage(500, true).setFollowUp(new Delay(2000).setFinishCmd(new Command() {
					public void call(M3Platformer game) {
						game.getLogic().getStoryIntro().customStep(2);
					}
				}))));
				objects.add(alanIdle);
				break;
			case 2:
				alanIdle.clearAnimations();
				alanIdle.addAnimation(new Move(1000, new Vector2D(-113,1), true));
				text1.addAnimation(new Move(1000, new Vector2D(0,-19), true).setFollowUp(new Delay(500).setFinishCmd(new Command() {
					public void call(M3Platformer game) {
						game.getLogic().getStoryIntro().customStep(3);
					}
				})));
			break;
			
			case 3:
				objects.add(alanSuit);
				objects.add(text2);
				
				// dress up for work
				alanSuit.setPosition(alanIdle.getX(), alanIdle.getY());
				alanSuit.setAlpha(0);
				alanSuit.addAnimation(new FadeImage(500, true).setFollowUp(new Delay(200).setFinishCmd(new Command() {
					public void call(M3Platformer game) {
						game.getLogic().getStoryIntro().customStep(4);
					}
				})));
				text2.setAlpha(0);
				text2.addAnimation(new FadeImage(500, true));
			break;
			
			case 4:
				objects.add(plus);
				objects.add(equals);
				objects.add(defectPortal);
				objects.add(workingPortal);
				
				// slowly fade in and out equation
				plus.setAlpha(0);
				equals.setAlpha(0);
				defectPortal.setAlpha(0);
				workingPortal.setAlpha(0);
				plus.addAnimation(new FadeImage(400, true).setFollowUp(new Delay(6200).setFollowUp(new FadeImage(400, false))));
				defectPortal.addAnimation(new Delay(400).setFollowUp(new FadeImage(400, true).setFollowUp(new Delay(5400).setFollowUp(new FadeImage(400, false)))));
				equals.addAnimation(new Delay(1500).setFollowUp(new FadeImage(400, true).setFollowUp(new Delay(3900).setFollowUp(new FadeImage(400, false)))));
				workingPortal.addAnimation(new Delay(2000).setFollowUp(new FadeImage(400, true).setFollowUp(new Delay(3000).setFollowUp(new FadeImage(400, false)))));
				
				// get out of those clothes
				alanSuit.addAnimation(new Delay(7000).setFollowUp(new FadeImage(400, false).setFinishCmd(new Command() {
					public void call(M3Platformer game) {
						game.getLogic().getStoryIntro().customStep(5);
					}
				})));
			break;
			
			case 5:
				objects.remove(alanSuit);
				objects.remove(plus);
				objects.remove(equals);
				objects.remove(defectPortal);
				objects.remove(workingPortal);
				
				objects.add(text3);
				objects.add(alinaIdle);
				
				// fade out first text
				text1.addAnimation(new Move(1000, new Vector2D(0,-34), true));
				text1.addAnimation(new FadeImage(1000, false));
				
				text2.addAnimation(new Move(1000, new Vector2D(0,-19), true));
				
				text3.setAlpha(0);
				text3.addAnimation(new Delay(1000).setFollowUp(new FadeImage(400, true)));
				
				// fade in alina
				alinaIdle.setAlpha(0);
				alinaIdle.addAnimation(new Delay(2000).setFollowUp(new FadeImage(400, true)));
				
				// move alan to alina
				alanIdle.addAnimation(new Delay(3000).setFollowUp(new Move(1000, new Vector2D(85,13), true).setFollowUp(new Delay(1500).setFinishCmd(new Command() {
					public void call(M3Platformer game) {
						game.getLogic().getStoryIntro().customStep(6);
					}
				}))));
			break;
			
			case 6:
				objects.add(alanLookingRight);
				objects.remove(alanIdle);
				objects.add(text4);
				
				// alan likes alina....
				alanLookingRight.setPosition(alanIdle.getX(), alanIdle.getY());
				
				text2.addAnimation(new Delay(2000).setFollowUp(new Move(1000, new Vector2D(0,-34), true)));
				text2.addAnimation(new Delay(2000).setFollowUp(new FadeImage(1000, false)));
				
				text3.addAnimation(new Delay(2000).setFollowUp(new Move(1000, new Vector2D(0,-25), true)));
				
				text4.setAlpha(0);
				text4.addAnimation(new Delay(3500).setFollowUp(new FadeImage(400, true).setFollowUp(new Delay(2000).setFinishCmd(new Command() {
					public void call(M3Platformer game) {
						game.getLogic().getStoryIntro().customStep(7);
					}
				}))));
			break;
			
			case 7:
				objects.remove(text2);
				objects.add(alanSuit);
				objects.add(text5);
				objects.add(text6);
				objects.add(alinaAngry);
				
				text3.addAnimation(new Move(1000, new Vector2D(0,-34), true));
				text3.addAnimation(new FadeImage(1000, false));
				
				text4.addAnimation(new Move(1000, new Vector2D(0,-19), true).setFollowUp(new Delay(2200).setFollowUp(new Move(1000, new Vector2D(0,-19), true))));
				
				text5.setAlpha(0);
				text5.addAnimation(new Delay(1500).setFollowUp(new FadeImage(400, true).setFollowUp(new Delay(1300).setFollowUp(new Move(1000, new Vector2D(0,-19), true)))));
				
				// get the suit on again
				alanSuit.setPosition(alanIdle.getX(), alanIdle.getY());
				alanSuit.setAlpha(0);
				alanSuit.addAnimation(new Delay(1500).setFollowUp(new FadeImage(400, true)));
				
				text6.setAlpha(0);
				text6.addAnimation(new Delay(4500).setFollowUp(new FadeImage(400, true)));
				
				logic.registerTimeout(new Timeout(M3Platformer.getInstance(), 5000, new Command() {
					public void call(M3Platformer game) {
						SoundManager.playMusic(SoundManager.MusicType.BADMUSIC);
					}
				}));
				
				alinaAngry.setAlpha(0);
				alinaAngry.addAnimation(new Delay(5000).setFollowUp(new FadeImage(10, true).setFollowUp(new Delay(1500).setFinishCmd(new Command() {
					public void call(M3Platformer game) {
						game.getLogic().getStoryIntro().customStep(8);
					}
				}))));
			break;
			
			case 8:
				objects.remove(text3);
				objects.add(text7);
				objects.remove(alinaIdle);
				objects.remove(alanLookingRight);
				
				text4.addAnimation(new Move(1000, new Vector2D(0,-34), true));
				text4.addAnimation(new FadeImage(1000, false));
				
				text5.addAnimation(new Move(1000, new Vector2D(0,-36), true));
				text6.addAnimation(new Move(1000, new Vector2D(0,-36), true));
				
				text7.setAlpha(0);
				text7.addAnimation(new Delay(1500).setFollowUp(new FadeImage(400, true).setFollowUp(new Delay(2500).setFinishCmd(new Command() {
					public void call(M3Platformer game) {
						game.getLogic().getStoryIntro().customStep(9);
					}
				}))));
			break;
			
			case 9:
				objects.remove(text4);
				objects.remove(alinaAngry);
				objects.add(alinaAngryRight);
				
				// alina walks away
				alinaAngryRight.addAnimation(new Move(800, new Vector2D(60,0), true).setFollowUp(new Delay(4000).setFollowUp(
						new Move(2400, new Vector2D(-180,0), true).setFinishCmd(new Command() {
							public void call(M3Platformer game) {
								game.getLogic().getStoryIntro().customStep(10);
							}
						}))));
				
				// stuff walks out of screen
				text5.addAnimation(new Delay(800).setFollowUp(new Move(4000, new Vector2D(-300,0), true)));
				text6.addAnimation(new Delay(800).setFollowUp(new Move(4000, new Vector2D(-300,0), true)));
				text7.addAnimation(new Delay(800).setFollowUp(new Move(4000, new Vector2D(-300,0), true)));
				alanSuit.addAnimation(new Delay(800).setFollowUp(new Move(4000, new Vector2D(-300,0), true)));
				
			break;
			
			case 10:
				objects.remove(text5);
				objects.remove(text6);
				objects.remove(text7);
				objects.remove(alanSuit);
				objects.add(workingPortal);
				objects.add(portalTitle);
				
				// get alina in front of everything else
				objects.remove(alinaAngryRight);
				objects.add(alinaAngryRight);
				
				// portal approching
				workingPortal.setAlpha(255);
				workingPortal.setPosition(350, 115);
				workingPortal.addAnimation(new Move(2400, new Vector2D(-150,0), true));
				portalTitle.addAnimation(new Move(2400, new Vector2D(-150,0), true));
				
				alinaAngryRight.addAnimation(new Delay(2400).setFollowUp(new Move(1800, new Vector2D(167,5), true).setFinishCmd(new Command() {
					public void call(M3Platformer game) {
						game.getLogic().getStoryIntro().customStep(11);
					}
				})));
			break;
			
			case 11:
				objects.add(redPortal);
				objects.remove(alinaAngryRight);
				objects.add(alinaAngryRight);
				objects.add(portalTitle2);
				
				// portals shaking around
				Animation shaking = new RepeatedAnimation().
									addAnimation(new Move(50, new Vector2D(-3,0), true)).
									addAnimation(new Move(50, new Vector2D(6,0), true)).
									addAnimation(new Move(50, new Vector2D(-3,0), true));
				
				redPortal.addAnimation(shaking);
				workingPortal.addAnimation(shaking);
				portalTitle2.setAlpha(0);
				portalTitle.addAnimation(new FadeImage(500, false));
				portalTitle2.addAnimation(new Delay(500).setFollowUp(new FadeImage(500, true)));
				
				// stop working, start being broken
				((AnimatedSprite)workingPortal).setPause(true).jump(0);
				
				redPortal.setPosition(200, 115);
				redPortal.setAlpha(255);
				redPortal.addAnimation(    new RepeatedAnimation().addAnimation(new FadeImage(10,true )).addAnimation(new Delay(100)).addAnimation(new FadeImage(10,false)).addAnimation(new Delay(500)));
				workingPortal.addAnimation(new RepeatedAnimation().addAnimation(new FadeImage(10,false)).addAnimation(new Delay(100)).addAnimation(new FadeImage(10,true )).addAnimation(new Delay(500)));
				
				// zooooooooom.......
				SoundManager.playSound(SoundManager.SoundType.ERROR);
				alinaAngryRight.addAnimation(new RepeatedAnimation().addAnimation(new Spin(500,2)));
				alinaAngryRight.addAnimation(new Scale(2000, 1, 0).setFinishCmd(new Command() {
					public void call(M3Platformer game) {
						game.getLogic().getStoryIntro().customStep(12);
					}
				}));
			break;
			
			case 12:
				objects.remove(alinaAngryRight);
				objects.remove(portalTitle);
				objects.add(text8);
				objects.add(alanSadRight);
				objects.add(text9);
				objects.add(text10);
				
				// alina...?
				text8.addAnimation(new Delay(2500).setFollowUp(new Move(1500, new Vector2D(130,0),true).setFollowUp(new Delay(3500).setFollowUp(new FadeImage(500,false)))));
				alanSadRight.addAnimation(new Delay(4500).setFollowUp(new Move(1500, new Vector2D(145,0),true)));
				
				// ohh nooo!
				text9.setAlpha(0);
				text9.addAnimation(new Delay(7000).setFollowUp(new FadeImage(500, true)));
				
				// screw problems
				text10.setAlpha(0);
				text10.addAnimation(new Delay(10000).setFollowUp(new FadeImage(500, true).setFollowUp(new Delay(1500).setFinishCmd(new Command() {
					public void call(M3Platformer game) {
						game.getLogic().getStoryIntro().customStep(13);
					}
				}))));
			break;
			
			case 13:
				objects.remove(text8);
				objects.add(text11);
				
				text9.addAnimation(new Move(1000, new Vector2D(0,-34), true));
				text9.addAnimation(new FadeImage(1000, false));
				
				text10.addAnimation(new Move(1000, new Vector2D(0,-19), true));
				
				// where did she go?
				text11.setAlpha(0);
				text11.addAnimation(new Delay(1000).setFollowUp(new FadeImage(500, true).setFollowUp(new Delay(2000).setFinishCmd(new Command() {
					public void call(M3Platformer game) {
						game.getLogic().getStoryIntro().customStep(14);
					}
				}))));
			break;
			
			case 14:
				objects.remove(text9);
				objects.add(text12);
				objects.add(text13);
				objects.add(alanAngryRight);
				
				text10.addAnimation(new Move(1000, new Vector2D(0,-34), true));
				text10.addAnimation(new FadeImage(1000, false));
				
				text11.addAnimation(new Move(1000, new Vector2D(0,-34), true));
				
				// only one way
				text12.setAlpha(0);
				text12.addAnimation(new Delay(1000).setFollowUp(new FadeImage(500, true)));
				
				// the journey begins
				text13.setAlpha(0);
				text13.addAnimation(new Delay(5500).setFollowUp(new FadeImage(500, true)));
				
				// oh alan...
				alanSadRight.addAnimation(new Delay(3600).setFollowUp(new FadeImage(10, false)));
				alanAngryRight.setAlpha(0);
				alanAngryRight.setPosition(alanSadRight.getX(), alanSadRight.getY());
				alanAngryRight.addAnimation(
						new Delay(3500).setFollowUp(
								new FadeImage(10, true).setFollowUp(
										new Delay(3500).setFollowUp(
												new Move(1000, new Vector2D(155,3), true).setFinishCmd(
														new Command() {
															public void call(M3Platformer game) {
																game.getLogic().getStoryIntro().customStep(15);
															}
				})))));
			break;
			
			case 15:
				objects.remove(alanSadRight);
				objects.remove(text10);
								
				// zooooooooom.......
				SoundManager.playSound(SoundManager.SoundType.ERROR);
				alanAngryRight.addAnimation(new RepeatedAnimation().addAnimation(new Spin(500,2)));
				alanAngryRight.addAnimation(new Scale(2000, 1, 0).setFinishCmd(new Command() {
					public void call(M3Platformer game) {
						game.getLogic().getStoryIntro().customStep(16);
					}
				}));
			break;
			
			case 16:
				objects.remove(alanAngryRight);
				objects.add(black);
				
				black.setAlpha(0);
				black.addAnimation(new FadeImage(5000, true).setFollowUp(new Delay(1500).setFinishCmd(new Command() {
					public void call(M3Platformer game) {
						game.getLogic().startNewGame();
					}
				})));
			break;
		}
	}
	
	@Override
	public void drawShape(int x, int y) {
		box.draw(x, y);
		
		super.drawShape(x, y);
		
		borderLeft.draw(x-1, y);
		borderRight.draw(x, y);
		
		if(objects.contains(black)) {
			black.draw(x,y);
		}
	}
}
