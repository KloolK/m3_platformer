package ui;

import processing.core.PApplet;
import processing.core.PFont;
import m3platformer.GameLogic;
import m3platformer.GameMap;
import m3platformer.ResourceManager;
import m3platformer.ScoreMedal;
import m3platformer.Sprite;
import m3platformer.StageScoreSystem;

public class PauseMenu extends UIScreen {
	
	private Sprite box;
	private Sprite nextStageActive, nextStageInactive;
	private Sprite[] clocks = new Sprite[ScoreMedal.values().length];
	private Sprite[] coins  = new Sprite[ScoreMedal.values().length];
	
	private PFont subtitleFont;
	private PFont numberFont;
	
	public PauseMenu(GameLogic l) {
		super(l);
		
		// load images
		box = new Sprite(parent, "data/imgs/pause/box.gif");
		nextStageActive = new Sprite(parent, "data/imgs/pause/nextStageActive.gif", 62, 196, 1, 0);
		nextStageInactive = new Sprite(parent, "data/imgs/pause/nextStageInactive.gif", 62, 196, 1, 0);
		
		for(ScoreMedal m : ScoreMedal.values()) {
			clocks[m.ordinal()] = new Sprite(parent, "data/imgs/icons/clock"+m.toString()+".gif", 160, 94, 1, 0);
			coins[m.ordinal()] = new Sprite(parent, "data/imgs/icons/coin"+m.toString()+".gif", 233, 94, 1, 0);
		}
		
		// load fonts
		subtitleFont = ResourceManager.getFont("data/fonts/8bitwonder10Smooth.vlw");
		numberFont   = ResourceManager.getFont("data/fonts/dice12Smooth.vlw");
		
		// draw stuff
		objects.add(box);
		boolean nextStageReached = logic.getCurrentMap().getScoring().hasReachedNextStage();
		Sprite nextStageButton = nextStageReached ? nextStageActive : nextStageInactive;
		objects.add(nextStageButton);
		
		objects.add(clocks[logic.getCurrentMap().getScoring().getTimeMedal().ordinal()]);
		objects.add(coins[logic.getCurrentMap().getScoring().getCoinMedal().ordinal()]);
		
		drawUntil = 999;
	}
	
	@Override
	public void drawShape(int x, int y) {
		
		// load data
		GameMap map = logic.getCurrentMap();
		StageScoreSystem score = map.getScoring();
		
		// semi-transparent overlay
		parent.fill(0,0.6f*255);
		parent.stroke(0);
		parent.rect(0,0,parent.width,parent.height);
		
		// draw objects
		super.drawShape(x, y);
		
		// subtitle
		int top, left, width;
		if(map.getTitle() != null && !map.getTitle().equals("")) {
			top = 59+y;
			left = x+parent.getWidth()/2;
			parent.fill(111,195,223);
			parent.textSize(10);
			parent.textFont(subtitleFont);
			parent.textAlign(PApplet.CENTER, PApplet.TOP);
			parent.text(map.getTitle(), left, top);
			
			// dashes
			width = (int) parent.textWidth(map.getTitle());
			parent.textFont(numberFont);
			parent.textSize(12);
			parent.textAlign(PApplet.RIGHT, PApplet.TOP);
			parent.text("-", left-width/2 - 3, top);
			
			parent.textAlign(PApplet.LEFT, PApplet.TOP);
			parent.text("-", left+width/2 + 3, top);
		}
		
		// best try
		parent.textAlign(PApplet.LEFT, PApplet.TOP);
		parent.textFont(numberFont);
		parent.textSize(12);
		parent.fill(score.getTimeMedal().getColor());
		int bestTime = score.getBestTime() < score.getSingleTryTimeLimit() ? score.getBestTime() : -1;
		parent.text(secondsToString(bestTime), x+175, y+95);

		parent.fill(score.getCoinMedal().getColor());
		parent.text(padCoinNum(score.getBestCoins()), x+249, y+95);
		
		// objectives
		parent.fill(ScoreMedal.Gold.getColor());
		parent.text(secondsToString(score.getGoldTime()), x+175, y+135);
		parent.text(padCoinNum(score.getGoldCoins()), x+249, y+135);
		
		parent.fill(ScoreMedal.Silver.getColor());
		parent.text(secondsToString(score.getSilverTime()), x+175, y+151);
		parent.text(padCoinNum(score.getSilverCoins()), x+249, y+151);
		
		parent.fill(ScoreMedal.Bronze.getColor());
		parent.text(secondsToString(score.getBronzeTime()), x+175, y+167);
		parent.text(padCoinNum(score.getBronzeCoins()), x+249, y+167);
	}
	
	private static String padCoinNum(int coins) {
		if(coins<10) return " "+coins;
		else return ""+coins;
	}
}
