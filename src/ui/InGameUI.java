package ui;

import objects.Drawable;
import processing.core.PApplet;
import processing.core.PFont;

import m3platformer.GameLogic;
import m3platformer.GameMap;
import m3platformer.M3Platformer;
import m3platformer.ResourceManager;
import m3platformer.ScoreMedal;
import m3platformer.Sprite;
import m3platformer.StageScoreSystem;
import m3platformer.Vector2D;

public class InGameUI extends Drawable {
	
	private M3Platformer parent;
	private GameLogic logic;
	
	// font
	private PFont timeFont;
	private PFont textFont;
	
	// images
	private Sprite background;
	private Sprite[] clocks = new Sprite[ScoreMedal.values().length];
	private Sprite[] coins  = new Sprite[ScoreMedal.values().length];
	
	public InGameUI(GameLogic l) {
		parent = M3Platformer.getInstance();
		logic = l;
		position = new Vector2D(0,0);
		timeFont = ResourceManager.getFont("data/fonts/dice12Smooth.vlw");
		textFont = ResourceManager.getFont("data/fonts/8bitwonder11Smooth.vlw");
		
		// load images
		background = new Sprite(parent, "data/imgs/ui.gif");
		for(ScoreMedal m : ScoreMedal.values()) {
			clocks[m.ordinal()] = new Sprite(parent, "data/imgs/icons/clock"+m.toString()+".gif");
			coins[m.ordinal()] = new Sprite(parent, "data/imgs/icons/coin"+m.toString()+".gif");
		}
		
		dimensions = new Vector2D(background.getWidth(), background.getHeight());
	}

	@Override
	protected void drawShape(int x, int y) {
		
		x += this.getX();
		y += this.getY();
		
		GameMap map = logic.getCurrentMap();
		StageScoreSystem scoring = map.getScoring();
		int elapsedTryTime = logic.getElapsedTryTime()/1000;
		int elapsedStageTime = logic.getElapsedStageTime()/1000;
		
		ScoreMedal timeMedal = scoring.getTimeMedal(elapsedTryTime);
		ScoreMedal coinMedal = scoring.getCoinMedal(logic.getPlayer().getCoinCount());
		
		int tryTimeLeft = scoring.getSingleTryTimeLimit() - elapsedTryTime;
		int stageTimeLeft = scoring.getStageTimeLimit() - elapsedStageTime;
		
		background.draw(x, y);
		
		// text options
		this.parent.strokeWeight(1);
		this.parent.textFont(timeFont);
		this.parent.textSize(12);
		this.parent.textAlign(PApplet.LEFT, PApplet.TOP);
		
		// time
		this.parent.fill(timeMedal.getColor());
		clocks[timeMedal.ordinal()].draw((int) (x + dimensions.getX() - 51), y + 10);
		this.parent.text(UIScreen.secondsToString(tryTimeLeft), x + dimensions.getX() - 36, y+11);
		
		// coins
		coins[coinMedal.ordinal()].draw((int) (x + dimensions.getX() - 116), y + 10);
		this.parent.fill(coinMedal.getColor());
		this.parent.textAlign(PApplet.RIGHT, PApplet.TOP);
		this.parent.text(logic.getPlayer().getCoinCount(), x + dimensions.getX() - 88, y+11);
		this.parent.stroke(coinMedal.getColor().getRed(), coinMedal.getColor().getGreen(), coinMedal.getColor().getBlue());
		this.parent.line(x + dimensions.getX() - 88, y+18, x + dimensions.getX() - 86, y+11); // "slash" (dirty solution, but font does not contain "/")
		this.parent.textAlign(PApplet.LEFT, PApplet.TOP);
		this.parent.text(logic.getCurrentMap().getCoinCount(), x + dimensions.getX() - 83, y+11);
		
		
		// time till next stage
		this.parent.fill(245,43,22);
		this.parent.text(UIScreen.secondsToString(stageTimeLeft), x+167, y+11);
		
		// stage x
		parent.textFont(textFont);
		this.parent.fill(111,195,223);
		this.parent.textSize(11);
		
		this.parent.text("STAGE "+logic.getCurrentStageNumber(), x + 6, y + 10);
	}
}
