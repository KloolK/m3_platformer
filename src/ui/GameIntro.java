package ui;

import m3platformer.Command;
import m3platformer.GameLogic;
import m3platformer.M3Platformer;
import m3platformer.Sprite;
import m3platformer.Vector2D;
import m3platformer.animations.*;

/**hard coded game intro cutscene
 */
public class GameIntro extends UIScreen {
	
	protected Command nextStepCmd = new Command() {
		public void call(M3Platformer game) {
			game.getLogic().getGameIntro().nextStep(false);
		}
	};
	
	protected Command nextStepClearCmd = new Command() {
		public void call(M3Platformer game) {
			game.getLogic().getGameIntro().nextStep(true);
		}
	};
	
	public GameIntro(GameLogic l) {
		super(l);
		
		// hard coded intro
		Sprite logo = new Sprite(parent, "data/imgs/developedBy.gif");
		Sprite alanOnly = new Sprite(parent, "data/imgs/logo/alanOnly.gif", -320, 0, 1, 0);
		Sprite alinaOnly = new Sprite(parent, "data/imgs/logo/alinaOnly.gif", 320, 0, 1, 0);
		Sprite andOnly = new Sprite(parent, "data/imgs/logo/and.gif", 0, -320, 1, 0);
		Sprite insertCoin = new Sprite(parent, "data/imgs/logo/insertCoin.gif", 0, 150, 1, 0);
		
		// add objects and corresponding animations
		Animation a,b,c;
		
		this.objects.add(logo);
		a = new FadeImage(5000, true);
		b = new Delay(2000);
		c = new FadeImage(5000, false);
		a.setFollowUp(b);
		b.setFollowUp(c);
		c.setFinishCmd(nextStepClearCmd);
		logo.addAnimation(a);
		
		this.objects.add(alanOnly);
		a = new Move(2000, new Vector2D(320,0), true);
		a.setFinishCmd(nextStepCmd);
		alanOnly.addAnimation(a);
		
		this.objects.add(andOnly);
		a = new Move(500, new Vector2D(0,320), true);
		a.setFinishCmd(nextStepCmd);
		andOnly.addAnimation(a);
		
		this.objects.add(alinaOnly);
		a = new Move(2000, new Vector2D(-320,0), true);
		a.setFinishCmd(nextStepCmd);
		alinaOnly.addAnimation(a);

		this.objects.add(insertCoin);
		b = new Move(1000, new Vector2D(0,-150), true);
		RepeatedAnimation r = new RepeatedAnimation();
		a = new Blink(2000, 1);
		r.addAnimation(a);
		b.setFollowUp(r);
		insertCoin.addAnimation(b);
		
		// prepare first step
		nextStep(false);
	}
	
	@Override
	public void drawShape(int x, int y) {
		parent.background(0);
		
		super.drawShape(x, y);
	}
}
