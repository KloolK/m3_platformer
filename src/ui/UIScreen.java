package ui;

import java.util.LinkedList;
import java.util.List;

import m3platformer.GameLogic;
import m3platformer.M3Platformer;
import m3platformer.Vector2D;

import objects.Drawable;


/**On screen user interface logic
 *
 */
public abstract class UIScreen extends Drawable {

	protected M3Platformer parent;
	protected GameLogic logic;
	protected List<Drawable> objects = new LinkedList<Drawable>();
	protected int drawUntil = -1;
	protected int drawOffset = 0;

	public UIScreen(GameLogic l) {
		parent = M3Platformer.getInstance();
		logic = l;
		
		dimensions = new Vector2D(parent.width,parent.height);
		position = new Vector2D(0,0);
	}
	
	public void nextStep(boolean removeLast) {nextStep((removeLast ? 1 : 0), 1);}
	
	public void nextStep(int startAdd, int endAdd) {
		drawOffset+=startAdd;
		
		// draw next object(s)
		if(this.objects.size() > drawUntil+endAdd) {
			for(int i=1;i<=endAdd;i++) {
				drawUntil++;
				this.objects.get(drawUntil).restartFirstAnimation();
			}
		}
	}
	
	public static String secondsToString(int secs) {
		if(secs < 0) return "-";
		
		return (int)(secs/60) + ":" + (secs%60<10 ? "0" + (secs%60) : secs%60);
	}
	
	public static String padNumZero(int num) {
		if(num < 10) return "00"+num;
		else if(num < 100) return "0"+num;
		else return num+"";
	}
	
	public static String numberFormat(int num) {
		String res = "";
		if(num >= 1000000) res += (num/1000000) + ".";
		if(num >= 1000)	  
			if(!res.equals("")) res += padNumZero(num%1000000 / 1000) + ".";
			else 				res += (num%1000000 / 1000) + ".";
		
		if(!res.equals("")) res += padNumZero(num%1000);
		else res = num%1000 + "";
		
		return res;
	}

	@Override
	public void drawShape(int x, int y) {
		
		int i = 0;
		for(Drawable d : new LinkedList<Drawable>(objects)) {
			
			// do not draw objects with index < drawOffset
			if(i < drawOffset) {
				i++;
				continue;
			}
			
			// do not draw objects with index > drawUntil
			if(i > drawUntil) break;
			
			d.draw(x,y);
			i++;
		}
	}

}