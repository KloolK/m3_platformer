package ui;

import java.awt.Color;

import processing.core.PApplet;
import processing.core.PFont;
import m3platformer.Command;
import m3platformer.GameLogic;
import m3platformer.M3Platformer;
import m3platformer.ResourceManager;
import m3platformer.Sprite;
import m3platformer.TextSprite;
import m3platformer.animations.Delay;
import m3platformer.animations.FadeImage;
import m3platformer.animations.FadeTextSprite;

/**hard coded game over cutscene
 */
public class GameOver extends UIScreen {
	
	private Sprite box, black;
	private Sprite text1, text2, text3, text4;
	private Sprite alinaSad;
	private TextSprite[] countDown = new TextSprite[10];
	
	private PFont numberFont;
	
	protected Command nextStepCmd = new Command() {
		public void call(M3Platformer game) {
			game.getLogic().getGameOver().nextStep(false);
		}
	};
	
	public GameOver(GameLogic l) {
		super(l);
		
		// load images
		box = new Sprite(parent, "data/imgs/gameOver/box.gif");
		black = new Sprite(parent, "data/imgs/black.gif",0,0,1,0);
		text1 = new Sprite(parent, "data/imgs/gameOver/text1.gif", 74, 64, 1, 0);
		text2 = new Sprite(parent, "data/imgs/gameOver/text2.gif", 39, 81, 1, 0);
		text3 = new Sprite(parent, "data/imgs/gameOver/text3.gif", 64, 190, 1, 0);
		text4 = new Sprite(parent, "data/imgs/gameOver/text4.gif", 65, 212, 1, 0);
		
		alinaSad = new Sprite(parent, "data/imgs/gameOver/alinaSad.gif", 135, 108, 1, 0);
		
		// load fonts
		numberFont = ResourceManager.getFont("data/fonts/dice60Smooth.vlw");
		
		// countdown
		for(int i=0;i<=9;i++) {
			countDown[i] = new TextSprite(i+"",numberFont,new Color(111,195,223), 60);
			countDown[i].setPosition(245,122);
			countDown[i].setAlign(PApplet.CENTER, PApplet.TOP);
		}
		
		// draw stuff
		objects.add(box);
		text1.addAnimation(new FadeImage(400, true).setFollowUp(new Delay(1500).setFinishCmd(nextStepCmd)));
		text2.addAnimation(new FadeImage(400, true).setFollowUp(new Delay(1500).setFinishCmd(nextStepCmd)));
		alinaSad.addAnimation(new FadeImage(400, true).setFollowUp(new Delay(2500).setFinishCmd(nextStepCmd).
				setFollowUp(new Delay(22800).setFollowUp(new FadeImage(3000,false)))));
		text3.addAnimation(new FadeImage(400, true).setFollowUp(new Delay(1500).setFinishCmd(nextStepCmd)));
		text4.addAnimation(new FadeImage(400, true).setFollowUp(new Delay(1500).setFinishCmd(nextStepCmd)));
		
		black.setAlpha(0);
		black.addAnimation(new Delay(5000).setFollowUp(new FadeImage(3000, true).setFinishCmd(new Command() {
							public void call(M3Platformer game) {
								game.getLogic().endGameOver();
							}
						})));
		
		objects.add(text1);
		objects.add(text2);
		objects.add(alinaSad);
		objects.add(text3);
		objects.add(text4);
		
		for(int i=9;i>=0;i--) {
			countDown[i].addAnimation(new FadeTextSprite(100, true).setFollowUp(new Delay(1500).setFollowUp(new FadeTextSprite(100, false).setFinishCmd(nextStepCmd))));
			objects.add(countDown[i]);
		}
		
		objects.add(black);
		
		nextStep(0,2);
	}
}
