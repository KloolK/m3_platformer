package ui;

import java.awt.Color;

import objects.Drawable;

import processing.core.PApplet;
import processing.core.PFont;
import m3platformer.Command;
import m3platformer.GameLogic;
import m3platformer.GameMap;
import m3platformer.M3Platformer;
import m3platformer.ResourceManager;
import m3platformer.ScoreMedal;
import m3platformer.SoundManager;
import m3platformer.Sprite;
import m3platformer.StageScoreSystem;
import m3platformer.TextSprite;
import m3platformer.Timeout;
import m3platformer.animations.Animation;
import m3platformer.animations.FadeImage;
import m3platformer.animations.FadeTextSprite;

public class StageScore extends UIScreen {
	
	private Sprite box;
	private Sprite line1;
	private Sprite line2;
	private Sprite line3;
	private Sprite plusCoins;
	private Sprite plusTime;
	private Sprite plusMedals;
	private Sprite bestTime;
	private Sprite maxCoins;
	private Sprite medals;
	private Sprite stageScore;
	private Sprite timeLeft;
	private Sprite[] xs     = new Sprite[ScoreMedal.values().length-1];
	private Sprite[] clocks = new Sprite[ScoreMedal.values().length-1];
	private Sprite[] coins  = new Sprite[ScoreMedal.values().length-1];
	
	private PFont numberFontSmall;
	private PFont numberFontBig;
	private PFont subtitleFont;
	
	private Color textColor = new Color(111,195,223);
	
	private boolean hasEnded = false;
	
	public StageScore(GameLogic l) {
		super(l);
		
		// load images
		box = new Sprite(parent, "data/imgs/stageScore/box.gif");
		line1 = new Sprite(parent, "data/imgs/stageScore/line.gif");
		line2 = new Sprite(parent, "data/imgs/stageScore/line.gif");
		line3 = new Sprite(parent, "data/imgs/stageScore/line.gif");
		plusCoins = new Sprite(parent, "data/imgs/stageScore/plus.gif");
		plusTime = new Sprite(parent, "data/imgs/stageScore/plus.gif");
		plusMedals = new Sprite(parent, "data/imgs/stageScore/plus.gif");

		bestTime = new Sprite(parent, "data/imgs/stageScore/bestTime.gif");
		maxCoins = new Sprite(parent, "data/imgs/stageScore/maxCoins.gif");
		medals = new Sprite(parent, "data/imgs/stageScore/medals.gif");
		stageScore = new Sprite(parent, "data/imgs/stageScore/stageScore.gif");
		timeLeft = new Sprite(parent, "data/imgs/stageScore/timeLeft.gif");
		
		for(ScoreMedal m : ScoreMedal.values()) {
			if(m == ScoreMedal.None) continue;
			
			clocks[m.ordinal()] = new Sprite(parent, "data/imgs/icons/clock"+m.toString()+".gif");
			xs[m.ordinal()] = new Sprite(parent, "data/imgs/stageScore/x"+m.toString()+".gif");
			
			coins[m.ordinal()] = new Sprite(parent, "data/imgs/icons/coin"+m.toString()+".gif");
		}
		
		// load fonts
		numberFontSmall = ResourceManager.getFont("data/fonts/dice16Smooth.vlw");
		numberFontBig   = ResourceManager.getFont("data/fonts/dice22Smooth.vlw");
		subtitleFont	= ResourceManager.getFont("data/fonts/8bitwonder10Smooth.vlw");
		
		// draw stuff		
		GameMap map = logic.getCurrentMap();
		StageScoreSystem score = map.getScoring();
		
		// --------------------------------------------------- old total score
		TextSprite totalScoreOld = new TextSprite(numberFormat(logic.getPlayer().getTotalScore() - score.getStageScore()), numberFontBig, textColor, 22);
		totalScoreOld.setPosition(298, 211);
		totalScoreOld.setAlign(PApplet.RIGHT, PApplet.TOP);
		objects.add(totalScoreOld);
		
		// --------------------------------------------------- best time
		Animation fadeInText = new FadeTextSprite(500, true);
		Animation fadeInSprite = new FadeImage(500, true);
		Animation fadeInSpriteWithCmd;
		
		fadeInSpriteWithCmd = new FadeImage(500, true);
		fadeInSpriteWithCmd.setFinishCmd(new Command() {
			public void call(M3Platformer game) {
				game.getLogic().getStageScoring().nextStep(0, 4);
			}
		});
		bestTime.addAnimation(fadeInSpriteWithCmd);
		bestTime.setPosition(21, 72);
		
		TextSprite time = new TextSprite(secondsToString(score.getBestTime()), numberFontSmall, score.getTimeMedal().getColor(), 16);
		time.setPosition(172, 71);
		time.setAlign(PApplet.CENTER, PApplet.TOP);
		time.addAnimation(fadeInText);
		
		TextSprite timeScore = new TextSprite(numberFormat(score.getRawTimeScore()), numberFontSmall, textColor, 16);
		timeScore.setPosition(298, 71);
		timeScore.setAlign(PApplet.RIGHT, PApplet.TOP);
		timeScore.addAnimation(fadeInText);
		
		objects.add(bestTime);
		objects.add(time);
		objects.add(timeScore);
		
		
		
		// --------------------------------------------------- max coins
		fadeInSprite = new FadeImage(500, true);
		fadeInText = new FadeTextSprite(500, true);
		fadeInSpriteWithCmd = new FadeImage(500, true);
		fadeInSpriteWithCmd.setFinishCmd(new Command() {
			public void call(M3Platformer game) {
				game.getLogic().getStageScoring().nextStep(0, 8);
			}
		});
		maxCoins.addAnimation(fadeInSpriteWithCmd);
		maxCoins.setPosition(21, 89);
		
		TextSprite coinNum = new TextSprite(score.getBestCoins()+"", numberFontSmall, score.getCoinMedal().getColor(), 16);
		coinNum.setPosition(172, 88);
		coinNum.setAlign(PApplet.CENTER, PApplet.TOP);
		coinNum.addAnimation(fadeInText);
		
		plusCoins.setPosition(235, 90);
		plusCoins.addAnimation(fadeInSprite);
		
		TextSprite coinScore = new TextSprite(numberFormat(score.getRawCoinScore()), numberFontSmall, textColor, 16);
		coinScore.setPosition(298, 88);
		coinScore.setAlign(PApplet.RIGHT, PApplet.TOP);
		coinScore.addAnimation(fadeInText);

		objects.add(maxCoins);
		objects.add(plusCoins);
		objects.add(coinNum);
		objects.add(coinScore);
		
		
		
		// --------------------------------------------------- first sum and medals
		fadeInText = new FadeTextSprite(500, true);
		fadeInSprite = new FadeImage(500, true);
		fadeInSpriteWithCmd = new FadeImage(500, true);
		
		fadeInSpriteWithCmd.setFinishCmd(new Command() {
			public void call(M3Platformer game) {
				game.getLogic().getStageScoring().nextStep(0, 6);
			}
		});
		medals.addAnimation(fadeInSpriteWithCmd);
		medals.setPosition(21, 125);
		objects.add(medals);
		
		Sprite medal = clocks[score.getTimeMedal().ordinal()];
		medal.setPosition(153, 124);
		medal.addAnimation(fadeInSprite);
		objects.add(medal);
		
		plusMedals.setPosition(166, 125);
		plusMedals.addAnimation(fadeInSprite);
		objects.add(plusMedals);
		
		medal = coins[score.getCoinMedal().ordinal()];
		medal.setPosition(178, 124);
		medal.addAnimation(fadeInSprite);
		objects.add(medal);
		
		line1.setPosition(228, 101);
		line1.addAnimation(fadeInSprite);
		objects.add(line1);
		
		TextSprite sumScore = new TextSprite(numberFormat(score.getRawCoinScore() + score.getRawTimeScore()), numberFontSmall, textColor, 16);
		sumScore.setPosition(298, 107);
		sumScore.setAlign(PApplet.RIGHT, PApplet.TOP);
		sumScore.addAnimation(fadeInText);
		objects.add(sumScore);
		
		ScoreMedal m = ScoreMedal.Bronze;
		if(score.getTimeMedal() == ScoreMedal.Gold || score.getCoinMedal() == ScoreMedal.Gold) m = ScoreMedal.Gold;
		else if(score.getTimeMedal() == ScoreMedal.Silver || score.getCoinMedal() == ScoreMedal.Silver) m = ScoreMedal.Silver;
		
		Sprite x = xs[m.ordinal()];
		x.setPosition(233,125);
		x.addAnimation(fadeInSprite);
		objects.add(x);
		
		TextSprite factor = new TextSprite(Math.round(score.getTotalMult()*10)/10+"", numberFontSmall, m.getColor(), 16);
		factor.setPosition(298, 123);
		factor.setAlign(PApplet.RIGHT, PApplet.TOP);
		factor.addAnimation(fadeInText);
		objects.add(factor);
		
		
		// --------------------------------------------------- second sum and time left
		fadeInText = new FadeTextSprite(500, true);
		fadeInSprite = new FadeImage(500, true);
		fadeInSpriteWithCmd = new FadeImage(500, true);
		
		fadeInSpriteWithCmd.setFinishCmd(new Command() {
			public void call(M3Platformer game) {
				game.getLogic().getStageScoring().nextStep(0, 3);
			}
		});
		timeLeft.addAnimation(fadeInSpriteWithCmd);
		timeLeft.setPosition(21, 161);
		objects.add(timeLeft);
		
		line2.setPosition(228, 136);
		line2.addAnimation(fadeInSprite);
		objects.add(line2);
		
		TextSprite secSumScore = new TextSprite(numberFormat((int)((score.getRawCoinScore() + score.getRawTimeScore()) * score.getTotalMult())), numberFontSmall, textColor, 16);
		secSumScore.setPosition(298, 143);
		secSumScore.setAlign(PApplet.RIGHT, PApplet.TOP);
		secSumScore.addAnimation(fadeInText);
		objects.add(secSumScore);
		
		TextSprite timeLeftSp = new TextSprite(secondsToString(score.getStageTimeLeft()), numberFontSmall, new Color(255,255,255), 16);
		timeLeftSp.setPosition(172, 160);
		timeLeftSp.setAlign(PApplet.CENTER, PApplet.TOP);
		timeLeftSp.addAnimation(fadeInText);
		objects.add(timeLeftSp);
		
		plusTime.setPosition(235, 161);
		plusTime.addAnimation(fadeInSprite);
		objects.add(plusTime);
		
		TextSprite timeLeftScore = new TextSprite(numberFormat(score.getRawStageTimeScore()), numberFontSmall, textColor, 16);
		timeLeftScore.setPosition(298, 160);
		timeLeftScore.setAlign(PApplet.RIGHT, PApplet.TOP);
		timeLeftScore.addAnimation(fadeInText);
		objects.add(timeLeftScore);
		
		// --------------------------------------------------- stage score
		fadeInText = new FadeTextSprite(500, true);
		fadeInSprite = new FadeImage(500, true);
		fadeInSpriteWithCmd = new FadeImage(500, true);
		
		fadeInSpriteWithCmd.setFinishCmd(new Command() {
			public void call(M3Platformer game) {
				game.getLogic().getStageScoring().nextStep(1, 1); // hide old total score and show new one
				SoundManager.playCoin();
				game.getLogic().getStageScoring().hasEnded = true;
			}
		});
		stageScore.addAnimation(fadeInSpriteWithCmd);
		stageScore.setPosition(21, 185);
		objects.add(stageScore);
		
		line3.setPosition(228, 175);
		line3.addAnimation(fadeInSprite);
		objects.add(line3);
		
		TextSprite stageScoreSum = new TextSprite(numberFormat(score.getStageScore()), numberFontBig, textColor, 22);
		stageScoreSum.setPosition(298, 182);
		stageScoreSum.setAlign(PApplet.RIGHT, PApplet.TOP);
		stageScoreSum.addAnimation(fadeInText);
		objects.add(stageScoreSum);
		
		// --------------------------------------------------- new total score
		fadeInText = new FadeTextSprite(500, true);
		TextSprite totalScoreNew = new TextSprite(numberFormat(logic.getPlayer().getTotalScore()), numberFontBig, ScoreMedal.Gold.getColor(), 22);
		totalScoreNew.setPosition(298, 211);
		totalScoreNew.setAlign(PApplet.RIGHT, PApplet.TOP);
		totalScoreNew.addAnimation(fadeInText);
		objects.add(totalScoreNew);
		
		// start with time only
		drawUntil = 3;
	}
	
	@Override
	public void drawShape(int x, int y) {
		
		// load data
		GameMap map = logic.getCurrentMap();
		
		// semi-transparent overlay
		parent.background(0);
		box.draw(x, y);
		
		// draw objects
		super.drawShape(x, y);
		
		// subtitle
		int top, left, width;
		if(map.getTitle() != null && !map.getTitle().equals("")) {
			top = 46+y;
			left = x+parent.getWidth()/2;
			parent.fill(textColor);
			parent.textSize(10);
			parent.textFont(subtitleFont);
			parent.textAlign(PApplet.CENTER, PApplet.TOP);
			parent.text(map.getTitle(), left, top);
			
			// dashes
			top += 1;
			width = (int) parent.textWidth(map.getTitle());
			parent.textFont(numberFontBig);
			parent.textSize(12);
			parent.textAlign(PApplet.RIGHT, PApplet.TOP);
			parent.text("-", left-width/2 - 3, top);
			
			parent.textAlign(PApplet.LEFT, PApplet.TOP);
			parent.text("-", left+width/2 + 3, top);
		}
	}
	
	public boolean hasEnded() {return this.hasEnded;}
	
	public void jumpToEnd() {
		if(this.hasEnded) return;
		else {
			this.drawUntil = 999;
			this.drawOffset = 1;
			for(Drawable o : this.objects) {
				o.clearAnimations();
				
				if(o instanceof Sprite)
					((Sprite)o).setAlpha(255);
			}
			
			this.hasEnded = true;
		}
	}
}
