package ui;

import java.awt.Color;
import java.util.Iterator;

import processing.core.PApplet;
import processing.core.PFont;
import m3platformer.GameLogic;
import m3platformer.Highscore;
import m3platformer.ResourceManager;
import m3platformer.ScoreMedal;
import m3platformer.Sprite;
import m3platformer.animations.Blink;
import m3platformer.animations.Delay;
import m3platformer.animations.FadeImage;
import m3platformer.animations.RepeatedAnimation;

public class HighscoreScreen extends UIScreen {
	
	private Sprite box;
	private Sprite coinMsg;
	private Sprite returnButton;
	private Sprite inputInstr;
	private Sprite saveButton;
	
	private int inputPosition = -1;
	
	private PFont numberFont;
	private PFont textFont;
	
	private Color textColor = new Color(111,195,223);
	private Color top1Color = ScoreMedal.Gold.getColor();
	
	public HighscoreScreen(GameLogic l) {
		super(l);
		
		// load images
		box = new Sprite(parent, "data/imgs/highscore/box.gif", 0, 0, 1, 0);
		coinMsg = new Sprite(parent, "data/imgs/logo/insertCoin.gif", 0, 16, 1, 0);
		coinMsg.addAnimation(new RepeatedAnimation().addAnimation(new Blink(2000, 1)));
		returnButton = new Sprite(parent, "data/imgs/highscore/returnButton.gif", 50, 214, 1, 0);
		inputInstr = new Sprite(parent, "data/imgs/highscore/input.gif", 21, 212, 1, 0);
		saveButton = new Sprite(parent, "data/imgs/highscore/save.gif", 134, 214, 1, 0);
		
		inputInstr.setAlpha(255);
		inputInstr.addAnimation(new RepeatedAnimation().addAnimation(new Delay(10000)).addAnimation(new FadeImage(400, false)).addAnimation(new Delay(10000)).addAnimation(new FadeImage(400, true)));
		saveButton.setAlpha(0);
		saveButton.addAnimation(new RepeatedAnimation().addAnimation(new Delay(10000)).addAnimation(new FadeImage(400, true)).addAnimation(new Delay(10000)).addAnimation(new FadeImage(400, false)));
		
		// load fonts
		numberFont = ResourceManager.getFont("data/fonts/dice16Smooth.vlw");
		textFont   = ResourceManager.getFont("data/fonts/8bitwonder10Smooth.vlw");
		
		objects.add(box);
		
		// start with time only
		drawUntil = 999;
	}
	
	@Override
	public void drawShape(int x, int y) {
		super.drawShape(x, y);
		
		// from bottom upwards
		Iterator<Highscore.HEntry> iterator = logic.getHighscore().getEntries().iterator();
		if(inputPosition > 0 && iterator.hasNext()) iterator.next();
		for(int i=10;i>0 && iterator.hasNext();i--) {	
			Color mainColor = (i==1) ? top1Color : textColor;
			
			parent.fill(mainColor);
			parent.stroke(mainColor.getRed(), mainColor.getGreen(), mainColor.getBlue());
			
			int top = 51+(i-1)*15;
			int left = 36;
			int right = 298;
			
			// position
			parent.textAlign(PApplet.RIGHT, PApplet.TOP);
			parent.textFont(numberFont);
			parent.textSize(16);
			parent.text(i+".", left, top);
			
			// are we in input mode?
			if(inputPosition > 0 && inputPosition==i) {
				String curInput = logic.getCurrentInput().getString();
				
				// score
				parent.text(numberFormat(logic.getPlayer().getTotalScore()), right, top);
				
				// name
				if(curInput.length() < logic.getCurrentInput().getMinLen()) parent.fill(245,43,22);
				parent.textAlign(PApplet.LEFT, PApplet.TOP);
				parent.textFont(textFont);
				parent.textSize(10);
				parent.text(curInput, left+8, top+2);
				
				// cursor
				parent.fill(mainColor);
				float width = parent.textWidth(curInput);
				float widthLast = parent.textWidth(curInput.substring(curInput.length()-1));
				parent.strokeWeight(2);
				parent.line(left+8+width+1, top+2+10, left+8+width-widthLast-2, top+2+10);
				parent.strokeWeight(1);
			}
			
			else {
				Highscore.HEntry entry = iterator.next();
				
				// score
				parent.text(numberFormat(entry.getScore()), right, top);
				
				// name
				parent.textAlign(PApplet.LEFT, PApplet.TOP);
				parent.textFont(textFont);
				parent.textSize(10);
				parent.text(entry.getName(), left+8, top+2);
			}
		}
	}
	
	public void drawCoinMsg() {
		coinMsg.draw(0, 0);
	}
	
	public void drawReturnButton() {
		returnButton.draw(0,0);
	}
	
	public void drawInputInstructions() {
		inputInstr.draw(0,0);
		saveButton.draw(0, 0);
	}

	public int getInputPosition() {
		return inputPosition;
	}

	public void setInputPosition(int inputPosition) {
		this.inputPosition = inputPosition;
	}
}
