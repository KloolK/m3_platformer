In this Jump'n'Run you play as Alan and help him on his journey to find his lost girlfriend Alina. It was developed as a part of the [FabArcade Project](http://www.fabarca.de)

Code is licensed under GPLv3, so feel free to fork :)


HOW TO COMPILE / DEVELOP
----------------
We used eclipse with the proclipsing plugin to develop the game. If you would like to make changes or compile it on your own, 
we can recommend this tutorial to get startet with proclipsing:
[GettingStarted](http://code.google.com/p/proclipsing/wiki/GettingStarted)

Just clone the repository and import the folder as a processing project into eclipse.
Eventually you will have to add the processing core and minim libraries to the build path for it to work.


We appreciate your comments and ideas! Just write us an email to:

* [jakob.bauer@rwth-aachen.de](mailto:jakob.bauer@rwth-aachen.de)
* [jan.bruckner@rwth-aachen.de](mailto:jan.bruckner@rwth-aachen.de)
* [michael.deutschen@rwth-aachen.de](mailto:michael.deutschen@rwth-aachen.de)
